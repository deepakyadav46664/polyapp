package allDB

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// all create, read, update, and delete functions should follow these patterns.
// createFunc func(industryID string, domainID string, createStruct interface{}) error
// readFunc   func(industryID string, domainID string, firestoreID string) (struct{}, error)
// updateFunc func(industryID string, domainID string, updateStruct *struct{}) error
// deleteFunc func(industryID string, domainID string, firestoreID string) error

// TODO add a test for Role

func TestCreateData(t *testing.T) {
	t.Parallel()
	d := &common.Data{
		FirestoreID: "TestCreateDataallDB",
		IndustryID:  "TestCreateDataallDB",
		DomainID:    "TestCreateDataallDB",
		SchemaID:    "TestCreateDataallDB",
	}
	unorderedData := make(map[string]interface{})
	unorderedData["a"] = "C"
	unorderedData["q"] = false
	d.Init(unorderedData)
	err := CreateData(d)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateData(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteData(d.FirestoreID)
	if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
		t.Fatal("unexpected error: " + err.Error())
	}
	err = CreateSchema(&common.Schema{
		FirestoreID:  "TestCreateDataallDB",
		IndustryID:   "TestCreateDataallDB",
		DomainID:     "TestCreateDataallDB",
		SchemaID:     "TestCreateDataallDB",
		DataTypes:    map[string]string{"a": "S", "q": "B"},
		DataHelpText: map[string]string{"a": "S", "q": "B"},
		DataKeys:     []string{"a", "q"},
	})
	err = CreateData(d)
	if err != nil {
		t.Error("Create Data: " + err.Error())
	}
	dOut, err := ReadData(d.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(d, &dOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadData(t *testing.T) {
	t.Parallel()
	var err error
	_, err = ReadData("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	// this test relies on CreateData. Sorry.
	d := &common.Data{
		FirestoreID: "TestReadDataallDB",
		IndustryID:  "TestReadDataallDB",
		DomainID:    "TestReadDataallDB",
		SchemaID:    "TestReadDataallDB",
	}
	unorderedData := make(map[string]interface{})
	unorderedData["a"] = "C"
	unorderedData["q"] = false
	d.Init(unorderedData)
	err = CreateSchema(&common.Schema{
		FirestoreID:  "TestUpdateDataallDB",
		IndustryID:   "TestUpdateDataallDB",
		DomainID:     "TestUpdateDataallDB",
		SchemaID:     "TestUpdateDataallDB",
		DataTypes:    map[string]string{"a": "S", "q": "B", "f": "F"},
		DataHelpText: map[string]string{"a": "a", "q": "q", "f": "f"},
		DataKeys:     []string{"a", "q", "f"},
	})
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not CreateSchema to get things set up: " + err.Error())
	}
	err = CreateData(d)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("couldn't use CreateData to get things set up: " + err.Error())
	}
	dOut, err := ReadData("TestReadDataallDB")
	if err != nil {
		t.Error("expected ReadData to succeed: " + err.Error())
	}
	if *dOut.S["a"] != "C" {
		t.Error("didn't set 'a'")
	}
	if *dOut.B["q"] != false {
		t.Error("didn't set 'q'")
	}
}

func TestUpdateData(t *testing.T) {
	t.Parallel()
	newData := &common.Data{
		FirestoreID: "TestUpdateDataallDB",
		IndustryID:  "TestUpdateDataallDB",
		DomainID:    "TestUpdateDataallDB",
		SchemaID:    "TestUpdateDataallDB",
	}
	unorderedData := make(map[string]interface{})
	unorderedData["a"] = "D"
	unorderedData["q"] = false
	unorderedData["f"] = 33.2
	newData.Init(unorderedData)
	var err error
	err = UpdateData(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	// this test relies on CreateData. Sorry.
	oldData := &common.Data{
		FirestoreID: "TestUpdateDataallDB",
		IndustryID:  "TestUpdateDataallDB",
		DomainID:    "TestUpdateDataallDB",
		SchemaID:    "TestUpdateDataallDB",
	}
	unorderedOldData := make(map[string]interface{})
	unorderedOldData["a"] = "C"
	unorderedOldData["q"] = false
	oldData.Init(unorderedOldData)
	err = CreateSchema(&common.Schema{
		FirestoreID:  "TestUpdateDataallDB",
		IndustryID:   "TestUpdateDataallDB",
		DomainID:     "TestUpdateDataallDB",
		SchemaID:     "TestUpdateDataallDB",
		DataTypes:    map[string]string{"a": "S", "q": "B", "f": "F"},
		DataHelpText: map[string]string{"a": "a", "q": "q", "f": "f"},
		DataKeys:     []string{"a", "q", "f"},
	})
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not CreateSchema to get things set up: " + err.Error())
	}
	err = CreateData(oldData)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("couldn't use CreateData to get things set up: " + err.Error())
	}
	err = UpdateData(newData)
	if err != nil {
		t.Error("expected UpdateData to succeed: " + err.Error())
	}

	dMerged, err := ReadData("TestUpdateDataallDB")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if dMerged.S == nil || *dMerged.S["a"] != "D" {
		t.Error("didn't set 'a'")
	}
	if v, ok := dMerged.B["q"]; *v != false || !ok {
		t.Error("didn't set 'q'")
	}
	if dMerged.F == nil || *dMerged.F["f"] != 33.2 {
		t.Error("didn't set 'f'")
	}
}

func TestDeleteData(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteData("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	// this test relies on createData. Sorry.
	oldData := &common.Data{
		FirestoreID: "TestDeleteDataaari",
		IndustryID:  "TestDeleteDataaari",
		DomainID:    "TestDeleteDataaari",
		SchemaID:    "TestDeleteDataaari",
	}
	unorderedOldData := make(map[string]interface{})
	unorderedOldData["a"] = "C"
	unorderedOldData["q"] = false
	oldData.Init(unorderedOldData)
	err = createData(oldData)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("couldn't use CreateData to get things set up: " + err.Error())
	}

	err = DeleteData("TestDeleteDataaari")
	if err != nil {
		t.Error("could not delete data: " + err.Error())
	}

	_, err = ReadData("TestDeleteDataaari")
	if err == nil {
		t.Error("there should be an error")
	}
	if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
		t.Error("unexpected error code when returned from ReadData on a created document: " + err.Error())
	}
}

func TestCreateSchema(t *testing.T) {
	t.Parallel()
	s := &common.Schema{
		FirestoreID:  "TestCreateSchemaallDB",
		NextSchemaID: common.String("TestCreateSchemaallDB"),
		IndustryID:   "TestCreateSchemaallDB",
		DomainID:     "TestCreateSchemaallDB",
		SchemaID:     "TestCreateSchemaallDB",
		DataTypes:    make(map[string]string),
		Deprecated:   common.Bool(false),
	}
	s.DataTypes["some field"] = "S"
	s.DataTypes["some field 2"] = "Ref"
	s.DataKeys = []string{"some field", "some field 2"}
	s.DataHelpText = map[string]string{
		"some field":   "generic",
		"some field 2": "generic",
	}
	err := CreateSchema(s)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateSchema(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteSchema(s.FirestoreID)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	err = CreateSchema(s)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	dOut, err := ReadSchema(s.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(s, &dOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadSchema(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateSchema(t *testing.T) {
	t.Parallel()
	newSchema := &common.Schema{
		FirestoreID:  "TestUpdateSchemaallDB",
		IndustryID:   "TestUpdateSchemaallDB",
		DomainID:     "TestUpdateSchemaallDB",
		SchemaID:     "TestUpdateSchemaallDB",
		DataTypes:    make(map[string]string),
		DataKeys:     make([]string, 0),
		DataHelpText: make(map[string]string),
		Deprecated:   common.Bool(false),
	}
	newSchema.DataTypes["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field"] = "S"
	newSchema.DataTypes["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_new field"] = "Ref"
	newSchema.DataKeys = []string{"TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field", "TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_new field"}
	newSchema.DataHelpText["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field"] = "askljsad"
	newSchema.DataHelpText["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_new field"] = "sdaljk"
	var err error
	err = UpdateSchema(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	// this test relies on CreateSchema. Sorry.
	oldSchema := &common.Schema{
		FirestoreID:  "TestUpdateSchemaallDB",
		NextSchemaID: common.String("TestUpdateSchemaallDB"),
		IndustryID:   "TestUpdateSchemaallDB",
		DomainID:     "TestUpdateSchemaallDB",
		SchemaID:     "TestUpdateSchemaallDB",
		DataTypes:    make(map[string]string),
		DataKeys:     make([]string, 2),
		DataHelpText: make(map[string]string),
		Deprecated:   common.Bool(false),
	}
	oldSchema.DataTypes["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field"] = "Ref"
	oldSchema.DataTypes["TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_do not touch"] = "S"
	oldSchema.DataKeys[0] = "TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field"
	oldSchema.DataKeys[1] = "TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_do not touch"
	oldSchema.DataHelpText = map[string]string{
		"TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_some field":   "a",
		"TestUpdateSchemaallDB_TestUpdateSchemaallDB_TestUpdateSchemaallDB_do not touch": "b",
	}
	err = CreateSchema(oldSchema)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("couldn't use CreateSchema to get things set up: " + err.Error())
	}
	err = UpdateSchema(newSchema)
	if err != nil {
		t.Error("expected UpdateSchema to succeed: " + err.Error())
	}

	dMerged, err := ReadSchema("TestUpdateSchemaallDB")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if dMerged.DataTypes == nil || dMerged.DataTypes["some field"] != "S" {
		t.Error("didn't update 'some field")
	}
	if dMerged.DataTypes == nil || dMerged.DataTypes["do not touch"] != "S" {
		t.Error("should not have modified a field not listed in the update")
	}
	if dMerged.DataTypes == nil || dMerged.DataTypes["new field"] != "Ref" {
		t.Error("should have added the new field")
	}
}

func TestDeleteSchema(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteSchema("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	sc := &common.Schema{
		FirestoreID:  "TestDeleteSchemaallDB",
		NextSchemaID: common.String("TestDeleteSchemaallDB"),
		IndustryID:   "TestDeleteSchemaallDB",
		DomainID:     "TestDeleteSchemaallDB",
		SchemaID:     "TestDeleteSchemaallDB",
		DataTypes:    make(map[string]string),
		DataKeys:     make([]string, 0),
		DataHelpText: make(map[string]string),
		Deprecated:   common.Bool(false),
	}
	sc.DataTypes["some field"] = "S"
	sc.DataTypes["some field 2"] = "Ref"
	sc.DataKeys = []string{"some field", "some field 2"}
	sc.DataHelpText["some field"] = "generic"
	sc.DataHelpText["some field 2"] = "generic"
	s := "TestDeleteSchemaallDB"
	err = CreateSchema(sc)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create schema to test deleting that schema: " + err.Error())
	}
	successErr := DeleteSchema(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteSchema(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}

func TestCreateUser(t *testing.T) {
	t.Parallel()
	u := &common.User{
		FirestoreID: "TestCreateUserallDB",
	}
	err := CreateUser(u)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateUser(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteUser(u.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	err = CreateUser(u)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	dOut, err := ReadUser(u.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(u, &dOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadUser(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateUser(t *testing.T) {
	t.Parallel()
	oldUser := &common.User{
		FirestoreID:   "TestUpdateUserallDB",
		FullName:      common.String("TestUpdateUserallDB"),
		PhotoURL:      common.String("TestUpdateUserallDB"),
		EmailVerified: common.Bool(true),
		PhoneNumber:   common.String("+17635555555"),
	}
	newUser := &common.User{
		FirestoreID: "TestUpdateUserallDB",
		FullName:    common.String("TestUpdateUserallDB"),
		PhotoURL:    common.String("TestUpdateUserallDB"),
		PhoneNumber: common.String("+17635556666"),
		Email:       common.String("TestUpdateUserallDB@polyapp.tech"),
	}
	var err error
	err = UpdateUser(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}

	err = DeleteUser("TestUpdateUserallDB")
	if err != nil {
		t.Fatal("could not reset test")
	}
	err = CreateUser(oldUser)
	if err != nil {
		t.Fatal("couldn't use CreateUser to get things set up: " + err.Error())
	}
	newUser.UID = oldUser.UID
	err = UpdateUser(newUser)
	if err != nil {
		t.Error("expected UpdateUser to succeed: " + err.Error())
	}

	dMerged, err := ReadUser("TestUpdateUserallDB")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if dMerged.PhoneNumber == nil || *dMerged.PhoneNumber != "+17635556666" {
		t.Error("didn't update Phone Number")
	}
	if dMerged.EmailVerified == nil || *dMerged.EmailVerified != true {
		t.Error("should not have modified a field not listed in the update")
	}
	if dMerged.Email == nil || *dMerged.Email != "TestUpdateUserallDB@polyapp.tech" {
		t.Error("should have added the new information")
	}
}

func TestDeleteUser(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteUser("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	user := &common.User{
		FirestoreID: "TestDeleteUserallDB",
	}
	s := "TestDeleteUserallDB"
	err = CreateUser(user)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create User to test deleting that User: " + err.Error())
	}
	successErr := DeleteUser(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteUser(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}

func TestCreateTask(t *testing.T) {
	t.Parallel()
	task := &common.Task{
		FirestoreID:   "TestCreateTaskallDB",
		IndustryID:    "TestCreateTaskallDB",
		DomainID:      "TestCreateTaskallDB",
		Name:          "Name",
		HelpText:      "HelpText",
		TaskGoals:     map[string]string{},
		FieldSecurity: map[string]*common.FieldSecurityOptions{},
		BotStaticData: map[string]string{},
	}
	err := CreateTask(task)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateTask(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteTask(task.FirestoreID)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	err = CreateTask(task)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	taskOut, err := ReadTask(task.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(taskOut, *task)
	if !areEqual {
		t.Error("not equal")
		t.Log(*task)
		t.Log(taskOut)
	}
}

func TestReadTask(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateTask(t *testing.T) {
	t.Parallel()
	oldTask := &common.Task{
		FirestoreID: "TestUpdateTaskallDB",
		IndustryID:  "TestUpdateTaskallDB",
		DomainID:    "TestUpdateTaskallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		TaskGoals:   map[string]string{},
		Deprecated:  common.Bool(false),
	}
	newTask := &common.Task{
		FirestoreID: "TestUpdateTaskallDB",
		IndustryID:  "TestUpdateTaskallDB",
		DomainID:    "TestUpdateTaskallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		TaskGoals:   map[string]string{"taskLevelGoal": "TestGoal"},
	}
	var err error
	err = UpdateTask(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	err = DeleteTask("TestUpdateTaskallDB")
	if err != nil {
		t.Fatal("could not reset test")
	}
	err = CreateTask(oldTask)
	if err != nil {
		t.Fatal("couldn't use CreateTask to get things set up: " + err.Error())
	}
	err = UpdateTask(newTask)
	if err != nil {
		t.Error("expected UpdateTask to succeed: " + err.Error())
	}

	dMerged, err := ReadTask("TestUpdateTaskallDB")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if dMerged.Deprecated == nil || *dMerged.Deprecated != false {
		t.Error("should not have modified a field not listed in the update")
	}
	if dMerged.TaskGoals["taskLevelGoal"] != "TestGoal" {
		t.Error("should have added the new field")
	}
}

func TestDeleteTask(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteTask("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	task := &common.Task{
		FirestoreID: "TestDeleteTaskallDB",
		IndustryID:  "TestDeleteTaskallDB",
		DomainID:    "TestDeleteTaskallDB",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	s := "TestDeleteTaskallDB"
	err = CreateTask(task)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create Task to test deleting that Task: " + err.Error())
	}
	successErr := DeleteTask(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteTask(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}

func TestCreateUX(t *testing.T) {
	t.Parallel()
	ux := &common.UX{
		FirestoreID:  "TestCreateUXallDB",
		IndustryID:   "TestCreateUXallDB",
		DomainID:     "TestCreateUXallDB",
		SchemaID:     "TestCreateUXallDB",
		HTML:         common.String("<p>hello TestcreateUXallDB</p>"),
		SelectFields: map[string]string{},
	}
	err := CreateUX(ux)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateUX(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteUX(ux.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	err = CreateUX(ux)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	dOut, err := ReadUX(ux.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(ux, &dOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadUX(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateUX(t *testing.T) {
	t.Parallel()
	oldUX := &common.UX{
		FirestoreID:  "TestUpdateUXallDB",
		IndustryID:   "TestUpdateUXallDB",
		DomainID:     "TestUpdateUXallDB",
		SchemaID:     "TestUpdateUXallDB",
		HTML:         common.String("TestUpdateUXallDB"),
		SelectFields: map[string]string{},
	}
	newUX := &common.UX{
		FirestoreID:  "TestUpdateUXallDB",
		HTML:         common.String("new"),
		IndustryID:   "TestUpdateUXallDB",
		DomainID:     "TestUpdateUXallDB",
		SchemaID:     "TestUpdateUXallDB",
		SelectFields: map[string]string{},
	}
	var err error
	err = UpdateUX(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	err = DeleteUX("TestUpdateUXallDB")
	if err != nil {
		t.Fatal("could not reset test")
	}
	err = CreateUX(oldUX)
	if err != nil {
		t.Fatal("couldn't use CreateUX to get things set up: " + err.Error())
	}
	err = UpdateUX(newUX)
	if err != nil {
		t.Error("expected UpdateUX to succeed: " + err.Error())
	}

	dMerged, err := ReadUX("TestUpdateUXallDB")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if dMerged.HTML == nil || *dMerged.HTML != "new" {
		t.Error("didn't update 'some field'")
	}
}

func TestDeleteUX(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteUX("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	ux := &common.UX{
		FirestoreID:  "TestDeleteUXallDB",
		IndustryID:   "TestDeleteUXallDB",
		DomainID:     "TestDeleteUXallDB",
		SchemaID:     "TestDeleteUXallDB",
		HTML:         common.String("<p>TestDeleteUXallDB</p>"),
		SelectFields: map[string]string{},
	}
	s := "TestDeleteUXallDB"
	err = CreateUX(ux)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create UX to test deleting that UX: " + err.Error())
	}
	successErr := DeleteUX(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteUX(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}

func TestCreateBot(t *testing.T) {
	t.Parallel()
	bot := &common.Bot{
		FirestoreID: "TestCreateBotallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		ActionIDs:   []string{"TestCreateBotallDB"},
	}
	err := CreateBot(bot)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateBot(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteBot(bot.FirestoreID)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	err = CreateBot(bot)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	botOut, err := ReadBot(bot.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(bot, &botOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadBot(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateBot(t *testing.T) {
	t.Parallel()
	oldBot := &common.Bot{
		FirestoreID: "TestUpdateBotallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		ActionIDs:   []string{},
		Deprecated:  common.Bool(false),
	}
	newBot := &common.Bot{
		FirestoreID: "TestUpdateBotallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		ActionIDs:   []string{"TestAction"},
	}
	var err error
	err = UpdateBot(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	err = DeleteBot("TestUpdateBotallDB")
	if err != nil {
		t.Fatal("could not reset test")
	}
	err = CreateBot(oldBot)
	if err != nil {
		t.Fatal("couldn't use CreateBot to get things set up: " + err.Error())
	}
	err = UpdateBot(newBot)
	if err != nil {
		t.Error("expected UpdateBot to succeed: " + err.Error())
	}

	dMerged, err := ReadBot("TestUpdateBotallDB")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if dMerged.Deprecated == nil || *dMerged.Deprecated != false {
		t.Error("should not have modified a field not listed in the update")
	}
	if len(dMerged.ActionIDs) != 1 || dMerged.ActionIDs[0] != "TestAction" {
		t.Error("should have added the new Action")
	}
}

func TestDeleteBot(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteBot("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	bot := &common.Bot{
		FirestoreID: "TestDeleteBotallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		ActionIDs:   []string{},
	}
	s := "TestDeleteBotallDB"
	err = CreateBot(bot)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create Bot to test deleting that Bot: " + err.Error())
	}
	successErr := DeleteBot(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteBot(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}

func TestCreateAction(t *testing.T) {
	t.Parallel()
	action := &common.Action{
		FirestoreID: "TestCreateActionallDB",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	err := CreateAction(action)
	if err == nil {
		t.Error("should err with empty input")
	}
	err = CreateAction(nil)
	if err == nil {
		t.Error("should err with nil input")
	}
	err = DeleteAction(action.FirestoreID)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	err = CreateAction(action)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	actionOut, err := ReadAction(action.FirestoreID)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	areEqual := reflect.DeepEqual(action, &actionOut)
	if !areEqual {
		t.Error("not equal")
	}
}

func TestReadAction(t *testing.T) {
	// In the Create test we are reading the output && verifying it == the input, which is what a Read test would do
}

func TestUpdateAction(t *testing.T) {
	t.Parallel()
	oldAction := &common.Action{
		FirestoreID: "TestUpdateActionallDB",
		Name:        "Name",
		HelpText:    "HelpText",
		Deprecated:  common.Bool(false),
	}
	newAction := &common.Action{
		FirestoreID: "TestUpdateActionallDB",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	var err error
	err = UpdateAction(nil)
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	err = DeleteAction("TestUpdateActionallDB")
	if err != nil {
		t.Fatal("could not reset test")
	}
	err = CreateAction(oldAction)
	if err != nil {
		t.Fatal("couldn't use CreateAction to get things set up: " + err.Error())
	}
	err = UpdateAction(newAction)
	if err != nil {
		t.Error("expected UpdateAction to succeed: " + err.Error())
	}

	dMerged, err := ReadAction("TestUpdateActionallDB")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if dMerged.Deprecated == nil || *dMerged.Deprecated != false {
		t.Error("should not have modified a field not listed in the update")
	}
}

func TestDeleteAction(t *testing.T) {
	t.Parallel()
	var err error
	err = DeleteAction("")
	if err == nil {
		t.Error("should fail if an argument is empty")
	}
	action := &common.Action{
		FirestoreID: "TestDeleteActionallDB",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	s := "TestDeleteActionallDB"
	err = CreateAction(action)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not create Action to test deleting that Action: " + err.Error())
	}
	successErr := DeleteAction(s)
	if successErr != nil {
		t.Error("should have succeeded")
	}
	err = DeleteAction(s)
	if err != nil {
		t.Error("deleting something which does not exist should not throw an error. Error: " + err.Error())
	}
}
