module gitlab.com/polyapp-open-source/polyapp

go 1.14

require (
	cloud.google.com/go v0.75.0
	cloud.google.com/go/firestore v1.4.0
	cloud.google.com/go/storage v1.12.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20201229214741-2366c2514674
	github.com/jmoiron/sqlx v1.3.4
	github.com/labstack/echo/v4 v4.5.0
	github.com/lib/pq v1.10.2
	github.com/mattn/go-isatty v0.0.13 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985
	golang.org/x/oauth2 v0.0.0-20210113160501-8b1d76fa0423
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/tools v0.1.0 // indirect
	google.golang.org/api v0.37.0
	google.golang.org/genproto v0.0.0-20210126160654-44e461bb6506
	google.golang.org/grpc v1.35.0
)
