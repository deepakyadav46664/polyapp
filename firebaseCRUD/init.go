package firebaseCRUD

import (
	"context"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"fmt"
	"sync"
)

var (
	clientMux sync.Mutex
	client    *auth.Client
	clientCtx context.Context
)

// GetFirebaseClient quickly gives you a shared firebase client.
func GetFirebaseClient() (context.Context, *auth.Client, error) {
	clientMux.Lock()
	defer clientMux.Unlock()
	if client != nil {
		return clientCtx, client, nil
	}
	// Requires that GOOGLE_APPLICATION_CREDENTIALS is set or similar.
	clientCtx = context.Background()
	app, err := firebase.NewApp(clientCtx, nil)
	if err != nil {
		return clientCtx, nil, fmt.Errorf("couldn't init firebase app: %w", err)
	}
	client, err := app.Auth(clientCtx)
	if err != nil {
		return clientCtx, nil, fmt.Errorf("couldn't get Auth client: %w", err)
	}
	return clientCtx, client, nil
}
