package gen

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"math/rand"
	"net/url"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// LabeledInputSort sorts LabeledInput by the label itself.
// It follows a pattern similar to the SortKeys example in https://golang.org/pkg/sort/
type LabeledInputBy func(li1 *LabeledInput, li2 *LabeledInput) bool

// Sort is a method on the function type that sorts the argument slice according to the function.
func (by LabeledInputBy) Sort(lis []LabeledInput) {
	sorter := &labeledInputSorter{
		lis: lis,
		by:  by,
	}
	sort.Sort(sorter)
}

// labeledInputSorter helps with sorting.
type labeledInputSorter struct {
	lis []LabeledInput
	by  func(li1 *LabeledInput, li2 *LabeledInput) bool
}

// Len is part of sort.Interface.
func (s *labeledInputSorter) Len() int {
	return len(s.lis)
}

// Swap is part of sort.Interface.
func (s *labeledInputSorter) Swap(i, j int) {
	s.lis[i], s.lis[j] = s.lis[j], s.lis[i]
}

// Sort is a method on the function type, By, that sorts the argument slice.
func (s labeledInputSorter) Less(i, j int) bool {
	return s.by(&s.lis[i], &s.lis[j])
}

// LabeledInput can be used to get Input Template with GenLabeledInput.
type LabeledInput struct {
	// WrapperID is optional. It is the ID of the surrounding element.
	WrapperID string
	// Label is the Label of the input
	Label string
	// HelpText for this field. This is required.
	HelpText string
	// ID is the ID of the actual input
	ID string
	// InitialValues is the initial value of the field.
	InitialValue string
	// Type is the type of the input. "text", "textarea", "checkbox", "radio", "number", "color", "ref"
	Type string
	// Readonly is optional. If set, the field becomes read-only.
	Readonly bool
	// AutocompleteOff. If set, the field will not use autocomplete.
	AutocompleteOff bool
}

// Validate ensures the correct fields are populated without validating the output.
func (labeledInput LabeledInput) Validate() error {
	if labeledInput.ID == "" {
		return errors.New("ID was empty")
	}
	if labeledInput.Label == "" {
		return errors.New("Label was empty")
	}
	if labeledInput.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if labeledInput.Type == "" {
		return errors.New("Type was empty")
	}
	switch labeledInput.Type {
	case "text":
		return nil
	case "textarea":
		return nil
	case "checkbox":
		if labeledInput.InitialValue != "" && labeledInput.InitialValue != "true" && labeledInput.InitialValue != "false" {
			return errors.New("checkbox input can only have values of empty string, true or false")
		}
		return nil
	case "radio":
		if labeledInput.InitialValue != "" && labeledInput.InitialValue != "true" && labeledInput.InitialValue != "false" {
			return errors.New("radio input can only have values of empty string, true or false")
		}
		return nil
	case "number":
		if labeledInput.InitialValue != "" {
			_, err := strconv.ParseFloat(labeledInput.InitialValue, 64)
			if err != nil {
				return fmt.Errorf("number inputs must have initial values which are numbers: %w", err)
			}
		}
		return nil
	case "color":
		if labeledInput.InitialValue != "" {
			if len(labeledInput.InitialValue) != 7 {
				return errors.New("Color should have a length of 7")
			}
			for i, c := range labeledInput.InitialValue {
				if i == 0 && c != '#' {
					return errors.New("Color should be prefixed with #")
				} else if i == 0 {
					// what we want
				} else if (c <= 'f' && c >= 'a') || (c <= '9' && c >= '0') {
					// we're golden
				} else {
					return errors.New("Color contains non-hex characters")
				}
			}
		}
		return nil
	case "ref":
		if labeledInput.InitialValue != "" {
			_, err := common.ParseRef(labeledInput.InitialValue)
			if err != nil {
				return fmt.Errorf("common.ParseRef indicates %v is not a valid Ref: %w", labeledInput.InitialValue, err)
			}
		}
		return nil
	default:
		// select type should use type SelectInput
		return errors.New("unhandled LabeledInput.Type")
	}
}

// GenLabeledInput creates Template out of an array of LabeledInput.
func GenLabeledInput(inputs []LabeledInput, writer io.Writer) error {
	if len(inputs) < 1 {
		return errors.New("expected an array of LabeledInput to be passed")
	}
	for i := range inputs {
		err := inputs[i].Validate()
		if err != nil {
			return fmt.Errorf("GenLabeledInput validation error at index "+strconv.Itoa(i)+" : %w", err)
		}
		inputs[i].ID = url.PathEscape(inputs[i].ID)
		if inputs[i].WrapperID == "" {
			inputs[i].WrapperID = common.GetRandString(15)
		}
	}
	inputTemplate := `{{/* . is []LabeledInput  */}}
{{define "inputTemplate"}}
{{- range . -}}
{{if eq .Type "text"}}
<div class="form-group" id="{{.WrapperID}}">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
        <img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<input type="text" {{if .AutocompleteOff}}autocomplete="off"{{end}} {{if .Readonly}}readonly class="form-control"{{else}}class="form-control"{{end}} id="{{.ID}}" value="{{.InitialValue}}" />
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{ if eq .Type "ref"}}
<div class="form-group" id="{{.WrapperID}}">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
        <img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
		<div class="input-group-prepend">
			<a {{if .InitialValue}}href="{{.InitialValue}}"{{end}} class="btn btn-link polyappjs-ref-link"><i class="fa fa-link"></i></a>
		</div>
		<input type="text" {{if .AutocompleteOff}}autocomplete="off"{{end}} {{if .Readonly}}readonly class="form-control"{{else}}class="form-control"{{end}} id="{{.ID}}" value="{{.InitialValue}}" />
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{if eq .Type "textarea"}}
<div class="form-group" id="{{.WrapperID}}">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<textarea type="text" {{if .Readonly}}readonly class="form-control"{{else}}class="form-control"{{end}} rows="5" id="{{.ID}}">{{.InitialValue}}</textarea>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{if eq .Type "checkbox"}}
<div class="form-check" id="{{.WrapperID}}">
	<label class="form-check-label">
		<input type="checkbox" class="form-check-input" value="" {{if ne .InitialValue ""}}checked{{end}} id="{{.ID}}" />{{.Label}}
	</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{if eq .Type "radio"}}
<div class="form-check" id="{{.WrapperID}}">
	<label class="form-check-label">
		<input type="radio" class="form-check-input"  name="{{.ID}}" {{if ne .InitialValue ""}}checked="checked"{{end}} id="polyappJSRadioID` + strconv.FormatUint(rand.Uint64(), 10) + `" />{{.Label}}
	</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{if eq .Type "number"}}
<div class="form-group" id="{{.WrapperID}}">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<input type="number" {{if .Readonly}}readonly class="form-control"{{else}}class="form-control"{{end}} id="{{.ID}}" value="{{.InitialValue}}" />
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{if eq .Type "color"}}
<div class="form-group" id="{{.WrapperID}}">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<input type="color" {{if .Readonly}}readonly class="form-control"{{else}}class="form-control"{{end}} id="{{.ID}}" value="{{.InitialValue}}" style="max-width:200px;" />
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end -}}
{{end -}}
{{end}}
`
	t, err := template.New("inputTemplate").Parse(inputTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, inputs)
	if err != nil {
		return err
	}
	return nil
}

// SelectOption is one possible thing you could choose from a list.
type SelectOption struct {
	// Selected is whether this option begins selected.
	Selected bool
	// Value is what the field will be set to in the database. This must be populated.
	//
	// Suggested value: "No Select Options Found. This may be an error with how the UX has been constructed."
	Value string
	// ID is always optional; one will be generated for you if you do not provide one and one is needed.
	ID string
}

// SelectInput can be used to create an Input of type Select
type SelectInput struct {
	// WrapperID (optional) is the ID of the surrounding element
	WrapperID string
	// HelpText to guide users.
	HelpText string
	// ID of the input
	ID string
	// Label for the full selection list
	Label string
	// SelectOptions the labels of the options you can select (and their values when submitted to the server).
	SelectOptions []SelectOption
	// MultiSelect, if true, allows selecting multiple options
	MultiSelect bool
	// TransmitUpdateNow, if set, forces Polyapp to immediately update the server when this field's value changes.
	TransmitUpdateNow bool
}

// Validate ensures the correct fields are populated without validating the output.
func (s *SelectInput) Validate() error {
	if s.ID == "" {
		return errors.New("no ID")
	}
	if s.Label == "" {
		return errors.New("no Label")
	}
	if s.HelpText == "" {
		return errors.New("no Help Text")
	}
	if s.SelectOptions == nil || len(s.SelectOptions) < 1 {
		return errors.New("must have SelectOptions")
	}
	var escapeWriter bytes.Buffer
	template.HTMLEscape(&escapeWriter, []byte(s.Label))
	s.Label = escapeWriter.String()
	escapeWriter.Reset()
	template.HTMLEscape(&escapeWriter, []byte(s.HelpText))
	s.HelpText = escapeWriter.String()
	escapeWriter.Reset()
	return nil
}

// GenSelectInput generates an Input of type Select.
// TODO make this component keyboard accessible with things like aria-selected attribute.
func GenSelectInput(input SelectInput, writer io.Writer) error {
	err := input.Validate()
	if err != nil {
		return fmt.Errorf("GenSelectInput failed validation: %w", err)
	}
	input.ID = url.PathEscape(input.ID)
	if input.WrapperID == "" {
		input.WrapperID = common.GetRandString(15)
	}
	selectTemplate := `{{/* . is SelectInput  */}}
{{define "selectTemplate"}}
<div class="form-group" id="{{.WrapperID}}">
  	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<br>
	<select {{if .MultiSelect}}multiple{{end}} class="large-bootstrap-select polyappSelect{{if .TransmitUpdateNow}} polyappJSTransmitUpdateNow{{end}}" id="{{.ID}}" name="sellist" data-live-search="true" {{if .MultiSelect}}aria-multiselectable="true"{{else}}aria-multiselectable="false"{{end}}>
		{{range .SelectOptions}}<option class="pointer" {{if .Selected}}selected{{end}}>{{.Value}}</option>{{end}}
	</select>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{- end}}
`
	t, err := template.New("selectTemplate").Parse(selectTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, input)
	if err != nil {
		return err
	}
	return nil
}

// GiantOptionSelect can be used to create giant buttons which are tied to the same field.
type GiantOptionSelect struct {
	// WrapperID is the ID of the surrounding element
	WrapperID string
	// Name of the input should be the field we're writing to's name
	Name string
	// Label for the options
	Label string
	// SelectOptions the labels of the options you can select and their values when submitted to the server.
	SelectOptions []SelectOption
}

// Validate ensures the correct fields are populated without validating the output.
func (g GiantOptionSelect) Validate() error {
	if g.Name == "" {
		return errors.New("no Name means we can't identify this element")
	}
	if g.Label == "" {
		return errors.New("no Label")
	}
	if g.SelectOptions == nil || len(g.SelectOptions) < 1 {
		return errors.New("must have SelectOptions")
	}
	for i := range g.SelectOptions {
		if g.SelectOptions[i].Value == "" {
			return errors.New("value lacking in SelectOptions")
		}
	}
	return nil
}

// GenGiantOptionSelect generates an Input which allows you to select between some giant options. The input
// TODO make this component keyboard accessible. It probably needs JS. https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20160317/examples/radio/radio.html
func GenGiantOptionSelect(input GiantOptionSelect, writer io.Writer) error {
	err := input.Validate()
	if err != nil {
		return fmt.Errorf("GenGiantOptionSelect failed validation: %w", err)
	}
	for i := range input.SelectOptions {
		if input.SelectOptions[i].ID == "" {
			input.SelectOptions[i].ID = "polyappJSRadioID" + strconv.FormatUint(rand.Uint64(), 10)
		}
		input.SelectOptions[i].ID = url.PathEscape(input.SelectOptions[i].ID)
	}
	giantLabelID := "a" + strconv.FormatUint(rand.Uint64(), 10)
	selectTemplate := `{{/* . is SelectInput  */}}
{{define "giantOptionSelect"}}
<div id="{{.WrapperID}}" class="giantOptionWrapper" role="radiogroup" aria-labelledby="` + giantLabelID + `">
	<label class="giantOptionLabel" id="` + giantLabelID + `">{{.Label}}</label>
	{{range $index, $element := .SelectOptions}}
	<input type="radio" id="{{.ID}}" name="` + input.Name + `" {{if .Selected}}checked aria-checked="true"{{else}}aria-checked="false"{{end}} value="{{.Value}}" />
	<div class="giantOption">
		<label for="{{.ID}}" role="radio" tabindex="{{if eq $index 0}}0{{else}}-1{{end}}" class="maxWidthHeight">
			<span class="centerElement">{{.Value}}</span>
		</label>
	</div>
	{{end}}
</div>
{{- end}}
`
	t, err := template.New("giantOptionSelect").Parse(selectTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, input)
	if err != nil {
		return err
	}
	return nil
}

// ListLabeledInput constructs a list of labeled input controls.
type ListLabeledInput struct {
	// HelpText is shown along with the control.
	HelpText string
	// Label is the Label of the input. There is only one label for the list.
	Label string
	// ID is the ID base of the inputs. The actual IDs will be {{.ID}}_# or {{.ID}} in the case of the wrapper ID.
	ID string
	// InitialValues is the initial value of the fields.
	InitialValues []string
	// DefaultValue is the default value of the fields if they are created anew.
	DefaultValue string
	// Type is the type of the input. "text", "number", "checkbox", "ref"
	Type string
	// Readonly disables all buttons in this control
	Readonly bool
}

func (l ListLabeledInput) Validate() error {
	labeledInput := LabeledInput{
		WrapperID:    l.ID + "Wrapper",
		HelpText:     l.HelpText,
		Label:        l.Label,
		ID:           l.ID,
		InitialValue: l.DefaultValue,
		Type:         l.Type,
	}
	err := labeledInput.Validate()
	if err != nil {
		return fmt.Errorf("labeledInput validation failure: %w", err)
	}

	// InitialValues should match the input type
	for _, iv := range l.InitialValues {
		switch l.Type {
		case "text":
			return nil
		case "textarea":
			return errors.New("unimplemented")
		case "checkbox":
			if iv != "" && iv != "true" && iv != "false" {
				return errors.New("checkbox input can only have values of empty string, true or false")
			}
			return nil
		case "number":
			if iv != "" {
				_, err := strconv.ParseFloat(iv, 64)
				if err != nil {
					return fmt.Errorf("number inputs must have initial values which are numbers: %w", err)
				}
			}
			return nil
		case "ref":
			if iv != "" {
				_, err := common.ParseRef(iv)
				if err != nil {
					return fmt.Errorf("ref inputs must be parseable as refs %v: %w", iv, err)
				}
			}
		default:
			// select type should use type SelectInput
			return errors.New("unhandled LabeledInput.Type: " + l.Type)
		}
	}
	return nil
}

// genListItem returns a new list item of the given type with the next incremental ID, or _1 if the last ID was not numbered.
//
// lastID is a string like "1" or "7" or "".
//
// LabeledInputType is one of: "text", "number", "checkbox".
//
// initialValue is some value to assign inside the field.
//
// labelAria is some label text for the element; likely the Label for the whole list.
//
// IDPrefix is prepended to all IDs. This is useful for identifying the polyappJSDisplayNone is part of a simple list.
func genListItem(lastID string, LabeledInputType string, initialValue string, labelAria string, IDPrefix string, readonly bool) (listItem string, newID string, err error) {
	lastIDSplit := strings.Split(lastID, "_")
	if len(lastIDSplit) == 4 {
		newID = lastID + "_1"
	} else if len(lastIDSplit) == 5 && len(lastIDSplit[0]) == 0 {
		newID = lastID + "_1"
	} else if len(lastIDSplit) == 5 {
		lastIDInt, err := strconv.Atoi(lastIDSplit[4])
		if err != nil {
			return "", "", errors.New("provided lastID was not a number. LastID: " + lastID + " and error from converter: " + err.Error())
		}
		lastIDInt += 1
		newID = lastIDSplit[0] + "_" + lastIDSplit[1] + "_" + lastIDSplit[2] + "_" + lastIDSplit[3] + "_" + strconv.Itoa(lastIDInt)
	} else if len(lastIDSplit) == 6 {
		lastIDInt, err := strconv.Atoi(lastIDSplit[5])
		if err != nil {
			return "", "", errors.New("provided lastID was not a number. LastID: " + lastID + " and error from converter: " + err.Error())
		}
		lastIDInt += 1
		newID = "_" + lastIDSplit[1] + "_" + lastIDSplit[2] + "_" + lastIDSplit[3] + "_" + lastIDSplit[4] + "_" + strconv.Itoa(lastIDInt)
	} else {
		return "", "", errors.New("provided lastID was misformatted. LastID: " + lastID)
	}

	ReadonlyString := ""
	if readonly {
		ReadonlyString = " readonly"
	}

	var w bytes.Buffer
	err = GenIconButton(IconButton{
		ID:        IDPrefix + "polyappJShtml5sortable-remove" + newID,
		ImagePath: "/assets/icons/red_minus.png",
		Text:      "",
		AltText:   "Remove List Item",
		Readonly:  readonly,
	}, &w)
	if err != nil {
		return "", "", fmt.Errorf("GenIconButton err: %w", err)
	}

	bodyOfList := ""
	switch LabeledInputType {
	case "text":
		bodyOfList = `<div class="input-group">
	<input id="` + IDPrefix + newID + `" type="` + LabeledInputType + `" class="form-control cursorAuto" value="` + initialValue + `" aria-label="` + labelAria + `"` + ReadonlyString + ` />
		<div class="input-group-append">
		` + w.String() + `
		</div>
	</div>`
	case "number":
		bodyOfList = `<div class="input-group">
	<input id="` + IDPrefix + newID + `" type="` + LabeledInputType + `" class="form-control cursorAuto" value="` + initialValue + `" aria-label="` + labelAria + `"` + ReadonlyString + ` />
		<div class="input-group-append">
		` + w.String() + `
		</div>
	</div>`
	case "checkbox":
		checkedText := ""
		if initialValue != "" {
			checkedText = "checked"
		}
		bodyOfList = `<div class="input-group"><div class="form-check">
	<label class="form-check-label centerElement cursorAuto">
		<input type="checkbox" class="form-check-input" value="" ` + checkedText + ` id="` + IDPrefix + newID + `"` + ReadonlyString + ` />{{.Label}}
	</label>
</div>
<div class="input-group-append">
	` + w.String() + `
</div>
</div>`
	case "ref":
		bodyOfList = `<div class="input-group">
	<div class="input-group-prepend">
		<a id="` + IDPrefix + newID + `" href="` + initialValue + `" class="btn btn-link"><i class="fa fa-link"></i></a>
	</div>
	<input id="` + IDPrefix + newID + `" type="` + LabeledInputType + `" class="form-control cursorAuto" value="` + initialValue + `" aria-label="` + labelAria + `"` + ReadonlyString + ` />
	<div class="input-group-append">
	` + w.String() + `
	</div>
</div>`
	default:
		return "", "", errors.New("unhandled LabeledInputType: " + LabeledInputType)
	}

	return `<div class="list-group-item list-group-item-dark list-group-item-action grabbable">
	` + bodyOfList + `
</div>`, newID, nil

}

// GenListLabeledInput is an ordered list of elements attached to a single label and field.
// labeledInput it used as a template for what a singleton of this list should look like.
// Each element in the list is given the ID_# where # starts at 1 and increases sequentially.
// There is only one wrapper, label, initial value, and type.
func GenListLabeledInput(labeledInput ListLabeledInput, writer io.Writer) error {
	err := labeledInput.Validate()
	if err != nil {
		return fmt.Errorf("GenListLabeledInput validation error: %w", err)
	}
	currentID := url.PathEscape(labeledInput.ID)
	listItems := ""
	var newListItem string
	for _, v := range labeledInput.InitialValues {
		newListItem, currentID, err = genListItem(currentID, labeledInput.Type, v, labeledInput.Label, "", labeledInput.Readonly)
		listItems += newListItem
		if err != nil {
			return fmt.Errorf("error in genListItem: %w", err)
		}
	}
	// displayNoneListItem is what a new list item would look like, were we to try to add one.
	// It's going to be encased in a display none div.
	// And if a user wishes to add a new node, this value is copied to the end of the list, unwrapped.
	displayNoneListItem, _, err := genListItem(currentID, labeledInput.Type, labeledInput.DefaultValue, labeledInput.Label, "polyappJSDisplayNone", labeledInput.Readonly)
	if err != nil {
		return fmt.Errorf("err in genListItem for display none purposes: %w", err)
	}

	var addImgButton bytes.Buffer
	err = GenIconButton(IconButton{
		ID:        "polyappJShtml5sortable-add" + currentID,
		ImagePath: "/assets/icons/add-24px.svg",
		Text:      labeledInput.Label,
		AltText:   "Add",
		Readonly:  labeledInput.Readonly,
	}, &addImgButton)
	if err != nil {
		return fmt.Errorf("err in GenIconButton for adding purposes")
	}

	labeledInput.ID = url.PathEscape(labeledInput.ID)

	// dependent upon JS in main.js which listens to clicks on class .js-add-item-button
	// and then makes a copy of
	inputTemplate := `{{/* . is ListLabeledInput  */}}
{{define "inputTemplate"}}
<div class="form-group" id="{{.ID}}">
	<label>{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="list-group html5sortable">
	` + listItems + `
	</div>
	<div class="displayNone">` + displayNoneListItem + `</div>
	` + addImgButton.String() + `
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end}}
`
	t, err := template.New("inputTemplate").Parse(inputTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, labeledInput)
	if err != nil {
		return err
	}
	return nil
}

// SearchLabeledInput is similar to a labeled input except when a user types into the
// box a search query is initiated and the query results appear below what has been typed.
// This control only supports "text" inputs and all queries are assumed to be trying to find duplicates.
// Wrapper element is always set to {{.ID}}-Wrapper
type SearchLabeledInput struct {
	// Label is the Label of the input.
	Label string
	// HelpText for this input.
	HelpText string
	// ID is the ID of the field.
	ID string
	// InitialValues is the initial value of the field.
	InitialValue string
	// QueryPrefixedField is an array field you are querying for a value across. Ex: polyapp_Schema_polyappSchema_DataKeys.
	// If the field is not found or it is an Array, this will result in an error.
	QueryPrefixedField string
}

func (l SearchLabeledInput) Validate() error {
	labeledInput := LabeledInput{
		WrapperID:    l.ID + "-Wrapper",
		HelpText:     l.HelpText,
		Label:        l.Label,
		ID:           l.ID,
		InitialValue: l.InitialValue,
		Type:         "text",
	}
	err := labeledInput.Validate()
	if err != nil {
		return fmt.Errorf("labeledInput validation failure: %w", err)
	}

	return nil
}

// GenSearchLabeledInput generates a text control which has some associated JS which can display search results as you
// type them.
func GenSearchLabeledInput(searchLabeledInput SearchLabeledInput, writer io.Writer) error {
	err := searchLabeledInput.Validate()
	if err != nil {
		return fmt.Errorf("GenSearchLabeledInput validation error: %w", err)
	}
	searchLabeledInput.ID = url.PathEscape(searchLabeledInput.ID)

	// Notes on how this works
	// 1. I considered using Bloodhound to make this, but it's a lot of JS & is really suited best for limited numbers of search results so you
	// can load all of the search results into memory.
	// 2. I assume that when a user types into the input field Go will be notified immediately and then Go can respond
	// with the appropriate results created from GenSearchLabledInputResult inserted into the DOM with 'beforeend'
	inputTemplate := `{{/* . is []LabeledInput  */}}
{{define "searchLabeledInputTemplate"}}
<div id="{{.ID}}-Wrapper" class="mb-4">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
		<input type="text" class="form-control" id="{{.ID}}" value="{{.InitialValue}}" data-searchable="true" aria-autocomplete="list" />
		<div class="input-group-append">
			<span class="input-group-text" id="basic-addon1">
				<img src="/assets/icons/search-24px.svg" aria-hidden="true" height="24px" width="24px" />
			</span>
		</div>
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end}}
`
	t, err := template.New("searchLabeledInputTemplate").Parse(inputTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, searchLabeledInput)
	if err != nil {
		return err
	}
	return nil
}

// SearchLabeledInputResult should be inserted into the DOM beforeend WrapperID for searchLabeledInputTemplate
type SearchLabeledInputResult struct {
	// Results are strings which are the search results. When clicked the input box is populated with these values.
	Results []string
	// SearchLabeledInputID is the ID of the input this is being inserted underneath.
	//
	// Ex. results
	SearchLabeledInputID string
}

func (s SearchLabeledInputResult) Validate() error {
	if s.Results == nil {
		return errors.New("Results was nil")
	}
	if s.SearchLabeledInputID == "" {
		return errors.New("SearchLabeledInputID was empty")
	}
	return nil
}

// GenSearchLabeledInputResults generates some UI used in search results.
//
// Use DeleteSelector: "#{{.SearchLabeledInputID}}-ResultsWrapper".
//
// Use InsertSelector: "#{{.SearchLabeledInputID}}-Wrapper".
//
// Use Action: "beforeend"
func GenSearchLabeledInputResults(searchLabeledInputResult SearchLabeledInputResult, writer io.Writer) error {
	err := searchLabeledInputResult.Validate()
	if err != nil {
		return fmt.Errorf("GenSearchLabeledInput validation error: %w", err)
	}
	// result is simlar to SearchLabeledInputResult but it's easier for text/template to parse & fixes duplicate IDs.
	type result struct {
		Result               string
		SearchLabeledInputID string
	}
	allResults := make([]result, len(searchLabeledInputResult.Results))
	for i, v := range searchLabeledInputResult.Results {
		allResults[i] = result{
			Result:               v,
			SearchLabeledInputID: searchLabeledInputResult.SearchLabeledInputID + "_" + strconv.FormatUint(rand.Uint64(), 10),
		}
	}
	inputTemplate := `{{define "searchLabeledInputTemplateResult"}}
<div class="list-group list-group-flush autocompleteResultsWrapper" role="listbox" id="` + searchLabeledInputResult.SearchLabeledInputID + `-ResultsWrapper">
{{- range . -}}
	<a class="list-group-item list-group-item-action pointer" id="polyappJSSearchableItem{{.SearchLabeledInputID}}" role="option" tabindex="0">{{.Result}}</a>
{{- end}}
</div>
{{end}}
`
	t, err := template.New("searchLabeledInputTemplateResult").Parse(inputTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, allResults)
	if err != nil {
		return err
	}
	return nil
}

type Time struct {
	ID       string
	Label    string
	HelpText string
	// InitialValue is a UNIX Timestamp in UTC / GMT.
	// Javascript will convert the timestamp into the local time & back out to UTC again.
	InitialValue int64
	// Format is "L" for date only or "LT" for time only. If empty, uses date and time.
	// Additional options from https://momentjs.com/docs/#/displaying/format/ include: LTS, l, LL, ll, LLL, lll, LLLL, llll
	Format            string
	Readonly          bool
	InitializeWithNow bool // if true will initialize the input with the current date and time.
}

func (t Time) Validate() error {
	if t.ID == "" {
		return errors.New("ID was empty")
	}
	if t.Label == "" {
		return errors.New("Label was empty")
	}
	if t.HelpText == "" {
		return errors.New("Help Text was empty")
	}
	switch t.Format {
	case "", "L", "LT", "LTS", "l", "LL", "ll", "LLL", "lll", "LLLL", "llll":
	default:
		return errors.New("unsupported format: " + t.Format)
	}
	return nil
}

// GenTime generates a date + time control by default. It can also generate a date-only control
// or a time-only control. https://getdatepicker.com/4/
func GenTime(ti Time, writer io.Writer) error {
	err := ti.Validate()
	if err != nil {
		return fmt.Errorf("GenTime validation error: %w", err)
	}
	ti.ID = url.PathEscape(ti.ID)
	var formattedInput struct {
		ID                string
		IDCSSEscaped      string
		Label             string
		HelpText          string
		FormattedValue    string
		Readonly          bool
		Format            string
		InitializeWithNow bool
	}
	formattedInput.ID = ti.ID
	formattedInput.IDCSSEscaped = common.CSSEscape(ti.ID)
	formattedInput.Label = ti.Label
	formattedInput.HelpText = ti.HelpText
	formattedInput.FormattedValue = strconv.FormatInt(ti.InitialValue, 10)
	formattedInput.Readonly = ti.Readonly
	formattedInput.Format = ti.Format
	formattedInput.InitializeWithNow = ti.InitializeWithNow
	inputTemplate := `{{define "time"}}
<div class="form-group">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group date polyappDateTime" {{if .Readonly}}readonly{{end}} id="{{.ID}}" data-target-input="nearest" data-polyappjsdatetimeformat="{{.Format}}">
		<input {{if .Readonly}}disabled="true"{{end}} type="text" class="form-control datetimepicker-input{{if .InitializeWithNow}} polyappDateTimeInitializeWithNow{{end}}" data-target="#{{.IDCSSEscaped}}" value="{{.FormattedValue}}" />
		<div class="input-group-append" data-target="#{{.IDCSSEscaped}}" data-toggle="datetimepicker">
{{if eq .Format "LT"}}			<div class="input-group-text"><i class="fa fa-clock-o"></i></div>
{{else}}			<div class="input-group-text"><i class="fa fa-calendar"></i></div>{{end}}
		</div>
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput"></div>
</div>
{{end}}
`
	t, err := template.New("time").Parse(inputTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, formattedInput)
	if err != nil {
		return err
	}
	return nil
}
