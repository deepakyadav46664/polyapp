package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"
)

type GeolocationAPI struct {
	ID       string
	Label    string
	HelpText string
}

func (g GeolocationAPI) Validate() error {
	if g.ID == "" {
		return errors.New("ID was empty")
	}
	if g.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if g.Label == "" {
		return errors.New("Label was empty")
	}
	return nil
}

// GenGeolocationAPI generates a read-only field which holds Latitude and Longitude coordinates
// of the position which was set when "Set Position" button was pressed.
// It also generates the "Set Position" button.
func GenGeolocationAPI(g GeolocationAPI, w io.Writer) error {
	err := g.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	g.ID = url.PathEscape(g.ID)

	geolocationTemplate := `<div class="mb-4">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
		<input type="text" class="form-control" readonly id="{{.ID}}" />
		<div class="input-group-append">
			<div class="input-group-text polyappjsgeolocationrecord pointer">
				<i class="fa fa-compass"></i>
			</div>
		</div>
	</div>
</div>
`
	t, err := template.New("").Parse(geolocationTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, g)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
