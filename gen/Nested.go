package gen

import (
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/url"
	"strconv"
	"text/template"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// NestedAccordion wraps a nested component which can contain any number of fields and other things inside
// an Accordion such that users can expand or contract the accordion pieces to see or hide the Ref.
//
// WrapperID is automatically assigned. There is no way to access the accordion elements via this component.
type NestedAccordion struct {
	// TitleField is the field name (id) whose value is shown when the section is collapsed or expanded.
	TitleField string
	// TitleStatic (optional) is used instead of TitleField. It's a static title for this section.
	TitleStatic string
	// Ref is a Reference to the document which InnerContent contains. Documentation about Ref format can be found under common.Data
	Ref string
	// InnerContent is placed inside the Accordion area when the Accordion is expanded.
	InnerContent string
}

// Validate ensures the correct fields are populated without validating the output.
func (n NestedAccordion) Validate() error {
	if n.TitleField == "" && n.TitleStatic == "" {
		return errors.New("Both TitleField and TitleStatic were empty")
	}
	if n.InnerContent == "" {
		return errors.New("InnerContents was empty")
	}
	if n.Ref == "" {
		return errors.New("Ref was empty")
	}
	err := common.ValidateRef(n.Ref)
	if err != nil {
		return fmt.Errorf("ValidateRef: %w", err)
	}
	return nil
}

// GenNestedAccordion wraps a nested component which can contain any number of fields and other things inside
// an Accordion such that users can expand or contract the accordion pieces to see or hide the Ref.
//
// Because this is a layout component it uses text/template instead of html/template.
func GenNestedAccordion(nestedAccordions []NestedAccordion, writer io.Writer) error {
	if nestedAccordions == nil || len(nestedAccordions) == 0 {
		return errors.New("nestedAccordion was nil or empty")
	}
	for i, n := range nestedAccordions {
		err := n.Validate()
		if err != nil {
			return fmt.Errorf("nestedAccordion failed validation at index "+strconv.Itoa(i)+": %w", err)
		}
	}

	nestedAccordionsWithIDs := make([]struct {
		TitleField        string
		PolyappTitleField string
		TitleStatic       string
		InnerContent      string
		HeaderID          string
		ContentID         string
		PolyappContentID  string // the version of ContentID which existed before we mangled the ID with a suffix
		ContentIDEscaped  string
	}, len(nestedAccordions))
	for i := range nestedAccordions {
		nestedAccordionsWithIDs[i].TitleStatic = nestedAccordions[i].TitleStatic
		nestedAccordionsWithIDs[i].TitleField = url.PathEscape(nestedAccordions[i].TitleField + "_" + common.GetRandString(10))
		nestedAccordionsWithIDs[i].PolyappTitleField = url.PathEscape(nestedAccordions[i].TitleField)
		nestedAccordionsWithIDs[i].InnerContent = nestedAccordions[i].InnerContent
		nestedAccordionsWithIDs[i].HeaderID = "polyappAccordionHeader" + strconv.Itoa(rand.Int())
		// polyappRef prefix is recognized in main.js as prefixing a Ref's Data ID.
		nestedAccordionsWithIDs[i].ContentID = url.PathEscape("polyappRef" + nestedAccordions[i].Ref + "_" + strconv.Itoa(rand.Int()))
		nestedAccordionsWithIDs[i].PolyappContentID = url.PathEscape("polyappRef" + nestedAccordions[i].Ref)
		nestedAccordionsWithIDs[i].ContentIDEscaped = common.CSSEscape(nestedAccordionsWithIDs[i].ContentID)
	}

	accordionID := "polyappAccordionWrapper" + strconv.Itoa(rand.Int()) // created in such a way that CSS escaping is unnecessary

	nestedAccordionTemplate := `{{define "nestedAccordionTemplate"}}
<div class="accordion" id="` + accordionID + `">
{{range .}}
{{/*overflow: visible; is necessary to override .accordion > .card style overflow: hidden; which unfortunately prevents bootstrap-select from floating over all parent content*/}}
<div class="card cursorAuto" style="overflow: visible;">
	<div id="{{.HeaderID}}" data-readonly="true" class="d-flex justify-content-between align-items-center">
		<h2 class="mb-0">
			<button class="btn btn-link collapsed" type="button" id="{{.TitleField}}" data-polyappID="{{.PolyappTitleField}}" data-toggle="collapse" data-target="#{{.ContentIDEscaped}}" data-readonly="true" aria-expanded="false" aria-controls="{{.ContentID}}">
			{{.TitleStatic}}
			</button>
		</h2>
		<a class="btn btn-link polyappjs-accordion-link"><i class="fa fa-link"></i></a>
	</div>{{/* the exact structure </h2> ... </div> is referenced in actions.go */}}
	<div id="{{.ContentID}}" data-polyappID="{{.PolyappContentID}}" class="collapse" aria-labelledby="{{.HeaderID}}" data-parent="#` + accordionID + `">
		<div class="card-body py-0">{{.InnerContent}}</div>
	</div>
</div>
{{- end -}}
</div>
{{- end}}
`
	t, err := template.New("nestedAccordionTemplate").Parse(nestedAccordionTemplate)
	if err != nil {
		return fmt.Errorf("parsing: %w", err)
	}
	err = t.Execute(writer, nestedAccordionsWithIDs)
	if err != nil {
		return fmt.Errorf("executing: %w", err)
	}
	return nil
}
