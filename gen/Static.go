package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"math/rand"
	"net/url"
	"strconv"
	textTemplate "text/template"
)

// StaticContent has information for generating static content.
// Don't use this for regular content creation - only creation via code.
type StaticContent struct {
	// ID (optional) is the ID of the over-arching element all content will be placed in to
	ID string
	// Content is the textual content being created
	Content string
	// Tag is the tag being used for the content from this list: h1 h2 h3 h4 h5 h6 p small (secondary text in heading)
	// mark (yellow background) blockquote code
	Tag string
}

// Validate ensures the correct fields are populated without validating the output.
func (g StaticContent) Validate() error {
	if g.Content == "" {
		return errors.New("no Content provided")
	}
	if g.Tag == "" {
		return errors.New("no Tag provided")
	}
	switch g.Tag {
	case "h1":
		return nil
	case "h2":
		return nil
	case "h3":
		return nil
	case "h4":
		return nil
	case "h5":
		return nil
	case "h6":
		return nil
	case "p":
		return nil
	case "small":
		return nil
	case "mark":
		return nil
	case "blockquote":
		return nil
	case "code":
		return nil
	default:
		return errors.New("unrecognized static content type")
	}
}

// GenStaticContent generates some static stuff
func GenStaticContent(staticContent StaticContent, writer io.Writer) error {
	err := staticContent.Validate()
	if err != nil {
		return fmt.Errorf("StaticContent failed validation: %w", err)
	}
	staticContent.ID = url.PathEscape(staticContent.ID)
	staticContentTemplate := `{{/* . is StaticContent  */}}
{{define "staticContent"}}
<{{.Tag}} {{if .ID}}id="{{.ID}}"{{end}}>{{.Content}}</{{.Tag}}>
{{- end}}
`
	t, err := textTemplate.New("staticContent").Parse(staticContentTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, staticContent)
	if err != nil {
		return err
	}
	return nil
}

// Alert generates markup for a very visible alert.
type Alert struct {
	// Text is the text of the error
	Text string
	// Severity is the severity of the error: "success", "info", "warning", "danger", "primary", "secondary", "dark", "light"
	Severity string
}

// Validate ensures the correct fields are populated without validating the output.
func (i Alert) Validate() error {
	if i.Text == "" {
		return errors.New("no Text provided")
	}
	if i.Severity == "" {
		return errors.New("no Severity provided")
	}
	switch i.Severity {
	case "success":
		return nil
	case "info":
		return nil
	case "warning":
		return nil
	case "danger":
		return nil
	case "primary":
		return nil
	case "secondary":
		return nil
	case "dark":
		return nil
	case "light":
		return nil
	default:
		return errors.New("severity was not recognized")
	}
}

// GenAlert generates markup for an Alert. This could be inserted after some text
// to indicate a validation failure.
func GenAlert(alert Alert, w io.Writer) error {
	err := alert.Validate()
	if err != nil {
		return fmt.Errorf("SelectInput failed validation: %w", err)
	}
	alertTemplate := `{{/* . is Alert  */}}
{{define "alertTemplate"}}
<div id="a` + strconv.FormatUint(rand.Uint64(), 10) + `" class="alert alert-{{.Severity}}">
	{{.Text}}
</div>
{{- end}}
`
	t, err := template.New("alertTemplate").Parse(alertTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, alert)
	if err != nil {
		return err
	}
	return nil
}

// FormError generates markup for a validation error.
type FormError struct {
	// Text is the text of the error
	Text string
}

// Validate ensures the correct fields are populated without validating the output.
func (f FormError) Validate() error {
	if f.Text == "" {
		return errors.New("no Text provided")
	}
	return nil
}

// GenFormError generates markup for a little thing that goes under forms which have an error. To use this properly,
// you will also need to...
func GenFormError(formError FormError, w io.Writer) error {
	err := formError.Validate()
	if err != nil {
		return fmt.Errorf("FormError Validation: %w", err)
	}
	formErrorTemplate := `{{define "formErrorTemplate"}}
<div id="a` + strconv.FormatUint(rand.Uint64(), 10) + `" class="invalid-feedback displayBlock">
	{{.Text}}
</div>
{{- end}}
`
	t, err := template.New("formErrorTemplate").Parse(formErrorTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, formError)
	if err != nil {
		return err
	}
	return nil
}
