package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"
)

// IFrame can be used to create a simple IFrame
type IFrame struct {
	// ID of the IFrame
	ID string
	// Source is the website the IFrame is showing
	Source string
	// Title describes the contents inside the IFrame.
	Title string
}

// Validate ensures the correct fields are populated without validating the output.
func (i IFrame) Validate() error {
	if i.ID == "" {
		return errors.New("no ID provided")
	}
	if i.Source == "" {
		return errors.New("no Source provided")
	}
	if i.Title == "" {
		return errors.New("no Title provided")
	}
	return nil
}

// GenIFrame generates an IFrame in Template.
func GenIFrame(IFrame IFrame, writer io.Writer) error {
	err := IFrame.Validate()
	if err != nil {
		return fmt.Errorf("GenIFrame failed validation: %w", err)
	}
	IFrame.ID = url.PathEscape(IFrame.ID)
	IFrameTemplate := `{{/* . is IFrame  */}}
{{define "IFrameTemplate"}}
<iframe src="{{.Source}}" title="{{.Title}}" id="{{.ID}}" width="100%" height="90%" class="polyappIFrame"></iframe>
{{- end}}
`
	t, err := template.New("IFrameTemplate").Parse(IFrameTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, IFrame)
	if err != nil {
		return err
	}
	return nil
}
