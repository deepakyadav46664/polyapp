package gen

import (
	"errors"
	"fmt"
	"io"
	"text/template"
)

type PredictFloat struct {
	ID    string
	Title string
}

func (p PredictFloat) Validate() error {
	if p.ID == "" {
		return errors.New("ID was empty")
	}
	if p.Title == "" {
		return errors.New("Title was empty")
	}
	return nil
}

func GenPredictFloat(f PredictFloat, w io.Writer) error {
	err := f.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	FullTextSearchTemplate := `
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="polyappJSPredictComponent" data-polyappjs-predict-id="{{.ID}}" data-polyappjs-predict-title="{{.Title}}">
	<div class="bg-light text-center">
      <h4 class="text-primary m-0">{{.Title}}</h4>
	</div>
	<div class="bg-light">
	  <canvas id="{{.ID}}" class="polyappJSPredictCanvas"></canvas>
	</div>
</div>

<script>
var makeChart = function(title, ID, initialValue1, initialValue2) {
const data = {
  datasets: [{
    label: title,
    data: [initialValue1, initialValue2],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(0,0,0,0)'
    ],
    hoverOffset: 4
  }]
};
const config = {
  type: 'doughnut',
  data: data,
  options: {
	responsive:true,
	maintainAspectRatio: false,
	plugins: {
		tooltip: {
			enabled: false,
		}
	},
	elements: {
  	  center: {
		  text: title,
		  color: '#FF6384', // Default is #000000
		  fontStyle: 'Arial', // Default is Arial
		  sidePadding: 20, // Default is 20 (as a percentage)
		  minFontSize: 20, // Default is 20 (in px), set to false and text will not wrap.
		  lineHeight: 25 // Default is 25 (in px), used for when text wraps
      }
    }
},
	plugins: [{
        beforeDraw: function(chart) {
    if (chart.config.options.elements.center) {
      // Get ctx from string
      var ctx = chart.ctx;

      // Get options from the center object in options
      var centerConfig = chart.config.options.elements.center;
      var fontStyle = centerConfig.fontStyle || 'Arial';
      var txt = centerConfig.text;
      var color = centerConfig.color || '#000';
      var maxFontSize = centerConfig.maxFontSize || 75;
      var sidePadding = centerConfig.sidePadding || 20;
      var sidePaddingCalculated = (sidePadding / 100) * (chart.getDatasetMeta(0).controller.innerRadius * 2)
      // Start with a base font of 30px
      ctx.font = "30px " + fontStyle;

      // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      var stringWidth = ctx.measureText(txt).width;
      var elementWidth = (chart.getDatasetMeta(0).controller.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      var widthRatio = elementWidth / stringWidth;
      var newFontSize = Math.floor(30 * widthRatio);
      var elementHeight = (chart.getDatasetMeta(0).controller.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      var fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
      var minFontSize = centerConfig.minFontSize;
      var lineHeight = centerConfig.lineHeight || 25;
      var wrapText = false;

      if (minFontSize === undefined) {
        minFontSize = 20;
      }

      if (minFontSize && fontSizeToUse < minFontSize) {
        fontSizeToUse = minFontSize;
        wrapText = true;
      }

      // Set font settings to draw it correctly.
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      ctx.font = fontSizeToUse + "px " + fontStyle;
      ctx.fillStyle = color;

      if (!wrapText) {
        ctx.fillText(txt, centerX, centerY);
        return;
      }

      var words = txt.split(' ');
      var line = '';
      var lines = [];

      // Break words up into multiple lines if necessary
      for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = ctx.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > elementWidth && n > 0) {
          lines.push(line);
          line = words[n] + ' ';
        } else {
          line = testLine;
        }
      }

      // Move the center up depending on line height and number of lines
      centerY -= (lines.length / 2) * lineHeight;

      for (var n = 0; n < lines.length; n++) {
        ctx.fillText(lines[n], centerX, centerY);
        centerY += lineHeight;
      }
      //Draw text in center
      ctx.fillText(line, centerX, centerY);
    }
  }
    }]
};

  var ctx = document.getElementById(ID);
 var myChart = new Chart(ctx, config);
	return myChart;
}
var movePredictions = function() {
	let els = document.getElementsByClassName('polyappJSPredictComponent');
	let predictSlot = document.getElementsByClassName('polyappJSPredictContainer');
	if (els != null && predictSlot != null && els.length > 0 && predictSlot.length === 1) {
		for (let i=0;i<els.length;i++) {
			predictSlot.item(0).appendChild(els[i]);
		}
	}
}
var doWorkAtLoad = function() {
	var chartReference = makeChart('', '{{.ID}}', '', '');

	movePredictions();
}
window.addEventListener('load', doWorkAtLoad);

</script>
`
	t, err := template.New("").Parse(FullTextSearchTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, f)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
