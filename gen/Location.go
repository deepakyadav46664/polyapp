package gen

import (
	"errors"
	"fmt"
	"io"
	"net/url"
	"text/template"
)

type PlaceAutocomplete struct {
	// Label is the Label of the input
	Label string
	// HelpText for this field
	HelpText string
	// ID is the ID of the actual input
	ID string
	// Readonly is optional. If set, the field becomes read-only.
	Readonly bool
	// APIKey is optional. If not set, the key used by Polyapp is used.
	APIKey string
}

func (p PlaceAutocomplete) Validate() error {
	if p.ID == "" {
		return errors.New("ID is required")
	}
	if p.HelpText == "" {
		return errors.New("HelpText is required")
	}
	if p.Label == "" {
		return errors.New("Label is required")
	}
	return nil
}

// GenPlaceAutocomplete generates a Google Place Autocomplete form field.
func GenPlaceAutocomplete(PlaceAutocomplete PlaceAutocomplete, writer io.Writer) error {
	err := PlaceAutocomplete.Validate()
	if err != nil {
		return fmt.Errorf("GenPlaceAutocomplete failed validation: %w", err)
	}
	PlaceAutocomplete.ID = url.PathEscape(PlaceAutocomplete.ID)
	if PlaceAutocomplete.APIKey == "" {
		PlaceAutocomplete.APIKey = "AIzaSyAjfSvhk776VYkLzetfslKb__qjw18ogTs"
	}
	// Address format info: https://developers.google.com/maps/documentation/javascript/reference/places-service#PlaceResult
	IconButtonTemplate := `
<div class="form-group">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
        <img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
		{{if .Readonly}}<input type="text" readonly class="form-control" id="{{.ID}}" />
		{{- else -}}
		<input id="{{.ID}}" type="text" placeholder="Enter a location" class="form-control" />
		{{- end}}
		<div class="input-group-append">
			<div class="input-group-text">
				<i class="fa fa-location-arrow"></i>
			</div>
		</div>
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key={{.APIKey}}&callback=initMap&libraries=places&v=weekly" async></script>
<script>
function initMap() {
	const i = document.getElementById("{{.ID}}");
	const options = {
    	componentRestrictions: { country: "us" },
    	strictBounds: false,
	}
	const autocomplete = new google.maps.places.Autocomplete(i, options);
	autocomplete.addListener("place_changed", () => {
		const place = autocomplete.getPlace();
		if (!place.geometry || !place.geometry.location) {
		  // User entered the name of a Place that was not suggested and
		  // pressed the Enter key, or the Place Details request failed.
		  window.alert("No details available for input: '" + place.name + "'");
		  return;
		}
		setAllData(getDataRef(i), getAllDataID(i.id), place.formatted_address, i.id);
	});
}
</script>
`
	t, err := template.New("IconButtonTemplate").Parse(IconButtonTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, PlaceAutocomplete)
	if err != nil {
		return err
	}
	return nil
}
