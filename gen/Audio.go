package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// AudioPlayer allows generating a control which can play audio.
type AudioPlayer struct {
	Title    string // Title describes the audio.
	Subtitle string // Subtitle could include the artist's name or other information.
	Style    string // Style is either "light" or "dark"
	Src      string // Src is the full path to the audio file we'll be playing.
}

// Validate returns an error if the AudioPlayer object is not valid.
func (a AudioPlayer) Validate() error {
	if a.Title == "" {
		return errors.New("Title is required")
	}
	if a.Subtitle == "" {
		return errors.New("Subtitle is required")
	}
	if a.Style != "light" && a.Style != "dark" {
		return errors.New("Style must be 'light' or 'dark'")
	}
	if a.Src == "" {
		return errors.New("Src must be provided")
	}
	return nil
}

// GenAudioPlayer allows generating a control which can play audio.
func GenAudioPlayer(a AudioPlayer, w io.Writer) error {
	err := a.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	// according to my research, ogg is better than mp3; mp3 is better than wav.
	// There is currently NO REASON to provide a WAV file. Instead, best-practice is to provide 2 <audio></audio>.
	// The first is an ogg, and the second an mp3. TODO make that possible by converting uploaded files to ogg and modifying this
	audioType := "mpeg"
	srcToLower := strings.ToLower(a.Src)
	if strings.HasSuffix(srcToLower, ".mp3") || strings.HasSuffix(srcToLower, ".mpeg") {
		audioType = "mpeg"
	} else if strings.HasSuffix(srcToLower, ".ogg") {
		audioType = "ogg"
	} else if strings.HasSuffix(srcToLower, ".wav") || strings.HasSuffix(srcToLower, ".wave") {
		audioType = "wav"
	}
	audioPlayerTemplate := `{{define "AudioPlayer"}}
<figure>
	<figcaption>
		<p class="p-0 m-0">{{.Title}}</p>
		<p class="p-0 m-0 text-secondary">{{.Subtitle}}</p>
	</figcaption>
	<audio controls style="width: 100%; max-width: 600px;">
		<source src="{{.Src}}" type="audio/` + audioType + `">
		Your browser does not support the <code>audio</code> element.
	</audio>
</figure>
{{end}}`
	t, err := template.New("AudioPlayer").Parse(audioPlayerTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, a)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

// AudioUpload can be used to upload an audio file.
type AudioUpload struct {
	ID       string
	Label    string
	HelpText string
}

// Validate returns an error if not all required inputs are populated.
func (a AudioUpload) Validate() error {
	if a.ID == "" {
		return errors.New("ID not populated")
	}
	return nil
}

// GenAudioUpload generates a control which is used to upload audio.
func GenAudioUpload(a AudioUpload, w io.Writer) error {
	err := a.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	randName := "a" + common.GetRandString(15)
	audioUploadTemplate := `{{define "AudioUpload"}}
{{if .Label}}<div>
	<p class="d-inline">{{.Label}}</p>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
</div>{{end}}
<div class="d-flex">
  <div class="custom-file mb-3 d-block" style="max-width: 600px;">
    <input type="file" accept='audio/*' class="custom-file-input pointer" id="{{.ID}}" name="` + randName + `" />
    <label class="custom-file-label" for="` + randName + `">Choose audio file</label>
  </div>
  <div>
    <button class="btn btn-sm btn-link mx-4" style="padding: .50rem .75rem;border: 1px solid;" data-polyappjs-blob-view-files-url="/blob/assets/polyappShouldBeOverwritten/{{.ID}}">
      View Files
    </button>
  </div>
</div>
{{- end}}`
	t, err := template.New("AudioUpload").Parse(audioUploadTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, a)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
