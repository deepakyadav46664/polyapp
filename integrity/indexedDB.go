package integrity

import (
	"errors"
)

// ValidateIndexedDBData ensures simplified data will not break the IndexedDB database.
func ValidateIndexedDB(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified should not be nil")
	}
	for k := range simplified {
		if k == "" {
			return errors.New("must not be empty")
		}
		// spec: https://www.w3.org/TR/IndexedDB/#key
		// Must be number, date, string, binary, or array. But ours are always string.
		// The rest of the commentary seems to concern things other than strings.

		// After searching a bit I can't find any other notable restrictions in JS nor IndexDB on strings.
		// MDN does not say anything: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures
	}
	return nil
}
