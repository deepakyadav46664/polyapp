package integrity

import (
	"fmt"
	"strings"
)

// ValidatePostgres ensures that field names can be encoded as column names by our code and that those column names
// will be valid.
func ValidatePostgres(simplified map[string]interface{}) error {
	for k := range simplified {
		for i, c := range k {
			// http://etutorials.org/SQL/Postgresql/Part+I+General+PostgreSQL+Use/Chapter+3.+PostgreSQL+SQL+Syntax+and+Use/PostgreSQL+Naming+Rules/
			if strings.ContainsRune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_", c) {
				continue
			} else if i > 0 && strings.ContainsRune("0123456789", c) {
				// ok after the first char
				continue
			} else {
				return fmt.Errorf("simplified key (%v) contained an invalid character (%v) at position (%v)", k, string(c), i)
			}
		}
	}
	return nil
}
