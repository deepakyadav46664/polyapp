package integrity

import (
	"errors"
	"strings"
)

// ValidateFirestore ensures the information being stored in Firestore will not break the database.
func ValidateFirestore(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified must be initialized")
	}
	docSize := 0
	for k, v := range simplified {
		errString := ValidFirestoreFieldName(k)
		if errString != "" {
			return errors.New("invalid key " + k + " : " + errString)
		}
		// documents may not be greater than 1MiB in size, or 1,048,487 bytes.
		// We need some space reserved for our 'polyapp' prefixed data.
		switch value := v.(type) {
		case string:
			length := len(value) + len(k)
			if length > 250_000 {
				// use a more precise length calculation
				length = len([]byte(value)) + len([]byte(k))
				if length > 1_000_000 {
					return errors.New("S key: " + k + " too long")
				}
			}
			docSize += length
		case []byte:
			// ignore this in the main count because []byte gets stored in Storage, not Firestore.
			// DO test if this exceeds a certain size... perhaps 100MB? Don't want to be storing HUGE files, after all.
			if len(value) > 100_000_000 {
				return errors.New("key: " + k + " is too long for Storage (100MB limit)")
			}
		case map[string]string:
			// for maps, the size = {"key":"value"} or something like that.
			totalSize := 2 + 5*len(value) + len(k)
			for innerKey, innerValue := range value {
				totalSize += len(innerKey) + len(innerValue)
			}
			if totalSize > 250_000 {
				return errors.New("M key: " + k + " too long")
			}
			docSize += totalSize
		case map[string]int64:
			// (5 + 4)*len where int64 takes 4 bytes to store
			totalSize := 2 + 9*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			if totalSize > 250_000 {
				return errors.New("key: " + k + " too long")
			}
			docSize += totalSize
		case map[string]bool:
			// (5 + 1)*len where bool takes 1 bytes to store
			totalSize := 2 + 6*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			if totalSize > 250_000 {
				return errors.New("key: " + k + " too long")
			}
			docSize += totalSize
		case map[string]float64:
			// (5 + 4)*len where float64 takes 4 bytes to store
			totalSize := 2 + 9*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			if totalSize > 250_000 {
				return errors.New("key: " + k + " too long")
			}
			docSize += totalSize
		default:
			// a guesstimate
			docSize += 1 + len(k)
		}
	}
	if docSize > 1_000_000 {
		return errors.New("document size is too large")
	}
	return nil
}

// ValidFirestoreFieldName returns "" if a key will be valid; a string which can be shown to the user otherwise
func ValidFirestoreFieldName(key string) string {
	if key == "" {
		return "Can't be empty"
	}
	// https://cloud.google.com/firestore/docs/best-practices under 'Field Names'
	if strings.ContainsAny(key, ".[]*`") {
		return "Please do not use any of these characters: . [ ] * `"
	}
	return ""
}
