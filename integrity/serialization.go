package integrity

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
)

// ValidateSerialization ensures all of the serialization schemes Polyapp uses can tolerate the input data structure
// That means JSON and Google's Protobuf. More information:
// https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A8-Insecure_Deserialization
// This function is substantially similar to the ValidateFile function.
func ValidateSerialization(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified should not be nil")
	}
	for k := range simplified {
		if k == "" {
			return errors.New("field name can't be empty")
		}
		for i, c := range k {
			if strings.ContainsRune("!#$%&()*+-./:<=>?@[]^_{|}~ ,", c) {
				// Backslash and quote chars are reserved, but
				// otherwise any punctuation chars are allowed
				// in a tag name.
				// Note: I added "," to the list because jslint.com said it was a valid char in JSON field names.
			} else if !unicode.IsLetter(c) && !unicode.IsDigit(c) {
				return fmt.Errorf("invalid name in key (%v) due to c (%v) at position (%v)", k, string(c), i)
			}
		}

		// There is also a 'checkValid' scan which is run by json, but I'm unwilling to replicate all of its unexported
		// logic in this function.
	}
	return nil
}
