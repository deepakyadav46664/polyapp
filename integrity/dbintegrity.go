package integrity

import (
	"fmt"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// ValidateAllDBs validates many objects will not break the underlying databases this data will be stored in to.
func ValidateAllDBs(queryable common.Queryable) error {
	simplified, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("Simplify: %w", err)
	}
	err = ValidateFirestore(simplified)
	if err != nil {
		return fmt.Errorf("ValidateFirestore: %w", err)
	}
	err = ValidateIndexedDB(simplified)
	if err != nil {
		return fmt.Errorf("ValidateIndexedDB: %w", err)
	}
	err = ValidateFile(simplified)
	if err != nil {
		return fmt.Errorf("ValidateFile: %w", err)
	}
	err = ValidateElasticsearch(simplified)
	if err != nil {
		return fmt.Errorf("ValidateElasticsearch: %w", err)
	}
	if queryable.CollectionName() != common.CollectionData {
		err = ValidatePostgres(simplified)
		if err != nil {
			return fmt.Errorf("ValidatePostgres: %w", err)
		}
	}
	err = ValidatePolyapp(simplified)
	if err != nil {
		return fmt.Errorf("ValidatePolyapp: %w", err)
	}
	return nil
}
