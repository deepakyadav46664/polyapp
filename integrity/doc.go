// Package integrity constitutes the 'Validation and Integrity library'. It is called by the allDB package.
//
// This package's responsibility is to run code which verifies that every change to the database will result in
// a valid state. A state is valid if it can be rolled back to the previous state or moved forward into
// a new state AND Polyapp's code is capable of transporting and operating on it safely. The 'state' of a database
// object is its current contents. The ability to move between states can only take the form of read, create, update,
// delete, and deprecate operations. Polyapp's code is capable of transporting and operating on the objects
// safely if the object will not break any of the underlying databases and will not break any of the languages used
// in the layers below this one. An example of 'breaking the database' would be SQL injection or rejected set operations.
//
// This package is very important because you may successfully write code which sets a document in the Firestore database
// but that same code would not work in the IDB database or in Elasticsearch or in the file system. If the database
// is saved to conditionally you might not realize your mistake until the code has already been deployed.
//
// This package is also very important for security.
package integrity
