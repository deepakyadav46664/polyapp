// package firestoreCRUD contains Firestore-specific code which can CRUD objects
// from the Firestore database.
//
// There is also support for queries.
package firestoreCRUD
