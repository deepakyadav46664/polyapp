package firestoreCRUD

import (
	"bytes"
	"context"
	"errors"
	"fmt"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Create creates a new document and sets it. It DOES require that you've already generated an ID.
//
// This is the generic function and should be used whenever possible.
func Create(ctx context.Context, client *firestore.Client, queryable common.Queryable) error {
	if client == nil {
		return errors.New("client was nil in Create")
	}
	if queryable == nil {
		return errors.New("queryable was nil in Create")
	}
	err := queryable.Validate()
	if err != nil {
		return fmt.Errorf("queryable was invalid: %w", err)
	}

	simple, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("queryable was not simplified: %w", err)
	}
	_, err = client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()).Create(ctx, simple)
	if err != nil {
		// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
		// but I would rather pass the error up so we could potentially utilize other resources like GCP Buckets
		return err
	}

	// Collection-specific handling is needed to deprecate the CreateData API.
	if queryable.CollectionName() == common.CollectionData {
		storageErrChan := make(chan error)
		lenStorageErrChan := 0
		for k, v := range simple {
			switch bv := v.(type) {
			case []byte:
				go func(byteArray []byte, firestoreID string, key string, errChan chan error) {
					storageErrChan <- SetBytes(byteArray, firestoreID, key)
				}(bv, queryable.GetFirestoreID(), k, storageErrChan)
				delete(simple, k)
				lenStorageErrChan++
			case [][]byte:
				for i := range bv {
					go func(byteArray []byte, firestoreID string, key string, storageErrChan chan error) {
						storageErrChan <- SetBytes(byteArray, firestoreID, key)
					}(bv[i], queryable.GetFirestoreID(), k, storageErrChan)
					lenStorageErrChan++
				}
				delete(simple, k)
			}
		}
		combinedErr := ""
		for i := 0; i < lenStorageErrChan; i++ {
			err = <-storageErrChan
			if err != nil {
				combinedErr += err.Error() + "; "
			}
		}
		if combinedErr != "" {
			return errors.New("storage sets: " + combinedErr)
		}
	}
	return nil
}

// SetBytes creates new storage objects. If there is an existing storage object with that name it is overwritten.
func SetBytes(b []byte, firestoreID string, key string) error {
	var err error
	objectName := GetStorageObjectName(common.CollectionData, firestoreID, key)
	err = SetStorageObject(GetBlobBucket(), bytes.NewReader(b), objectName, nil)
	if err != nil {
		return fmt.Errorf("SetStorageObject for name (%q): %w", objectName, err)
	}
	return nil
}

// Read reads from Firestore into the Queryable object passed in.
func Read(ctx context.Context, client *firestore.Client, queryable common.Queryable) error {
	if client == nil || queryable.GetFirestoreID() == "" {
		return errors.New("not all inputs were populated for Read")
	}
	snap, err := client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()).Get(ctx)
	if err != nil {
		return fmt.Errorf("couldn't read document with id: %v error: %w", queryable.GetFirestoreID(), err)
	}
	snapData := snap.Data()
	snapData[common.PolyappFirestoreID] = snap.Ref.ID
	err = queryable.Init(snapData)
	if err != nil {
		return fmt.Errorf("error Init-ing: %w", err)
	}
	switch queryable.CollectionName() {
	case common.CollectionData:
		data := queryable.(*common.Data)
		if data.SchemaID == "" {
			return fmt.Errorf("data.SchemaID was empty for ID: %v", queryable.GetFirestoreID())
		}
		if data.SchemaCache.FirestoreID == "" {
			data.SchemaCache, err = ReadSchema(ctx, client, data.SchemaID)
			if err != nil {
				return fmt.Errorf("ReadSchema %v: %w", data.SchemaID, err)
			}
		}
		err = common.ConvertDataToMatchSchema(data, &data.SchemaCache)
		if err != nil {
			return fmt.Errorf("common.ConvertDataToMatchSchema: %w", err)
		}
		err = common.RemoveExtraFields(data, &data.SchemaCache)
		if err != nil {
			return fmt.Errorf("common.RemoveExtraField: %w", err)
		}
	default:
	}
	return nil
}

// Update in Firestore. You must already know the document ID. Does not allow updating documents which do not exist.
func Update(ctx context.Context, client *firestore.Client, queryable common.Queryable) error {
	if client == nil {
		return errors.New("not all inputs were populated for Update")
	}
	err := queryable.Validate()
	if err != nil {
		return fmt.Errorf("validate: %w", err)
	}

	firestoreData, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("data could not be simplified: %w", err)
	}

	// exists because Update should not succeed if the document does not exist
	err = client.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		_, err = tx.Get(&firestore.DocumentRef{
			Path: client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()).Path,
			ID:   queryable.GetFirestoreID(),
		})
		if codes.NotFound == status.Code(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("error getting document to update: %w", err)
		}

		err = tx.Set(client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()), firestoreData, firestore.MergeAll)
		if err != nil {
			// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
			// but I would rather pass the error up so we could potentially utilize other resources like GCP Buckets
			return err
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction failed: %w", err)
	}

	return nil
}

// Delete removes a document. Allows you to delete documents which do not exist.
func Delete(ctx context.Context, client *firestore.Client, queryable common.Queryable) error {
	if client == nil {
		return errors.New("not all inputs were populated for Delete")
	}
	_, err := client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()).Delete(ctx)
	if err != nil {
		return fmt.Errorf("failed to delete document: %w", err)
	}
	return nil
}

// Deprecate sets the 'polyappDeprecated' property.
func Deprecate(ctx context.Context, client *firestore.Client, queryable common.Queryable) error {
	if client == nil || queryable.GetFirestoreID() == "" {
		return errors.New("not all inputs were populated for Deprecate")
	}
	// using a map lets us use firestore.MergeAll
	_, err := client.Collection(queryable.CollectionName()).Doc(queryable.GetFirestoreID()).Set(ctx, map[string]interface{}{
		common.PolyappDeprecated: true,
	}, firestore.MergeAll)
	if err != nil {
		return fmt.Errorf("failed to deprecate document: %w", err)
	}
	return nil
}
