package firestoreCRUD

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
	"testing"

	"google.golang.org/api/iterator"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionData).Doc("TestCreateData").Delete(ctx)
	err = CreateData(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.Data{
		FirestoreID: "TestCreateData",
		IndustryID:  "TestCreateData",
		DomainID:    "TestCreateData",
		SchemaID:    "TestCreateData",
		Ref:         make(map[string]*string),
		S:           make(map[string]*string),
		I:           make(map[string]*int64),
		B:           make(map[string]*bool),
		F:           make(map[string]*float64),
	}
	err = CreateData(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = CreateData(ctx, client, &d)
	if err != nil {
		t.Error("should not have errored when creating that document. Error: " + err.Error())
	}
	_, err = client.Collection(common.CollectionData).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Errorf("document should have been created: " + err.Error())
	}
}

func TestCreateDataContext(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionData).Doc("TestCreateDataContext").Delete(ctx)
	err = CreateDataContext(ctx, nil, "", "", "", nil)
	if err == nil {
		t.Error("should fail if inputs are nil")
	}
	err = CreateDataContext(ctx, client, "", "", "", &common.Data{})
	if err == nil {
		t.Error("should fail if there is no schema to find the document")
	}
	d := common.Data{}
	d.Init(nil)
	err = CreateDataContext(ctx, client, "TestCreateDataContext", "TestCreateDataContext", "TestCreateDataContext", &d)
	if err != nil {
		t.Error("should be able to create a document. Error: " + err.Error())
	}
	if d.FirestoreID == "" {
		t.Error("should be setting FirestoreID")
	}
	doc, err := client.Collection(common.CollectionData).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Fatal("error getting document which should have been created. Error: " + err.Error())
	}
	type A struct {
		PolyappIndustryID string `firestore:"polyappIndustryID"`
		PolyappDomainID   string `firestore:"polyappDomainID"`
		PolyappSchemaID   string `firestore:"polyappSchemaID"`
	}
	a := A{}
	err = doc.DataTo(&a)
	if err != nil {
		t.Error("couldn't parse the data. Error: " + err.Error())
	} else {
		if a.PolyappIndustryID != "TestCreateDataContext" {
			t.Error("polyappIndustryID not populated")
		}
		if a.PolyappDomainID != "TestCreateDataContext" {
			t.Error("polyappDomainID not populated")
		}
		if a.PolyappSchemaID != "TestCreateDataContext" {
			t.Error("polyappSchemaID not populated")
		}
	}
}

func TestReadData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	testReadData := make(map[string]string)
	testReadData["a"] = "b"
	_, err = client.Collection(common.CollectionData).Doc("TestReadData").Set(ctx, testReadData)
	if err != nil {
		t.Fatal(err)
	}

	_, err = ReadData(ctx, nil, "TestReadData")
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	o, err := ReadData(ctx, client, "TestReadData")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if o.FirestoreID != "TestReadData" {
		t.Error("should have set FirestoreID")
	}
	if len(o.F) != 0 || len(o.B) != 0 || len(o.I) != 0 {
		t.Error("should not have set these things")
	}
	if *o.S["a"] != "b" {
		t.Error("did not populate the value")
	}
}

func TestReadDataContext(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	testReadData := make(map[string]string)
	testReadData["a"] = "b"
	testReadData["polyappIndustryID"] = "TestReadDataContext"
	testReadData["polyappDomainID"] = "TestReadDataContext"
	testReadData["polyappSchemaID"] = "TestReadDataContext"
	_, err = client.Collection(common.CollectionData).Doc("TestReadDataContext").Set(ctx, testReadData)
	if err != nil {
		t.Fatal(err)
	}

	_, err = ReadDataContext(ctx, nil, "TestReadDataContext", "TestReadDataContext", "TestReadDataContext")
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	dataIter, err := ReadDataContext(ctx, client, "TestReadDataContext", "TestReadDataContext", "TestReadDataContext")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	sawAtLeastOne := false
	for {
		queryable, err := dataIter.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			t.Fatal("unexpected error iterating over data: " + err.Error())
		}
		sawAtLeastOne = true
		// we know from when we formulated the query that this will be a Data document.
		data := queryable.(*common.Data)
		if data.FirestoreID != "TestReadDataContext" {
			t.Error("should have set FirestoreID")
		}
		if len(data.F) != 0 || len(data.B) != 0 || len(data.I) != 0 {
			t.Error("should not have set these things")
		}
		if *data.S["a"] != "b" {
			t.Error("did not populate the value")
		}
	}
	if !sawAtLeastOne {
		t.Error("never found a document when querying")
	}
}

func TestUpdateData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Collection(common.CollectionData).Doc("TestUpdateData").Delete(ctx)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal(err)
	}
	data := common.Data{
		FirestoreID: "TestUpdateData",
		IndustryID:  "TestUpdateData",
		DomainID:    "TestUpdateData",
		SchemaID:    "TestUpdateData",
	}
	apart := make(map[string]interface{})
	apart["a"] = "b"
	data.Init(apart)
	err = UpdateData(ctx, client, &data)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, err = client.Collection(common.CollectionData).Doc("TestUpdateData").Set(ctx, map[string]string{
		common.PolyappIndustryID: "TestUpdateData",
		common.PolyappDomainID:   "TestUpdateData",
		common.PolyappSchemaID:   "TestUpdateData",
	})
	if err != nil {
		t.Fatal(err)
	}
	_, err = client.Collection(common.CollectionSchema).Doc("TestUpdateData").Set(ctx, map[string]interface{}{
		common.PolyappIndustryID: "TestUpdateData",
		common.PolyappDomainID:   "TestUpdateData",
		common.PolyappSchemaID:   "TestUpdateData",
		"DataTypes":              map[string]string{"a": "S"},
		"DataKeys":               []string{"a"},
		"DataHelpText":           map[string]string{"a": "generic"},
	})
	if err != nil {
		t.Fatal(err)
	}
	err = UpdateData(ctx, client, &data)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestUpdateDataContext(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Collection(common.CollectionSchema).Doc("TestUpdateDataContext").Set(ctx, map[string]interface{}{
		common.PolyappIndustryID: "TestUpdateDataContext",
		common.PolyappDomainID:   "TestUpdateDataContext",
		common.PolyappSchemaID:   "TestUpdateDataContext",
		"DataTypes":              map[string]string{"a": "S"},
		"DataKeys":               []string{"a"},
		"DataHelpText":           map[string]string{"a": "generic"},
	})
	if err != nil {
		t.Fatal(err)
	}
	_, _ = client.Collection(common.CollectionData).Doc("TestUpdateDataContext").Delete(ctx)
	data := common.Data{
		FirestoreID: "TestUpdateDataContext",
		IndustryID:  "TestUpdateDataContext",
		DomainID:    "TestUpdateDataContext",
		SchemaID:    "TestUpdateDataContext",
	}
	apart := make(map[string]interface{})
	apart["a"] = "b"
	data.Init(apart)
	err = UpdateDataContext(ctx, client, "TestUpdateDataContext", "TestUpdateDataContext", "TestUpdateDataContext", &data)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	} else if status.Code(err) != codes.NotFound {
		t.Fatal("unexpected err: " + err.Error())
	}
	dataInDoc := make(map[string]string)
	dataInDoc[common.PolyappSchemaID] = "TestUpdateDataContext"
	dataInDoc[common.PolyappIndustryID] = "TestUpdateDataContext"
	dataInDoc[common.PolyappDomainID] = "TestUpdateDataContext"
	_, err = client.Collection(common.CollectionData).Doc("TestUpdateDataContext").Set(ctx, dataInDoc)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal(err)
	}
	err = UpdateDataContext(ctx, client, "TestUpdateDataContext", "TestUpdateDataContext", "TestUpdateDataContext", &data)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Collection(common.CollectionData).Doc("TestDeleteData").Set(ctx, map[string]interface{}{
		common.PolyappIndustryID: "TestDeleteData",
		common.PolyappDomainID:   "TestDeleteData",
		common.PolyappSchemaID:   "TestDeleteData",
	})
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal(err)
	}
	err = DeleteData(ctx, nil, "TestDeleteData")
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = DeleteData(ctx, client, "TestDeleteData")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionData).Doc("TestDeprecateData").Set(ctx, make(map[string]interface{}))
	err = DeprecateData(ctx, nil, "TestDeprecateData")
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = DeprecateData(ctx, client, "TestDeprecateData")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionData).Doc("TestDeprecateData").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt("polyappDeprecated")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}

func TestDeprecateDataContext(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	dataInDoc := make(map[string]string)
	dataInDoc["polyappIndustryID"] = "TestDeprecateDataContext"
	dataInDoc["polyappDomainID"] = "TestDeprecateDataContext"
	dataInDoc["polyappSchemaID"] = "TestDeprecateDataContext"
	_, _ = client.Collection(common.CollectionData).Doc("TestDeprecateDataContext").Set(ctx, dataInDoc)
	err = DeprecateDataContext(ctx, nil, "TestDeprecateDataContext", "TestDeprecateDataContext", "TestDeprecateDataContext")
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = DeprecateDataContext(ctx, client, "TestDeprecateDataContext", "TestDeprecateDataContext", "TestDeprecateDataContext")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	iter := client.Collection(common.CollectionData).Where("polyappIndustryID", "==", "TestDeprecateDataContext").Documents(ctx)
	numDocs := 0
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			t.Fatal("error in iterator: " + err.Error())
		}
		numDocs++
		v, err := doc.DataAt("polyappDeprecated")
		if err != nil {
			t.Error("unexpected error: " + err.Error())
		}
		if !v.(bool) {
			t.Error("should have deprecated the document")
		}
	}
}

func TestDeleteTestData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	iter := client.Collection(common.CollectionData).Where("polyappIndustryID", "==", "TestCreateDataContext").Documents(ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			t.Fatal("error in iterator: " + err.Error())
		}
		_, err = doc.Ref.Delete(ctx)
		if err != nil {
			t.Fatal(err)
		}
	}
	iter = client.Collection(common.CollectionData).Where("polyappIndustryID", "==", "TestDeprecateDataContext").Documents(ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			t.Fatal("error in iterator: " + err.Error())
		}
		_, err = doc.Ref.Delete(ctx)
		if err != nil {
			t.Fatal(err)
		}
	}
}

func TestQueryReadData(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal("could not get client: " + err.Error())
	}
	tqrd := client.Collection(common.CollectionData).Doc("TestQueryReadData")
	d := make(map[string]string)
	d[common.PolyappSchemaID] = "TestQueryReadData"
	d[common.PolyappIndustryID] = "TestQueryReadData"
	d[common.PolyappDomainID] = "TestQueryReadData"
	_, err = tqrd.Set(ctx, d)
	if err != nil {
		t.Fatal("could not set TestQueryReadData so it's unclear how to get this working")
	}
	query := client.Collection(common.CollectionData).Where(common.PolyappSchemaID, "==", "TestQueryReadData")
	i, err := QueryRead(ctx, client, "TestQueryReadData", "TestQueryReadData", "TestQueryReadData", query)
	if err != nil {
		t.Error("QueryReadData failed: " + err.Error())
	}
	ct := 0
	for {
		queryable, err := i.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			t.Fatal("error iterating: " + err.Error())
		}
		ct++
		// we know from when we formulated the query that this will be a Data document.
		d := queryable.(*common.Data)
		if d.SchemaID != "TestQueryReadData" || d.IndustryID != "TestQueryReadData" || d.DomainID != "TestQueryReadData" {
			t.Error("values not set as expected")
		}
	}
	if ct != 1 {
		t.Error("query should return exactly one result")
	}
}
