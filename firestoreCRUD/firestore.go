package firestoreCRUD

import (
	"context"
	"fmt"
	"golang.org/x/oauth2/google"
	"sync"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/api/option"
)

var (
	clientMux    sync.Mutex
	cachedCtx    context.Context
	cachedClient *firestore.Client
)

// GetClient gets a client.
func GetClient() (context.Context, *firestore.Client, error) {
	clientMux.Lock()
	defer clientMux.Unlock()
	if cachedClient != nil {
		return cachedCtx, cachedClient, nil
	}

	projectID := common.GetGoogleProjectID()
	credentials, err := google.FindDefaultCredentials(context.Background(), []string{
		"https://www.googleapis.com/auth/datastore",
	}...)
	if err != nil {
		return nil, nil, fmt.Errorf("google.FindDefaultCredentials: %w", err)
	}

	// Get a Firestore client.
	cachedCtx = context.Background()
	cachedClient, err = firestore.NewClient(cachedCtx, projectID, option.WithCredentials(credentials))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create client: %v", err)
	}
	return cachedCtx, cachedClient, nil
}
