package handlers

import (
	"bytes"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func HandleMultipleBlobs(d common.Data, key string, URL *url.URL) ([]byte, error) {
	var ux struct {
		Title   string
		Content template.HTML
	}
	baseURL := URL.Path
	var c bytes.Buffer
	URLs := make([]string, len(d.ABytes[key]))
	for i := range d.ABytes[key] {
		if baseURL[len(baseURL)-1] == '/' {
			URLs[i] = baseURL + strconv.Itoa(i)
		} else {
			URLs[i] = baseURL + "/" + strconv.Itoa(i)
		}
	}
	c.WriteString(`<div class="container">`)
	for i := range URLs {
		if strings.HasPrefix(http.DetectContentType(d.ABytes[key][i]), "image") {
			c.WriteString(`<img src="` + URLs[i] + `" />`)
		} else if len(d.ABytes[key][i]) < 100_000 {
			c.WriteString(`<div>`)
			c.WriteString(html.EscapeString(string(d.ABytes[key][i])))
			c.WriteString(`</div>`)
		} else {
			c.WriteString(`<div>Inline file was too large to display inline</div>`)
		}
	}
	c.WriteString(`</div>`)
	ux.Content = template.HTML(c.String())
	if d.SchemaCache.Name != nil {
		ux.Title = *d.SchemaCache.Name
	}
	var buf bytes.Buffer
	templates, err := getTemplates()
	if err != nil {
		return []byte(""), fmt.Errorf("error getting templates: %w", err)
	}
	err = templates.ExecuteTemplate(&buf, "multipleImages", ux)
	if err != nil {
		return []byte(""), fmt.Errorf("error executing templates: %w", err)
	}
	return buf.Bytes(), err
}
