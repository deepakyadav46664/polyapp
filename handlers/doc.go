// Package handlers can compile to both WASM and any server-side compilation target. It is the first package
// which is reached in the flow of a request after the main command code. It does things like updating Data documents
// when a POST request is made, responding to GET requests, and calling application-specific code contained in Actions.
package handlers
