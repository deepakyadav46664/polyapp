package handlers

import (
	"html/template"
)

var (
	templates *template.Template = nil
)

// getTemplates gives you the templates you need to construct Polyapp's app shell.
// Templates for actual tasks are stored in the UX collection of the database.
// Those templates are parsed and rendered at runtime to avoid unauthorized access.
func getTemplates() (*template.Template, error) {
	var err error
	if templates == nil {
		templates, err = findAndParseTemplates(template.FuncMap{})
	}
	return templates, err
}
