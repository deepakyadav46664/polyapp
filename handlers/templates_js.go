package handlers

import (
	"errors"
	"html/template"
)

// findAndParseTemplates finds and parses the app shell templates.
// Since we're looking for persistent storage and these files have a 1:1 with the
// server they'll likely be stored in a cache.
//
// TODO this needs to be implemented but it also poses some practical problems. Up until a recent release of Go there
// wasn't a native way to embed files within Go code so I would have had to create a string out of each file and put
// that string inside the code. Perhaps now with the release of embed https://github.com/golang/go/issues/41191 we can
// do this in a way which doesn't require storing large strings inside Go files. Unfortunately Go 1.16 support is not
// yet available on App Engine and I think it's fair to assume other systems may not support the latest release of Go yet.
func findAndParseTemplates(funcMap template.FuncMap) (*template.Template, error) {
	return nil, errors.New("unimplemented")
}
