// +build !js

package handlers

import (
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// findAndParseTemplates walks rootDir and all subdirectories. It finds *.html and parses it into a template
// which is placed on a root template which is returned (potentially with an error).
func findAndParseTemplates(funcMap template.FuncMap) (*template.Template, error) {
	cleanRoot := filepath.Clean(common.GetPublicPath())
	prefixLength := len(cleanRoot) + 1
	root := template.New("")

	err := filepath.Walk(cleanRoot, func(path string, info os.FileInfo, e1 error) error {
		if !info.IsDir() && strings.HasSuffix(path, ".html") {
			if e1 != nil {
				return e1
			}

			b, e2 := ioutil.ReadFile(path)
			if e2 != nil {
				return e2
			}

			name := path[prefixLength:]
			t := root.New(name).Funcs(funcMap)
			_, e2 = t.Parse(string(b))
			if e2 != nil {
				return e2
			}
		}
		return nil
	})
	return root, err
}
