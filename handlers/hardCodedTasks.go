package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/gen"
)

// TODO replace all of the handlers in this page with a combination of Actions and a method which translates
// prefixes of paths like /polyappChangeContext into a definitive Task, Schema, and UX, like PublicMap but potentially more flexible.

// homeRouter is hard-coded to look at the taskID for the home page and reply with the content of that page.
// This allows you to change the query parameters of the home page and the result is that you're 'routed' to a new Task.
//
// This function is called both from GET requests and POST requests.
func homeRouter(request common.POSTRequest) (modDOMs []common.ModDOM, NewURL string, Title string, err error) {
	switch request.PublicPath {
	case "/":
		return homePage(request)
	case "/polyappChangeContext/":
		return changeContextPage(request)
	case "/polyappChangeTask/":
		return changeTaskPage(request)
	case "/polyappChangeData/":
		return changeDataPage(request)
	case "/signIn/":
		return signInPage(request)
	default:
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML:           "<p>This page was not found</p>",
		}}, "", "Page Not Found", nil
	}
}

// changeContextPage allows you to select a context or create a new one and when you click 'Done' you are directed to
// the changeTaskPage.
//
// This function uses a bad strategy for changing the options for Domain: it deletes the field from the UI and replaces
// it with an identical field with different options. It would be much better to dynamically update the options
// instead of dynamically updating the entire UI element, or better yet, to use a control which makes a variable
// query to the database to populate the options. Unfortunately such a control does not yet exist.
func changeContextPage(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, title string, err error) {
	var allContent bytes.Buffer
	modDOMs = make([]common.ModDOM, 0)

	industriesArray, industriesMap := getIndustries()

	dataIndex := url.PathEscape("/polyappChangeContext?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain())
	if request.Data[dataIndex] == nil {
		dataIndex = url.PathEscape("/polyappChangeContext?industry=" + request.GetIndustry())
	}
	if request.Data[dataIndex] == nil {
		dataIndex = url.PathEscape("/polyappChangeContext")
	}
	industry, industryOk := request.Data[dataIndex][common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "industry")].(string)
	domain, domainOk := request.Data[dataIndex][common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "domain")].(string)
	done := request.Data[dataIndex][common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "done")]
	if !industryOk {
		industry = request.GetIndustry()
	}
	if !domainOk {
		domain = request.GetDomain()
	}
	initialLoad := true
	if industryOk || domainOk || (done != nil && done != "") {
		initialLoad = false
	}
	if !initialLoad {
		var tmpBuffer bytes.Buffer
		modDOMs = append(modDOMs, common.ModDOM{
			DeleteSelector: "#polyapp_polyapp_contextualize_industry-Wrapper .alert",
		})
		if !industriesMap[industry] {
			err = gen.GenFormError(gen.FormError{
				Text: "Selected industry is invalid",
			}, &tmpBuffer)
			if err != nil {
				return nil, "", title, fmt.Errorf("GenFormError for Industry: %w", err)
			}
			modDOMs = append(modDOMs, common.ModDOM{
				DeleteSelector: "#polyappErrorpolyapp_polyapp_contextualize_industry > *",
				InsertSelector: "#polyappErrorpolyapp_polyapp_contextualize_industry",
				Action:         "beforeend",
				HTML:           tmpBuffer.String(),
			})
			tmpBuffer.Reset()
		} else {
			modDOMs = append(modDOMs, common.ModDOM{
				DeleteSelector: "#polyappErrorpolyapp_polyapp_contextualize_industry > *",
			})
		}

		if domain == "" || domain == "polyappNone" {
			err = gen.GenFormError(gen.FormError{
				Text: "Domain is required. You will be able to select a Domain after selecting an Industry.",
			}, &tmpBuffer)
			modDOMs = append(modDOMs, common.ModDOM{
				DeleteSelector: "#polyappErrorpolyapp_polyapp_contextualize_domain > *",
				InsertSelector: "#polyappErrorpolyapp_polyapp_contextualize_domain",
				Action:         "beforeend",
				HTML:           tmpBuffer.String(),
			})
			tmpBuffer.Reset()
		} else {
			modDOMs = append(modDOMs, common.ModDOM{
				DeleteSelector: "#polyappErrorpolyapp_polyapp_contextualize_domain > *",
			})
		}
		if industry == "" || !industriesMap[industry] {
			return modDOMs, "", title, nil
		}

		if done != nil && done != "" && industriesMap[industry] && domain != "" && domain != "polyappNone" {
			// Done with this page since Done was pressed and Industry and Domain are valid.
			newURL := "/polyappChangeTask?industry=" + industry + "&domain=" + domain + "&schema=polyappTask" +
				"&user=" + request.UserID + "&role=" + request.RoleID
			return nil, newURL, title, nil
		}
	}

	domainSelectOptions := make([]gen.SelectOption, 1)
	domainSelectOptions[0] = gen.SelectOption{
		Selected: false,
		Value:    "",
	}
	if industry != "" {
		q := allDB.Query{}
		q.Init("", "", "", common.CollectionTask)
		q.AddEquals("polyappIndustryID", industry)
		iter, err := q.QueryRead()
		if err != nil {
			return nil, "", title, fmt.Errorf("q.QueryRead: %w", err)
		}
		domains := make(map[string]bool)
		for {
			queryable, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return nil, "", title, fmt.Errorf("iter.Next: %w", err)
			}
			task := queryable.(*common.Task)
			domains[task.DomainID] = true
		}
		for k := range domains {
			domainSelectOptions = append(domainSelectOptions, gen.SelectOption{
				Selected: k == domain,
				Value:    k,
			})
		}
	}

	industrySelectOptions := make([]gen.SelectOption, len(industriesArray)+1)
	industrySelectOptions[0] = gen.SelectOption{
		Selected: false,
		Value:    "",
	}
	for i := range industriesArray {
		industrySelectOptions[i+1].Value = industriesArray[i]
		if industriesArray[i] == industry {
			industrySelectOptions[i+1].Selected = true
		}
	}

	var domainHTML bytes.Buffer
	err = gen.GenSelectInput(gen.SelectInput{
		HelpText: "Business Domain is a group of people and processes which share common terminology. In Polyapp every " +
			"name you give to something inside a Domain must be unique and all information is stored in the same \"folder\". " +
			"For example, in a hospital one business domain is OBGYN which is a group of doctors, nurses and support " +
			"staff who provide care for pregnant women, catch babies and improve patient's reproductive health. Everyone " +
			"in this Domain shares the same understanding of terms like \"patient\" and \"insurance\". This understanding " +
			"is different than a plastic surgeon, whose work is in a different business Domain.",
		ID:            common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "domain"),
		Label:         "Business Domain",
		SelectOptions: domainSelectOptions,
		MultiSelect:   false,
		WrapperID:     "domainWrapperID",
	}, &domainHTML)
	if err != nil {
		return nil, "", title, fmt.Errorf("gen.GenSelectInput for Domain: %w", err)
	}

	if initialLoad {
		allContent.WriteString(`<h1>Change Domain</h1>`)
		err = gen.GenSelectInput(gen.SelectInput{
			HelpText: "What industry does your organization operate in? NAICS has classified over 1,000 industries, from " +
				"Private Households to Fire Protection. Polyapp places all information about a particular Industry and Domain " +
				"combination into the same folder.",
			ID:                common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "industry"),
			Label:             "NAICS Industry",
			SelectOptions:     industrySelectOptions,
			MultiSelect:       false,
			TransmitUpdateNow: true,
		}, &allContent)
		if err != nil {
			return nil, "", title, fmt.Errorf("gen.GenSelectInput for Industry: %w", err)
		}
		allContent.Write(domainHTML.Bytes())
		err = gen.GenIconButton(gen.IconButton{
			ID:        common.AddFieldPrefix("polyapp", "polyapp", "contextualize", "done"),
			Text:      "Done",
			AltText:   "Done",
			ImagePath: "/assets/icons/green_check.png",
		}, &allContent)
		if err != nil {
			return nil, "", title, fmt.Errorf("generating Done button: %w", err)
		}
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML:           `<div class="container">` + allContent.String() + "</div>",
		}}, "", title, nil
	} else {
		modDOMs = append([]common.ModDOM{{
			DeleteSelector: "#domainWrapperID",
			InsertSelector: "#polyapp_polyapp_contextualize_done",
			Action:         "beforebegin",
			HTML:           domainHTML.String(),
		}}, modDOMs...)
	}

	return modDOMs, "", "Change Domain", nil
}

// changeTaskPage allows you to select a Task from a particular Context or create a new one. If you do not have a Context
// yet it redirects you to the changeContextPage. After finishing, it directs you to the changeSchemaPage.
func changeTaskPage(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, title string, err error) {
	var allContent bytes.Buffer
	err = writeBreadcrumb(request, &allContent)
	if err != nil {
		err = fmt.Errorf("writeBreadcrumb: %w", err)
		return
	}
	allContent.WriteString(`<h1>Change Task</h1>`)
	// allContent.WriteString(`<a class="btn btn-primary" href="/polyappChangeData?industry=polyapp&amp;domain=TaskSchemaUX&amp;task=gqyLWclKrvEaWHalHqUdHghrt&amp;schema=gjGLMlcNApJyvRjmgQbopsXPI&amp;ux=hRpIEGPgKvxSzvnCeDpAVcwgJ">Edit Task</a>`)
	modDOMs, newURL, err = changePageRedirects(request)
	if err != nil {
		return []common.ModDOM{{}}, newURL, title, fmt.Errorf("changePageRedirects: %w", err)
	}
	if newURL != "" {
		return modDOMs, newURL, title, err
	}

	if len(request.Data) > 1 {
		return []common.ModDOM{{}}, "", title, errors.New("request.Data length was > 1 in changeTaskPage. Was: " + strconv.Itoa(len(request.Data)))
	}

	if request.MessageID != "" {
		// POST request
		return []common.ModDOM{{}}, "", title, nil
	}

	q := allDB.Query{}
	q.Init(request.OverrideIndustryID, request.OverrideDomainID, request.SchemaID, common.CollectionTask)
	q.SetLength(500)
	q.AddEquals(common.PolyappIndustryID, request.OverrideIndustryID)
	q.AddEquals(common.PolyappDomainID, request.OverrideDomainID)
	err = q.AddSortBy("Name", "asc")
	if err != nil {
		return []common.ModDOM{{}}, "", title, fmt.Errorf("q.AddSortBy: %w", err)
	}
	iter, err := q.QueryRead()
	if err != nil {
		return []common.ModDOM{{}}, "", title, fmt.Errorf("q.QueryRead: %w", err)
	}
	type taskData struct {
		Name     string
		HelpText string
		Href     string
	}
	taskDatas := make([]taskData, 0)
	for {
		out, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return []common.ModDOM{{}}, "", title, fmt.Errorf("iter.Next: %w", err)
		}
		t := out.(*common.Task)

		// The Data which is used to generate a Task.
		// We use this to find Tasks because it is the only thing right now which has references to the Task, Schema, and UX at the same time.
		q2 := allDB.Query{}
		q2.Init("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", common.CollectionData)
		q2.AddEquals("polyapp_TaskSchemaUX_gjGLMlcNApJyvRjmgQbopsXPI_Task ID", t.FirestoreID)
		q2.SetLength(1)
		iter2, err := q2.QueryRead()
		if err != nil {
			return nil, "", title, fmt.Errorf("q2.QueryRead: %w", err)
		}
		d, err := iter2.Next()
		if err != nil && err == common.IterDone {
			continue
		}
		if err != nil {
			return nil, "", title, fmt.Errorf("iter2.Next: %w", err)
		}
		d2 := d.(*common.Data)
		schemaID := *d2.S["polyapp_TaskSchemaUX_gjGLMlcNApJyvRjmgQbopsXPI_Schema ID"]
		uxID := *d2.S["polyapp_TaskSchemaUX_gjGLMlcNApJyvRjmgQbopsXPI_UX ID"]
		href := "/polyappChangeData?industry=" + request.OverrideIndustryID + "&domain=" + request.OverrideDomainID +
			"&task=" + t.FirestoreID + "&ux=" + uxID + "&schema=" + schemaID + "&user=" + request.UserID + "&role=" + request.RoleID
		taskDatas = append(taskDatas, taskData{
			Name:     t.Name,
			HelpText: t.HelpText,
			Href:     href,
		})
	}
	if len(taskDatas) >= 500 {
		allContent.WriteString(`<div class="alert alert-warning">There are over 500 Tasks in this Industry / Domain combination. Only the first 500 are shown.</div>`)
	}
	if len(taskDatas) > 0 {
		allContent.WriteString(`<div class="list-group">`)
		for _, t := range taskDatas {
			allContent.WriteString(`<a href="` + t.Href + `" class="list-group-item list-group-item-action">`)
			allContent.WriteString(`<h4>` + t.Name + `</h4>`)
			allContent.WriteString(`<p>` + t.HelpText + `</p>`)
			allContent.WriteString(`</a>`)
		}
		allContent.WriteString(`</div>`)
	} else {
		allContent.WriteString(`<p>No Tasks were found in the "` + request.OverrideIndustryID + `" Industry and "` + request.OverrideDomainID + `" Domain.</p>`)
	}

	return []common.ModDOM{{
		DeleteSelector: "#polyappJSMain > *",
		InsertSelector: "#polyappJSMain",
		Action:         "afterbegin",
		HTML:           `<div class="container">` + allContent.String() + "</div>",
	}}, "", "Change Task", nil
}

// changeDataPage allows you to select a particular Data instance or create a new one. If you do not have a Context yet
// it redirects you to the changeContextPage. If you do not have a Task, it directs you to changeTaskPage. After finishing,
// it directs you to the Data in the Task in the Context.
func changeDataPage(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, title string, err error) {
	var allContent bytes.Buffer
	err = writeBreadcrumb(request, &allContent)
	if err != nil {
		err = fmt.Errorf("writeBreadcrumb: %w", err)
		return
	}
	allContent.WriteString(`<h1>Change Data</h1>`)
	modDOMs, newURL, err = changePageRedirects(request)
	if err != nil {
		return []common.ModDOM{{}}, newURL, title, fmt.Errorf("changePageRedirects: %w", err)
	}
	if newURL != "" {
		return modDOMs, newURL, title, err
	}

	if len(request.Data) > 1 {
		return []common.ModDOM{{}}, "", title, errors.New("request.Data length was > 1 in changeTaskPage. Was: " + strconv.Itoa(len(request.Data)))
	}
	dataIndex := ""
	for k := range request.Data {
		// there is only one dataIndex since there is only one level to our Task, so just grab it instead of hard-coding
		// the exact key
		dataIndex = k
	}
	schema, err := allDB.ReadSchema(request.SchemaID)
	if err != nil {
		return nil, "", title, fmt.Errorf("ReadSchema: %w", err)
	}

	if request.MessageID != "" {
		// POST request
		if request.Data[dataIndex]["createData"] != nil &&
			request.Data[dataIndex]["createData"] != "" {
			// Must be authorized to perform this action
			user := request.UserCache
			if user == nil {
				u, err := allDB.ReadUser(request.UserID)
				if err != nil {
					return nil, "", title, fmt.Errorf("allDB.ReadUser: %w", err)
				}
				user = &u
			}
			err = RoleAuthorize(user, common.GETRequest{
				IndustryID: request.GetIndustry(),
				DomainID:   request.GetDomain(),
				DataID:     request.DataID,
				SchemaID:   request.SchemaID,
			}, request.RoleID, 'c')
			if err != nil {
				return nil, "", title, echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user to create a Data: "+err.Error())
			}

			var newData *common.Data
			if schema.FirestoreID == "ADdkaAyYPQwXOLkMfGUGqoPsl" {
				// special requirements because we do not allow users to edit all required fields
				// Note: this code is duplicated elsewhere asdlfjsadjigqrpnjiobjnsigrepqrgtnfdas
				themeData := common.Data{}
				themeData.Init(nil)
				themeData.S = map[string]*string{
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Body Background Color": common.String("#ffffff"),
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Body Color":            common.String("#212529"), // gray-900
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Danger Color":          common.String("#dc3545"), // red
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Dark Color":            common.String("#343a40"), // "$gray-800"
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Info Color":            common.String("#17a2b8"), // cyan
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Light Color":           common.String("#f8f9fa"), // $gray-100
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Primary Color":         common.String("#007bff"),
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Secondary Color":       common.String("#6c757d"), // gray-600
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Success Color":         common.String("#28a745"), // green
					"Custom Computer Programming Services_Website Builder_gbRhAMUhMOmskAArPSPjdMkbE_Warning Color":         common.String("#ffc107"), // yellow
				}
				themeData.IndustryID = "Custom Computer Programming Services"
				themeData.DomainID = "Website Builder"
				themeData.SchemaID = "gbRhAMUhMOmskAArPSPjdMkbE"
				themeData.FirestoreID = common.GetRandString(25)
				err = allDB.CreateData(&themeData)
				if err != nil {
					return nil, "", title, fmt.Errorf("allDB.CreateData for themeData (%v): %w", themeData.FirestoreID, err)
				}

				themeDataRef := "/t/Custom Computer Programming Services/Website Builder/OvQSmnHVGpewlytmjVNVupduJ?ux=cylpBvWKwCksBeUnQycwfEIJo&schema=gbRhAMUhMOmskAArPSPjdMkbE&data=" + themeData.FirestoreID
				siteSchema, err := allDB.ReadSchema("ADdkaAyYPQwXOLkMfGUGqoPsl")
				if err != nil {
					return nil, "", title, fmt.Errorf("allDB.ReadSchema (%v): %w", "ADdkaAyYPQwXOLkMfGUGqoPsl", err)
				}
				newData, err = common.CreateDataFromSchema(&siteSchema, &common.Data{
					Ref: map[string]*string{
						"polyapp_TaskSchemaUX_gbRhAMUhMOmskAArPSPjdMkbE_Theme": common.String(themeDataRef),
					},
					S: map[string]*string{
						"polyapp_TaskSchemaUX_gbRhAMUhMOmskAArPSPjdMkbE_Domain": common.String("test-domain-" + common.GetRandString(20) + ".com"),
					},
				})
				if err != nil {
					return nil, "", title, fmt.Errorf("CreateDataFromSchema: %w", err)
				}
				err = allDB.CreateData(newData)
				if err != nil {
					return nil, "", title, fmt.Errorf("CreateData: %w", err)
				}
			} else {
				newData, err = common.CreateDataFromSchema(&schema, &common.Data{})
				if err != nil {
					return nil, "", title, fmt.Errorf("CreateDataFromSchema: %w", err)
				}
				err = allDB.CreateData(newData)
				if err != nil {
					return nil, "", title, fmt.Errorf("CreateData: %w", err)
				}
			}

			newURL := common.CreateURL(schema.IndustryID, schema.DomainID, request.OverrideTaskID, request.UXID, request.SchemaID, newData.FirestoreID, request.UserID, request.RoleID, "", "")
			return nil, newURL, title, nil
		}
		if request.Data[dataIndex]["btnEditTask"] != nil &&
			request.Data[dataIndex]["btnEditTask"] != "" {
			q := allDB.Query{}
			q.Init("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", common.CollectionData)
			q.AddEquals(common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), request.GetTask())
			q.SetLength(1)
			iter, err := q.QueryRead()
			if err != nil {
				return nil, "", title, fmt.Errorf("q.QueryRead: %w", err)
			}
			d, err := iter.Next()
			if err != nil {
				return nil, "", title, fmt.Errorf("iter.Next could not find an Edit Task which matched the Task ID %v: %w", request.GetTask(), err)
			}
			newURL := common.CreateURL("polyapp", "TaskSchemaUX", "gqyLWclKrvEaWHalHqUdHghrt", "hRpIEGPgKvxSzvnCeDpAVcwgJ", "gjGLMlcNApJyvRjmgQbopsXPI", d.GetFirestoreID(), request.UserID, request.RoleID, "", "")
			return nil, newURL, title, nil
		}
		chosenRow := request.Data[dataIndex][request.SchemaID]
		if chosenRow != nil {
			chosenRowArray, ok := chosenRow.([]interface{})
			if !ok {
				return nil, "", title, errors.New("chosenRow was not an array of interface")
			}
			chosenRowStrings := make([]string, len(chosenRowArray))
			for i, v := range chosenRowArray {
				chosenRowStrings[i], ok = v.(string)
				if !ok {
					return nil, "", title, errors.New("chosenRow could not be converted to a string at index: " + strconv.Itoa(i))
				}
			}
			IDInRowSelected := chosenRowStrings[len(chosenRowArray)-1]
			newURL := common.CreateURL(schema.IndustryID, schema.DomainID, request.OverrideTaskID, request.UXID, request.SchemaID, IDInRowSelected, request.UserID, request.RoleID, "", "")
			return nil, newURL, title, nil
		}
		return []common.ModDOM{{}}, "", title, nil
	}

	user := request.UserCache
	if user == nil {
		u, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return nil, "", title, fmt.Errorf("allDB.ReadUser: %w", err)
		}
		user = &u
	}
	tb := gen.TextButton{
		ID:      "createData",
		Text:    "Create",
		Styling: "primary",
	}
	err = RoleAuthorize(user, common.GETRequest{
		IndustryID: request.GetIndustry(),
		DomainID:   request.GetDomain(),
		DataID:     request.DataID,
		SchemaID:   request.SchemaID,
	}, request.RoleID, 'c')
	if err != nil && !strings.Contains(err.Error(), "did not have a role which was authorized") {
		return nil, "", title, echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	} else if err != nil {
		tb.Disabled = true
	}

	err = gen.GenTextButton(tb, &allContent)
	if err != nil {
		return nil, "", title, fmt.Errorf("GenTextButton: %w", err)
	}

	columns := make([]gen.Column, 0)
	for _, v := range schema.DataKeys {
		if v == "" || common.RemoveFieldPrefix(v) == "" {
			// happens if you create an empty field (which should not happen, but it COULD happen in some circumstances)
			continue
		}
		if schema.DataTypes[v] != "S" && schema.DataTypes[v] != "B" && schema.DataTypes[v] != "Ref" && schema.DataTypes[v] != "I" && schema.DataTypes[v] != "F" {
			// we should only show fields which are easily converted to strings
			// If we include other fields they either look bad (arrays) or the query won't return any results (Bytes).
			continue
		}
		columns = append(columns, gen.Column{
			Header: common.RemoveFieldPrefix(v),
			ID:     v,
		})
	}
	columns = append(columns, gen.Column{
		Header: "ID",
		ID:     "polyappFirestoreID",
	})

	err = gen.GenTable(gen.Table{
		ID:       request.SchemaID,
		Industry: request.GetIndustry(),
		Domain:   request.GetDomain(),
		Schema:   request.SchemaID,
		Columns:  columns,
	}, &allContent)
	if err != nil {
		return nil, "", title, fmt.Errorf("GenTable: %w", err)
	}

	return []common.ModDOM{{
		DeleteSelector: "#polyappJSMain > *",
		InsertSelector: "#polyappJSMain",
		Action:         "afterbegin",
		HTML:           `<div class="container">` + allContent.String() + "</div>",
	}}, "", "Change Data", nil
}

// signInPage lets you sign in with a username or password or, alternatively, choose to sign in with Google via a redirect.
//
// This page must be implemented as a hard coded Task because we are trying to not save the password in Firestore.
func signInPage(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, title string, err error) {
	if request.UserID != "" && request.UserID != "polyappNone" {
		// already signed in
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML: `<div class="container">
	<p>You are already signed in.</p>
	<button class="btn btn-primary" onclick="signOut()">Sign Out</button>
</div>`,
		}}, "", title, nil
	}
	if request.MessageID != "" {
		// POST request
		return []common.ModDOM{{}}, "", title, nil
	}

	googleIsEnabled, appleIsEnabled, microsoftIsEnabled, err := getEnabledAuthProviders()
	if err != nil {
		return nil, "", "", fmt.Errorf("getEnabledAuthProviders: %w", err)
	}
	var allContent bytes.Buffer
	allContent.WriteString(`<h1>Sign In</h1>`)
	if googleIsEnabled {
		allContent.WriteString(`<button class="btn btn-primary m-1" onclick="signInGoogle()">Sign In with Google</button>`)
	}
	if microsoftIsEnabled {
		allContent.WriteString(`<button class="btn btn-primary m-1" onclick="signInOAuth('microsoft.com')">Sign In with Microsoft</button>`)
	}
	if appleIsEnabled {
		allContent.WriteString(`<button class="btn btn-primary m-1" onclick="signInOAuth('apple.com')">Sign In with Apple</button>`)
	}
	allContent.WriteString(`
<div class="form-group" >
	<label for="polyappSignInEmail">Email</label>
	<input type="text" class="form-control" id="polyappSignInEmail" value="" />
</div>
<div class="form-group" >
	<label for="polyappSignInPassword">Password</label>
	<input type="text" class="form-control" id="polyappSignInPassword" value="" />
</div>
<button class="btn btn-primary" onclick="signInLocal()" id="signInButton">Sign In</button>
`)
	return []common.ModDOM{{
		DeleteSelector: "#polyappJSMain > *",
		InsertSelector: "#polyappJSMain",
		Action:         "afterbegin",
		HTML:           `<div class="container">` + allContent.String() + "</div>",
	}}, "", title, nil
}

// changePageRedirects should be called by pages which are trying to modify the Industry, Domain, Task, Schema, UX, or
// Data of a page. This code will detect which page you are trying to access and if not all of the prerequisite information
// for that page is available it helps redirect the users to the beginning of their lack of information.
//
// If a redirect is necessary, newURL != "", modDOMs is set, and there is no error returned.
func changePageRedirects(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, err error) {
	var w bytes.Buffer
	if request.PublicPath != "/polyappChangeContext/" && (request.GetIndustry() == "" || request.GetIndustry() == "polyappNone" || request.GetDomain() == "" ||
		request.GetDomain() == "polyappNone") {
		// path can be taken by /polyappChangeTask/ or /polyappChangeSchema/ or /polyappChangeUX/ or /polyappChangeData/
		w.WriteString(`<p>Context is not defined. Please click on 'Change Domain' (the folder icon) above.`)
		newURL := "/polyappChangeContext?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() + "&user=" + request.UserID + "&role=" + request.RoleID
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML:           w.String(),
		}}, newURL, nil
	} else if (request.PublicPath != "/polyappChangeTask/" && request.PublicPath != "/polyappChangeContext/") &&
		(request.GetTask() == "" || request.GetTask() == "polyappNone" || request.SchemaID == "" || request.SchemaID == "polyappNone" ||
			request.UXID == "" || request.UXID == "polyappNone") {
		// path can be taken by /polyappChangeSchema/ or /polyappChangeUX/ or /polyappChangeData/
		// This Task sets the Task, Schema, and UX so if we lack any of those 3 pieces of information redirect the browser here
		w.WriteString(`<p>Task is not defined. Please click on 'Change Task' (the briefcase icon) above.`)
		newURL := "/polyappChangeTask?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() + "&user=" + request.UserID +
			"&role=" + request.RoleID + "&task=" + request.GetTask() +
			"&schema=polyappTask" // workaround
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML:           w.String(),
		}}, newURL, nil
	}
	return nil, "", nil
}

func homePage(request common.POSTRequest) (modDOMs []common.ModDOM, newURL string, title string, err error) {
	var allContent bytes.Buffer
	if request.UserID == "" || request.UserID == "polyappNone" {
		// Not logged in yet or we don't have access to the User ID because the Auth header was not set.
		// Currently it is very difficult to access this page because the page requires Auth and Auth sets request.UserID.
		allContent.WriteString(`<h1>Welcome to Polyapp</h1>`)
		allContent.WriteString(`<p>Hello and welcome to Polyapp! To get started, click on the "Change Domain" button above. If you have any questions try clicking on the Help Icons which appear on most pages.</p>`)
		return []common.ModDOM{{
			DeleteSelector: "#polyappJSMain > *",
			InsertSelector: "#polyappJSMain",
			Action:         "afterbegin",
			HTML:           `<div class="container">` + allContent.String() + "</div>",
		}}, "", title, nil
	}

	if request.UserCache == nil {
		user, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return nil, "", title, fmt.Errorf("allDB.ReadUser: %w", err)
		}
		request.UserCache = &user
	}
	if request.UserCache.HomeTask == "DisplayDashboard" && request.UserCache.HomeUX == "DisplayDashboard" && request.UserCache.HomeSchema == "DisplayDashboard" {
		return []common.ModDOM{{}}, "/blob/assets/" + request.UserCache.HomeData + "/polyapp_Reporting_ZhcLBvwSBdjcpFVApHUyAygqv_Dashboard%20HTML?cacheBuster=kljlkjlkj", "", nil
	}
	// Default Task, UX, Schema, and Data are designed to point to a Task which gets information on the currently logged
	// in user.
	rewrittenRequest := common.GETRequest{
		IndustryID:         "polyapp",
		OverrideIndustryID: "",
		DomainID:           "User",
		OverrideDomainID:   "",
		TaskID:             common.HomeTaskID,
		OverrideTaskID:     "",
		UXID:               common.HomeUXID,
		SchemaID:           common.HomeSchemaID,
		DataID:             common.HomeDataID,
		UserID:             request.UserID,
		UserCache:          request.UserCache,
		RoleID:             request.RoleID,
		ModifyID:           request.ModifyID,
		FinishURL:          "",
		PublicPath:         "",
	}
	if request.UserCache.HomeTask != "" && request.UserCache.HomeUX != "" && request.UserCache.HomeSchema != "" &&
		request.UserCache.HomeData != "" {
		rewrittenRequest.TaskID = request.UserCache.HomeTask
		rewrittenRequest.UXID = request.UserCache.HomeUX
		rewrittenRequest.SchemaID = request.UserCache.HomeSchema
		rewrittenRequest.DataID = request.UserCache.HomeData
	}
	var HTML string
	ux, redirectURL, _, err := InnerHTML(rewrittenRequest)
	if err != nil {
		return nil, "", title, fmt.Errorf("innerHTML: %w", err)
	}
	HTML = *ux.HTML
	return []common.ModDOM{{
		DeleteSelector: "#polyappJSMain > *",
		InsertSelector: "#polyappJSMain",
		Action:         "afterbegin",
		HTML:           HTML,
	}}, redirectURL, "Polyapp", nil
}

func writeBreadcrumb(request common.POSTRequest, allContent *bytes.Buffer) error {
	industryLink := "/polyappChangeContext?industry=" + request.GetIndustry() + "&domain=polyappNone"
	domainLink := "/polyappChangeContext?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain()
	taskLink := "/polyappChangeTask?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() + "&schema=polyappTask"
	//dataLink := "/polyappChangeData?industry="+request.GetIndustry()+"&domain="+request.GetDomain()+
	//	"&task="+request.GetTask()+"&schema="+request.SchemaID+"&ux="+request.UXID
	if request.GetTask() != "polyappNone" && request.GetTask() != "polyappNone" && request.SchemaID != "" && request.UXID != "" {
		// Change Data page
		allContent.WriteString(`
<div class="row align-items-center">
<nav aria-label="Breadcrumb" class="p-1 col-10">
	<ol class="breadcrumb m-0">`)
		allContent.WriteString(`<li class="breadcrumb-item"><a href="` + industryLink + `">`)
		allContent.WriteString(request.GetIndustry())
		allContent.WriteString(`</a></li>`)
		allContent.WriteString(`<li class="breadcrumb-item"><a href="` + domainLink + `">`)
		allContent.WriteString(request.GetDomain())
		allContent.WriteString(`</a></li>`)
		task, err := allDB.ReadTask(request.GetTask())
		if err != nil {
			return fmt.Errorf("allDB.ReadTask: %w", err)
		}
		allContent.WriteString(`<li class="breadcrumb-item"><a href="` + taskLink + `">`)
		allContent.WriteString(task.Name)
		allContent.WriteString(`</a></li>`)
		allContent.WriteString(`<li class="breadcrumb-item active">Change Data</li>`)
		allContent.WriteString(`</ol>
</nav>
<div class="col-2">
	<button id="btnEditTask" class="btn btn-primary">Edit Task</button>
</div>
</div>`)
		return nil
	}
	if request.SchemaID == "polyappTask" && request.GetDomain() != "" && request.GetDomain() != "polyappNone" &&
		request.GetIndustry() != "" && request.GetIndustry() != "polyappNone" {
		// Change Task page
		allContent.WriteString(`<div class="row align-items-center">
	<nav aria-label="Breadcrumb" class="p-1 col-10">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="` + industryLink + `">` + request.GetIndustry() + `</a></li>
			<li class="breadcrumb-item"><a href="` + domainLink + `">` + request.GetDomain() + `</a></li>
			<li class="breadcrumb-item active">Change Task</li>
		</ol>
	</nav>
</div>
`)
		return nil
	}
	return nil
}

type defaultSupportedIdpConfigs struct {
	Name string `json:"name"`
}

type listDefaultSupportedIDPConfigs struct {
	DefaultSupportedIdpConfigs []defaultSupportedIdpConfigs `json:"defaultSupportedIdpConfigs"`
}

func getEnabledAuthProviders() (googleIsEnabled, appleIsEnabled, microsoftIsEnabled bool, err error) {
	req, err := http.NewRequest("GET", "https://identitytoolkit.googleapis.com/admin/v2/projects/"+common.GetGoogleProjectID()+"/defaultSupportedIdpConfigs", nil)
	if err != nil {
		err = fmt.Errorf("http.NewRequest: %w", err)
		return
	}
	client, err := google.DefaultClient(context.Background(), []string{"https://www.googleapis.com/auth/firebase"}...)
	if err != nil {
		err = fmt.Errorf("google.DefaultClient: %w", err)
		return
	}
	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("http.Get: %w", err)
		return
	}
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("ioutil.ReadAll: %w", err)
		return
	}
	if resp.StatusCode != 200 {
		err = fmt.Errorf("resp.StatusCode != 200")
		return
	}
	l := listDefaultSupportedIDPConfigs{}
	err = json.Unmarshal(responseBody, &l)
	if err != nil {
		err = fmt.Errorf("json.Unmarshal: %w", err)
		return
	}
	for _, config := range l.DefaultSupportedIdpConfigs {
		if strings.Contains(config.Name, "google.com") {
			googleIsEnabled = true
		} else if strings.Contains(config.Name, "apple.com") {
			appleIsEnabled = true
		} else if strings.Contains(config.Name, "microsoft.com") {
			microsoftIsEnabled = true
		}
	}

	return
}

func getIndustries() ([]string, map[string]bool) {
	return []string{
			"Construction",
			"Education and Health Services",
			"Financial Activities",
			"Information",
			"Leisure and Hospitality",
			"Manufacturing",
			"Natural Resources and Mining",
			"Other Services (except Public Administration)",
			"Professional and Business Services",
			"Trade, Transportation, and Utilities",
			"polyapp",
		}, map[string]bool{
			"Construction":                                  true,
			"Education and Health Services":                 true,
			"Financial Activities":                          true,
			"Information":                                   true,
			"Leisure and Hospitality":                       true,
			"Manufacturing":                                 true,
			"Natural Resources and Mining":                  true,
			"Other Services (except Public Administration)": true,
			"polyapp":                                       true,
		}
}
