package common

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Queryable implementation is a prerequisite for using the Query interface because Query needs to use Iter which returns a Queryable.
type Queryable interface {
	Validate() error
	Simplify() (map[string]interface{}, error)
	GetFirestoreID() string
	// Init should populate the structure with the contents of 'simple'. It assumes there are no existing contents.
	//
	// Init implementations MUST set Queryable.FirestoreID = simple[polyappFirestoreID].
	Init(simple map[string]interface{}) error
	// CollectionName is something like common.CollectionData or "schema". This helps with the creation of generic methods.
	CollectionName() string
}

// IterDone is returned by an iterator's Next method when the iteration is
// complete; when there are no more items to return.
var IterDone = errors.New("no more items in iterator")

// Iter lets you iterate over results from a query. It is created with a function like 'CreateDBIterator'.
//
// The Iter interface design is based on Firestore's iterator interface design.
type Iter interface {
	Next() (Queryable, error)
	Stop()
	// Length is number of available results.
	Length() int
}

// Query in a database-agnostic way. You must Init the Query, optionally add query constraints like "AddEquals",
// and then call QueryRead(). QueryRead() returns an iterator interface which can be used to iterate over the query results.
//
// The Query interface design is based on Firestore's query interface design.
type Query interface {
	// Init the query. This should be called immediately after creating the Query object.
	Init(industryID string, domainID string, schemaID string, firestoreCollection string)
	// AddEquals ensures the key k's value == v, a string
	AddEquals(k string, v string)
	// AddArrayContains ensures the key k's value, which should be an array, contains the value v.
	AddArrayContains(k string, v string) error
	// QueryRead uses the query to perform a read via an Iterator interface.
	QueryRead() (Iter, error)
}

// Task stores validation requirements, a list of bots, and bot configuration data.
// Its Name is shown at the top of the form which you see when loading the Task.
//
// When someone thinks of Polyapp they will probably think of Tasks as a collection of things they see, the database
// schema, and the validation for the Task. In reality the Task struct and document in the database has no reference
// to the schema or the user interface or the data.
//
// The Task is not directly coupled to Data, Schema, or UX. Instead it can merely be 'satisfied' by the Data. This
// setup means one Task could be used with multiple Schema, UX, or Data.
type Task struct {
	FirestoreID string `json:"polyappFirestoreID"`
	IndustryID  string `json:"polyappIndustryID"`
	DomainID    string `json:"polyappDomainID"`
	// Name associated with the Task.
	Name string
	// HelpText associated with the Task.
	HelpText string
	// TaskGoals should always be created programmatically with TaskGoalAdd()
	//
	// TaskGoals should always be evaluated with TaskGoalEvaluate()
	//
	// TaskGoals are stored internally in a JSON format derived from the 'taskGoal' struct with keys which are either
	// prefixed with 'polyappError' which means they're field-specific or which are NOT prefixed & therefore task-level.
	TaskGoals map[string]string
	// FieldSecurity holds access control properties which are related to the ability to access a field.
	FieldSecurity map[string]*FieldSecurityOptions
	// BotsTriggeredAtLoad are IDs of Bots which are triggered on every GET request for this task.
	// When creating one of these Bots, keep in mind that the user will be waiting at a loading screen while this is happening.
	BotsTriggeredAtLoad []string
	// BotsTriggeredContinuously are IDs of Bots which are triggered on every POST request.
	// Since POST requests are triggered automatically and periodically as part of auto-save these Bots run frequently.
	//
	// These Bots run before BotsTriggeredAtDone.
	BotsTriggeredContinuously []string
	// BotsTriggeredAtDone are IDs of Bots which are triggered when the "Done" value is set to True.
	BotsTriggeredAtDone []string
	// BotStaticData is provided to bots when they are run. This data is supposed to be read-only and encapsulates
	// information which changes on a Task by Task basis but not a Data by Data basis. For example there is an Action
	// which can open a new Data for a Task when a user clicks "Done". This Action allows you to configure which Data to
	// open next by inputting a Query into the Task's BotStaticData. The Action knows which current Data
	// you are on, so it can run the Query with StartAfter set to the current data, thereby returning the next result
	// in the Query.
	//
	// Keys are the last chunk of fields after the last "_" (aka the field's Name). Values are arbitrary and might be parsed to ints or floats.
	BotStaticData map[string]string
	// IsPublic is true if this is publicly accessible. It is false or not set otherwise.
	//
	// IsPublic may eventually be deprecated when we figure out a more granular access control method.
	IsPublic   bool
	Deprecated *bool `json:"polyappDeprecated" firestore:"polyappDeprecated"`
}

// FieldSecurityOptions holds access control information about a particular field like: industry_domain_schema_field name
type FieldSecurityOptions struct {
	Readonly bool
}

// Validate returns an error for improper data.
func (t *Task) Validate() error {
	if t.FirestoreID == "" {
		return errors.New("FirestoreID not set")
	}
	if t.IndustryID == "" || t.DomainID == "" {
		return fmt.Errorf("IndustryID (%v) or DomainID (%v) not set", t.IndustryID, t.DomainID)
	}
	if t.Name == "" {
		return errors.New("Name not set")
	}
	if t.HelpText == "" {
		return errors.New("HelpText not set")
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (t *Task) GetFirestoreID() string {
	return t.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (t *Task) CollectionName() string {
	return CollectionTask
}

// Init translates a raw map into the Task type. It does NOT set FirestoreID.
func (t *Task) Init(simple map[string]interface{}) error {
	t.FieldSecurity = make(map[string]*FieldSecurityOptions)
	if simple["FieldSecurity"] != nil {
		for k, v := range simple["FieldSecurity"].(map[string]interface{}) {
			switch typedV := v.(type) {
			case map[string]interface{}:
				fieldSecurityOptions := &FieldSecurityOptions{}
				fieldSecurityOptions.Readonly, _ = typedV["Readonly"].(bool)
				t.FieldSecurity[k] = fieldSecurityOptions
			}
		}
	}
	t.BotStaticData = make(map[string]string)
	t.TaskGoals = make(map[string]string)
	Dereference(simple)
	sElements := reflect.ValueOf(t).Elem()
	return populateType(sElements, simple)
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (t *Task) Simplify() (map[string]interface{}, error) {
	outMap := map[string]interface{}{
		PolyappIndustryID: t.IndustryID,
		PolyappDomainID:   t.DomainID,
		"IsPublic":        t.IsPublic,
	}
	if t.Deprecated != nil {
		outMap[PolyappDeprecated] = *t.Deprecated
	}
	if t.TaskGoals != nil {
		outMap["TaskGoals"] = t.TaskGoals
	}
	if t.FieldSecurity != nil {
		outMap["FieldSecurity"] = t.FieldSecurity
	}
	if t.Name != "" {
		outMap["Name"] = t.Name
	}
	if t.HelpText != "" {
		outMap["HelpText"] = t.HelpText
	}
	if t.BotsTriggeredAtLoad != nil {
		outMap["BotsTriggeredAtLoad"] = t.BotsTriggeredAtLoad
	}
	if t.BotsTriggeredContinuously != nil {
		outMap["BotsTriggeredContinuously"] = t.BotsTriggeredContinuously
	}
	if t.BotsTriggeredAtDone != nil {
		outMap["BotsTriggeredAtDone"] = t.BotsTriggeredAtDone
	}
	if t.BotStaticData != nil {
		outMap["BotStaticData"] = t.BotStaticData
	}
	return outMap, nil
}

// UX contains both the UI as stored in the HTML field and data which helps render that UI.
//
// This structure is at the bottom of a dependency chain. Task is satisfied by some Schemas; Schema is satisfied by Data which
// is following that Schema. UX is different in that it can contain nested components which could come from different Data
// documents which follow different Schemas. So a simple UX with no ARef or Ref is satisfied by a single Schema, but
// if it contains ARef or Ref keys the schema must contain those too.
type UX struct {
	FirestoreID string `json:"polyappFirestoreID"`
	IndustryID  string `json:"polyappIndustryID"`
	DomainID    string `json:"polyappDomainID"`
	SchemaID    string `json:"polyappSchemaID"`
	// HTML is everything this UX needs to host some Task or Tasks. It does NOT include navigation or metadata.
	HTML *string
	// Navbar is the navigation bar HTML.
	Navbar *string
	// Title is a value in the header of an HTML page: https://www.w3schools.com/tags/tag_title.asp
	Title *string
	// MetaDescription is one of the named meta HTML elements: https://www.w3schools.com/tags/tag_meta.asp
	MetaDescription *string
	// MetaKeywords is one of the named meta HTML elements: https://www.w3schools.com/tags/tag_meta.asp
	MetaKeywords *string
	// MetaAuthor is one of the named meta HTML elements: https://www.w3schools.com/tags/tag_meta.asp
	MetaAuthor *string
	// HeadTag contains additional content which is directly placed into the Head tag. This could include scripts.
	HeadTag *string
	// IconPath is the path to an icon you can show in the upper right hand corner.
	IconPath *string
	// CSSPath is the path you can call to get a custom CSS file. If not set, the CSS which is loaded is Bootstrap's default CSS.
	//
	// This may also be known as "ThemePath" from the User.
	CSSPath *string
	// SelectFields are Fields / UI IDs in THIS SCHEMA -> search field, aka the Field in the searched schema.
	// To use this, when the UI is being loaded you must run the query given in the value field. Use the values you
	// get from that query to populate the Select markup options.
	//
	// This could also be implemented via a data-* attribute, but doing so would require trusting the client to decide
	// what to search. Since searching is inherently more expensive than other operations things have been implemented
	// this way instead.
	SelectFields map[string]string
	// IsPublic is true if this is publicly accessible. It is false or not set otherwise.
	IsPublic bool
	// FullAuthPage is True if you want to demand users use full authentication.
	FullAuthPage *bool
	Deprecated   *bool `json:"polyappDeprecated" firestore:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (ux *UX) Validate() error {
	if ux.FirestoreID == "" {
		return errors.New("FirestoreID must be set")
	}
	if ux.IndustryID == "" || ux.DomainID == "" || ux.SchemaID == "" {
		return fmt.Errorf("must set industry (%v), domain (%v) and schema (%v) information", ux.IndustryID, ux.DomainID, ux.SchemaID)
	}
	if ux.SelectFields == nil {
		return errors.New("SelectFields must not be nil")
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (ux *UX) GetFirestoreID() string {
	return ux.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (ux *UX) CollectionName() string {
	return CollectionUX
}

// Init translates a raw map into the UX type. It does NOT set FirestoreID.
func (ux *UX) Init(simple map[string]interface{}) error {
	ux.SelectFields = make(map[string]string)
	Dereference(simple)
	sElements := reflect.ValueOf(ux).Elem()
	return populateType(sElements, simple)
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (ux *UX) Simplify() (map[string]interface{}, error) {
	outMap := map[string]interface{}{
		PolyappIndustryID: ux.IndustryID,
		PolyappDomainID:   ux.DomainID,
		PolyappSchemaID:   ux.SchemaID,
		"IsPublic":        ux.IsPublic,
	}
	if ux.HTML != nil {
		outMap["HTML"] = *ux.HTML
	}
	if ux.SelectFields != nil {
		outMap["SelectFields"] = ux.SelectFields
	}
	if ux.Title != nil {
		outMap["Title"] = ux.Title
	}
	if ux.MetaDescription != nil {
		outMap["MetaDescription"] = ux.MetaDescription
	}
	if ux.MetaKeywords != nil {
		outMap["MetaKeywords"] = ux.MetaKeywords
	}
	if ux.MetaAuthor != nil {
		outMap["MetaAuthor"] = ux.MetaAuthor
	}
	if ux.HeadTag != nil {
		outMap["HeadTag"] = ux.HeadTag
	}
	if ux.IconPath != nil {
		outMap["IconPath"] = ux.IconPath
	}
	if ux.Navbar != nil {
		outMap["Navbar"] = ux.Navbar
	}
	if ux.CSSPath != nil {
		outMap["CSSPath"] = ux.CSSPath
	}
	if ux.FullAuthPage != nil {
		outMap["FullAuthPage"] = ux.FullAuthPage
	}
	if ux.Deprecated != nil {
		outMap[PolyappDeprecated] = *ux.Deprecated
	}
	return outMap, nil
}

// User is someone using the software.
// Roles can be nil.
type User struct {
	FirestoreID   string  `json:"polyappFirestoreID" suffix:"User ID"`
	UID           *string `suffix:"UID"`
	FullName      *string `suffix:"Full Name"`
	PhotoURL      *string `suffix:"Photo URL"`
	EmailVerified *bool   `suffix:"Email Verified"`
	PhoneNumber   *string `suffix:"Phone Number"`
	Email         *string `suffix:"Email"`
	// Roles holds Firestore IDs of Role documents
	Roles []string `suffix:"Roles"`
	// BotsTriggeredAtLoad are triggered every time this User loads a Task.
	BotsTriggeredAtLoad []string `suffix:"Bots Triggered At Load"`
	// BotsTriggeredAtDone are triggered every time this User clicks "Done" on a Task.
	BotsTriggeredAtDone []string `suffix:"Bots Triggered At Done"`
	HomeTask            string   `suffix:"Home Task"`
	HomeUX              string   `suffix:"Home UX"`
	HomeSchema          string   `suffix:"Home Schema"`
	HomeData            string   `suffix:"Home Data"`
	// ThemePath like /blob/assets/??/polyapp_TaskSchemaUX_KxZRoxrRpyECTbCudVfXKnmHB_Bootstrap%20Theme?cacheBuster=1628015252293
	ThemePath  *string `suffix:"Theme Path"`
	Deprecated *bool   `json:"polyappDeprecated" firestore:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (u *User) Validate() error {
	if u == nil {
		return errors.New("User was nil")
	}
	if u.FirestoreID == "" {
		return errors.New("User must have a FirestoreID")
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (u *User) GetFirestoreID() string {
	return u.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (u *User) CollectionName() string {
	return CollectionUser
}

// Init translates a raw map into the User type. It sets FirestoreID if possible.
func (u *User) Init(simple map[string]interface{}) error {
	Dereference(simple)
	sElements := reflect.ValueOf(u).Elem()
	return populateType(sElements, simple)
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (u *User) Simplify() (map[string]interface{}, error) {
	outMap := make(map[string]interface{})
	if u.Deprecated != nil {
		outMap[PolyappDeprecated] = *u.Deprecated
	}
	if u.UID != nil {
		outMap["UID"] = *u.UID
	}
	if u.Roles != nil {
		outMap["Roles"] = u.Roles
	}
	if u.EmailVerified != nil {
		outMap["EmailVerified"] = u.EmailVerified
	}
	if u.FullName != nil {
		outMap["FullName"] = u.FullName
	}
	if u.PhoneNumber != nil {
		outMap["PhoneNumber"] = u.PhoneNumber
	}
	if u.Email != nil {
		outMap["Email"] = u.Email
	}
	if u.PhotoURL != nil {
		outMap["PhotoURL"] = u.PhotoURL
	}
	if u.BotsTriggeredAtLoad != nil {
		outMap["BotsTriggeredAtLoad"] = u.BotsTriggeredAtLoad
	}
	if u.BotsTriggeredAtDone != nil {
		outMap["BotsTriggeredAtDone"] = u.BotsTriggeredAtDone
	}
	if u.HomeTask != "" {
		outMap["HomeTask"] = u.HomeTask
	}
	if u.HomeSchema != "" {
		outMap["HomeSchema"] = u.HomeSchema
	}
	if u.HomeUX != "" {
		outMap["HomeUX"] = u.HomeUX
	}
	if u.HomeData != "" {
		outMap["HomeData"] = u.HomeData
	}
	if u.ThemePath != nil {
		outMap["ThemePath"] = *u.ThemePath
	}
	return outMap, nil
}

// Access defines what Industry, Domain, and (optionally) Schema it grants access to and which privileges are granted.
type Access struct {
	// Industry is the Industry the Datas being granted access to are in.
	Industry string
	// Domain is the Domain of the Datas being granted access to.
	Domain string
	// Schema (optional) of the Datas being granted access to.
	// If Schema is not set or is empty then this Access grants access to all Schemas in the Industry/Domain combination.
	Schema string
	// Create access allows a user to "Create" a new Data, usually with the "Create Data" button.
	Create bool
	// Read access allows a user to perform GET requests with the Data, but never POST or similar requests.
	Read bool
	// Update access allows a user to edit an existing Data.
	Update bool
	// Delete access allows a user to delete a Data.
	Delete bool
	// KeyValue grants custom access control attributes. For example, "EditTask": true grants access to using the Edit
	// Task Bot.
	KeyValue map[string]string `suffix:"Key Value"`
}

// Role is a Role-collection object pointed to by a User.
// Its arrays can be nil; that just means they weren't set in the request.
//
// Note: Access is stored as a map in the database. Firestore supports []map[string]interface{}
type Role struct {
	FirestoreID string `json:"polyappFirestoreID" suffix:"Role ID"`
	Name        string // Name of this role. Used when searching for roles.
	// Access defines to what a Role grants access.
	//
	// key: industry_domain
	// value: one or more of the letters 'c' 'r' 'u' 'd'
	//
	// "domain" could contain an underscore. But since no Industry contains an underscore we know the first underscore must be the delimiter.
	Access     []Access `json:"Access" suffix:"Access"`
	Deprecated *bool    `json:"polyappDeprecated" firestore:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (r *Role) Validate() error {
	if r == nil {
		return errors.New("role was nil")
	}
	if r.FirestoreID == "" {
		return errors.New("role must have a Firestore ID")
	}
	for i := range r.Access {
		if r.Access[i].Industry == "" {
			return fmt.Errorf("r.Access[%v].Industry was empty string", i)
		}
		if r.Access[i].Domain == "" {
			return fmt.Errorf("r.Access[%v].Domain was empty string", i)
		}
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (r *Role) GetFirestoreID() string {
	return r.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (r *Role) CollectionName() string {
	return CollectionRole
}

// Init translates a raw map into the Role type. It does NOT set FirestoreID.
func (r *Role) Init(simple map[string]interface{}) error {
	var ok bool
	for k, v := range simple {
		switch k {
		case "Access":
			accessCastArray := v.([]interface{})
			accessCast := make([]map[string]interface{}, len(accessCastArray))
			for i := range accessCastArray {
				accessCast[i], ok = accessCastArray[i].(map[string]interface{})
				if !ok {
					return fmt.Errorf("could not cast accessCastArray to map[string]interface{} at index %v", i)
				}
			}
			access := make([]Access, len(accessCast))
			for i := range accessCast {
				industry, ok := accessCast[i]["Industry"].(string)
				if !ok {
					return fmt.Errorf("could not cast Industry to string at Access index %v", i)
				}
				domain, ok := accessCast[i]["Domain"].(string)
				if !ok {
					return fmt.Errorf("could not cast Domain to string at Access index %v", i)
				}
				schema, ok := accessCast[i]["Schema"].(string)
				if !ok {
					return fmt.Errorf("could not cast Schema to string at Access index %v", i)
				}
				create, ok := accessCast[i]["Create"].(bool)
				if !ok {
					return fmt.Errorf("could not cast Create to bool at Access index %v", i)
				}
				read, ok := accessCast[i]["Read"].(bool)
				if !ok {
					return fmt.Errorf("could not cast Read to bool at Access index %v", i)
				}
				update, ok := accessCast[i]["Update"].(bool)
				if !ok {
					return fmt.Errorf("could not cast Update to bool at Access index %v", i)
				}
				del, ok := accessCast[i]["Delete"].(bool)
				if !ok {
					return fmt.Errorf("could not cast Delete to bool at Access index %v", i)
				}
				keyValueMap := make(map[string]string)
				switch keyValue := accessCast[i]["Key Value"].(type) {
				case []string:
					for _, s := range keyValue {
						split := strings.Split(s, " ")
						if len(split) != 2 || len(split[0]) < 1 || len(split[1]) < 1 {
							continue
						}
						keyValueMap[split[0]] = split[1]
					}
				case map[string]interface{}:
					for keyValueKey, keyValueValue := range keyValue {
						switch v := keyValueValue.(type) {
						case string:
							keyValueMap[keyValueKey] = v
						}
					}
				default:
					// Key Value can be empty so don't return an error
				}
				access[i] = Access{
					Industry: industry,
					Domain:   domain,
					Schema:   schema,
					Create:   create,
					Read:     read,
					Update:   update,
					Delete:   del,
					KeyValue: keyValueMap,
				}
			}
			r.Access = access
		case "polyappFirestoreID", "FirestoreID":
			r.FirestoreID = v.(string)
		case "Deprecated", "polyappDeprecated":
			if v != nil {
				r.Deprecated = Bool(v.(bool))
			}
		case "Name":
			if v != nil {
				r.Name = v.(string)
			}
		}
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (r *Role) Simplify() (map[string]interface{}, error) {
	outMap := make(map[string]interface{})
	accessOut := make([]map[string]interface{}, len(r.Access))
	for i := range r.Access {
		accessOut[i] = map[string]interface{}{
			"Industry":  r.Access[i].Industry,
			"Domain":    r.Access[i].Domain,
			"Schema":    r.Access[i].Schema,
			"Create":    r.Access[i].Create,
			"Read":      r.Access[i].Read,
			"Update":    r.Access[i].Update,
			"Delete":    r.Access[i].Delete,
			"Key Value": r.Access[i].KeyValue,
		}
	}
	outMap["Access"] = accessOut
	outMap["Name"] = r.Name
	if r.Deprecated != nil {
		outMap[PolyappDeprecated] = r.Deprecated
	}
	return outMap, nil
}

// Data holds the values people enter when filling out forms.
// It can be difficult to interact with directly because you need to know, for
// a given key, what data type it is. This 'data type definition' is provided by type Schema.
// I have previously tried to leave this data structure inside a more generic map, but I found that to be even more difficult to use.
//
// IndustryID, DomainID and SchemaID provide context. For right now, SchemaID is often set to the ID of the schema.
type Data struct {
	FirestoreID string `json:"polyappFirestoreID"`
	IndustryID  string `json:"polyappIndustryID" firestore:"polyappIndustryID"`
	DomainID    string `json:"polyappDomainID" firestore:"polyappDomainID"`
	SchemaID    string `json:"polyappSchemaID" firestore:"polyappSchemaID"`
	Deprecated  *bool  `json:"polyappDeprecated" firestore:"polyappDeprecated"`
	// SchemaCache is the Schema of this Data. The Schema can't change after the Data has been read, so
	// it should be safe to cache it. Schema is extremely useful for BackPopulate and similar functions.
	SchemaCache Schema `json:"-"`
	// Ref is used for complicated data structures where you need objects within objects. Its format is that of a link:
	//
	// /[industry]/[domain]/[task]?ux=[UXID]&schema=[SchemaID]&data=[dataID]
	//
	// You can create links with common.CreateRef().
	//
	// There are two ways to display a Ref to a user. One is to display the link within an <a> element.
	// The other renders the content of the task as an embedded Task. The decision on which to do is taken by the UX object.
	// See the UX struct for more documentation on this issue.
	//
	// Note: although Refs are stored with an _ prefix in Schema, inside Data they are stored without the _ prefix.
	Ref map[string]*string
	// S is string data
	S map[string]*string
	// I is int data. Uses 64 bits even though JS only does some operations on 32-bit integers
	I map[string]*int64
	// B is boolean data.
	B map[string]*bool
	// F is floating point numbers (number in JS)
	F map[string]*float64
	// ARef is array of Ref. See Ref's description for help.
	ARef map[string][]string
	// AS is array of strings
	AS map[string][]string
	// AI is array of ints
	AI map[string][]int64
	// AB is array of bools
	AB map[string][]bool
	// AF is array of floats
	AF map[string][]float64
	// Bytes is an array of bytes. It is stored into an object whose name is: SchemaName_FirestoreID_Key
	Bytes map[string][]byte
	// ABytes is an array of blobs. It is stored into an object whose name is: SchemaName_FirestoreID_Key_index.
	ABytes map[string][][]byte
	// Deprecated. To delete a field, set its value to nil.
	Nils map[string]bool
}

// Init makes all of the maps and initializes them.
// If the inputs are non-nil, it populates DataTypes from the map[string]interface{} structure.
// If the input key exists and its value is nil the key is stored in Nils.
//
// Strings are assumed to be References if their keys are prefixed with "_".
//
// Keys are assumed to be PathEscaped, so for instance "%20" is unescaped to " ". The opposite of this operation is performed
// when the UX is created with encodings in its IDs - aka the opposite should already exist in the database.
func (d *Data) Init(simple map[string]interface{}) error {
	Dereference(simple)
	d.Nils = make(map[string]bool)
	d.Ref = make(map[string]*string)
	d.S = make(map[string]*string)
	d.I = make(map[string]*int64)
	d.F = make(map[string]*float64)
	d.B = make(map[string]*bool)
	d.AS = make(map[string][]string)
	d.ARef = make(map[string][]string)
	d.AI = make(map[string][]int64)
	d.AF = make(map[string][]float64)
	d.AB = make(map[string][]bool)
	d.Bytes = make(map[string][]byte)
	d.ABytes = make(map[string][][]byte)
	if simple == nil || len(simple) < 1 {
		return nil
	}
	for k, v := range simple {
		// Avoid URL encodings in names
		// TODO when creating a new field / allowing users to enter a new field make sure the "%" sign is not allowed.
		k, err := url.PathUnescape(k)
		if err != nil {
			return fmt.Errorf("PathUnescape: %w", err)
		}
		kNoPrefix := RemoveDataKeyUnderscore(k)
		if v == nil {
			d.Nils[kNoPrefix] = true
			continue
		}
		switch converted := v.(type) {
		case string:
			if k == PolyappSchemaID {
				d.SchemaID = converted
			} else if k == PolyappDomainID {
				d.DomainID = converted
			} else if k == PolyappIndustryID {
				d.IndustryID = converted
			} else if k == PolyappFirestoreID {
				d.FirestoreID = converted
			} else if []rune(k)[0] == '_' {
				if len(converted) > 0 && converted[0] != '/' {
					converted, err = url.PathUnescape(converted)
					if err != nil {
						return fmt.Errorf("PathUnescape for ref: %w", err)
					}
				}
				d.Ref[kNoPrefix] = String(converted)
			} else {
				d.S[kNoPrefix] = String(converted)
			}
		case int:
			// We can't currently support both floats and ints in the UI - they all look the same coming from JS.
			// TODO fix the Tasks so that if the Schema says it's an int, the task rejects non-integer inputs.
			// TODO Also use the Schema information to ensure fields are stored to the correct type.
			d.I[kNoPrefix] = Int64(int64(converted))
			//d.F[kNoPrefix] = Float64(float64(converted))
		case int64:
			d.I[kNoPrefix] = Int64(converted)
			//d.F[kNoPrefix] = Float64(float64(converted))
		case bool:
			if k == PolyappDeprecated {
				d.Deprecated = Bool(converted)
			} else {
				d.B[kNoPrefix] = Bool(converted)
			}
		case float64:
			d.F[kNoPrefix] = Float64(converted)
		case float32:
			d.F[kNoPrefix] = Float64(float64(converted))
		case []string:
			if []rune(k)[0] == '_' {
				d.ARef[kNoPrefix] = make([]string, len(converted))
				for arrayI, arrayV := range converted {
					if len(arrayV) > 0 && arrayV[0] != '/' {
						arrayV, err = url.PathUnescape(arrayV)
						if err != nil {
							return fmt.Errorf("PathUnescape for ref in array position "+strconv.Itoa(arrayI)+": %w", err)
						}
					}
					d.ARef[kNoPrefix][arrayI] = arrayV
				}
			} else {
				d.AS[kNoPrefix] = make([]string, len(converted))
				for arrayI, arrayV := range converted {
					d.AS[kNoPrefix][arrayI] = arrayV
				}
			}
		case []int64:
			d.AI[kNoPrefix] = converted

			//d.AF[kNoPrefix] = make([]float64, len(converted))
			//for arrayI, arrayV := range converted {
			//	d.AF[kNoPrefix][arrayI] = float64(arrayV)
			//}
		case []float64:
			d.AF[kNoPrefix] = converted
		case []bool:
			d.AB[kNoPrefix] = converted
		case []byte:
			d.Bytes[kNoPrefix] = converted
		case []interface{}:
			if len(converted) < 1 && k == kNoPrefix {
				d.AS[kNoPrefix] = make([]string, 0)
				continue
			} else if len(converted) < 1 && k != kNoPrefix {
				d.ARef[kNoPrefix] = make([]string, 0)
				continue
			}
			switch converted[0].(type) {
			case string:
				if []rune(k)[0] == '_' {
					d.ARef[kNoPrefix] = make([]string, len(converted))
					for arrayI, arrayV := range converted {
						if arrayV.(string)[0] != '/' {
							arrayV, err = url.PathUnescape(arrayV.(string))
							if err != nil {
								return fmt.Errorf("PathUnescape for ref in array position "+strconv.Itoa(arrayI)+": %w", err)
							}
						}
						d.ARef[kNoPrefix][arrayI] = arrayV.(string)
					}
				} else {
					d.AS[kNoPrefix] = make([]string, len(converted))
					for arrayI, arrayValue := range converted {
						d.AS[kNoPrefix][arrayI] = arrayValue.(string)
					}
				}
			case int64:
				d.AI[kNoPrefix] = make([]int64, len(converted))
				for arrayI, arrayValue := range converted {
					d.AI[kNoPrefix][arrayI] = arrayValue.(int64)
				}

				//d.AF[kNoPrefix] = make([]float64, len(converted))
				//for arrayI, arrayV := range converted {
				//	d.AF[kNoPrefix][arrayI] = float64(arrayV.(int64))
				//}
			case float64:
				d.AF[kNoPrefix] = make([]float64, len(converted))
				for arrayI, arrayValue := range converted {
					d.AF[kNoPrefix][arrayI] = arrayValue.(float64)
				}
			case bool:
				d.AB[kNoPrefix] = make([]bool, len(converted))
				for arrayI, arrayValue := range converted {
					d.AB[kNoPrefix][arrayI] = arrayValue.(bool)
				}
			case byte:
				d.Bytes[kNoPrefix] = make([]byte, len(converted))
				for arrayI, arrayValue := range converted {
					d.Bytes[kNoPrefix][arrayI] = arrayValue.(byte)
				}
			case []byte:
				d.ABytes[kNoPrefix] = make([][]byte, len(converted))
				for arrayI, arrayValue := range converted {
					d.ABytes[kNoPrefix][arrayI] = arrayValue.([]byte)
				}
			case []interface{}:
				return errors.New("[]interface{} within [], indicating we need to support this case for [][]byte")
			}
		}
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (d *Data) GetFirestoreID() string {
	return d.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (d *Data) CollectionName() string {
	return CollectionData
}

// Validate returns an error for improper data.
func (d *Data) Validate() error {
	if d == nil {
		return errors.New("DataTypes was nil")
	}
	if d.Ref == nil || d.S == nil || d.I == nil || d.B == nil || d.F == nil {
		return errors.New("must Init() DataTypes before calling Validate")
	}
	for k := range d.Nils {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") && k != PolyappDeprecated {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.I {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.F {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.B {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k, v := range d.Ref {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
		if v == nil || *v == "" {
			// ARef accepts an empty array, so we will accept an empty value for a Ref. It will have to be populated later.
			continue
		}
		err := ValidateRef(*v)
		if err != nil {
			return fmt.Errorf("could not parse d.Ref for key %v because: %w", k, err)
		}
	}
	for k := range d.S {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.AS {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.AI {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.AF {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.AB {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.Bytes {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k := range d.ABytes {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
	}
	for k, ARefValue := range d.ARef {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return fmt.Errorf("invalid key %v because prefix was polyapp", k)
		}
		for i, v := range ARefValue {
			err := ValidateRef(v)
			if err != nil {
				return fmt.Errorf("could not parse d.Ref for key: "+k+" index: "+strconv.Itoa(i)+" because: %w", err)
			}
		}
	}
	if d.IndustryID == "" || d.DomainID == "" || d.SchemaID == "" {
		return errors.New("industry or domain or schema were empty")
	}
	if d.FirestoreID == "" {
		return errors.New("Firestore ID was empty")
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
//
// Caveat: If Nils is set for a key k, & any other map is set at k, then Nils will take precedence. This enables deletion.
func (d *Data) Simplify() (map[string]interface{}, error) {
	if d.IndustryID == "" || d.DomainID == "" || d.SchemaID == "" {
		return nil, errors.New("Industry, Domain, and Schema are required due to the need to prefix the fields.")
	}
	simplified := map[string]interface{}{
		PolyappIndustryID: d.IndustryID,
		PolyappDomainID:   d.DomainID,
		PolyappSchemaID:   d.SchemaID,
	}
	if d.Deprecated != nil {
		simplified[PolyappDeprecated] = *d.Deprecated
	}
	for k, v := range d.Ref {
		kWithPrefix := k
		if k[0] != '_' {
			kWithPrefix = "_" + k
		}
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, fmt.Errorf("overlapping key id for Ref: %v", kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.F {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for F: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.B {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for B: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.I {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for I: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.S {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for S: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.ARef {
		kWithPrefix := k
		if k[0] != '_' {
			kWithPrefix = "_" + k
		}
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for ARef: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.AS {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for AS: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.AI {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for AI: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.AF {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for AF: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.AB {
		kWithPrefix := k
		_, ok := simplified[kWithPrefix]
		if ok {
			return nil, errors.New("overlapping key id for AB: " + kWithPrefix)
		}
		simplified[kWithPrefix] = v
	}
	for k, v := range d.Bytes {
		// After this operation there could be both a [_key] = some/ref/value && a [key] = somereallylongbytesValue
		_, ok := simplified[k]
		if ok {
			return nil, errors.New("overlapping key id for Bytes: " + k)
		}
		simplified[k] = v
	}
	for k, v := range d.ABytes {
		_, ok := simplified[k]
		if ok {
			return nil, errors.New("overlapping key id for ABytes: " + k)
		}
		simplified[k] = v
	}
	for k := range d.Nils {
		// nils skips the overlapping key ID to allow merged systems to override.
		delete(simplified, k)
		delete(simplified, "_"+k)
	}
	return simplified, nil
}

// Merge merges a Data 'toMerge' into the pointer receiver. It requires that they have the same FirestoreID.
//
// Merging basically creates a superset of the inputs. If you want to remove a value then set its key into the Nils in toMerge.
// When this function finds keys in the Nils category it searches the d *Data & replaces its value appropriately.
// Conversely if a value is set in the Nils in d *Data but has a real value in toMerge *Data, the Nils entry is removed.
// By removing the Nils entry we ensure the new value from toMerge overwrites the null value in d *Data.
func (d *Data) Merge(toMerge *Data) error {
	if toMerge.FirestoreID != d.FirestoreID {
		return fmt.Errorf("toMerge must have same FirestoreID as the pointer receiver on the method. toMerge.FirestoreID (%v) d.FirestoreID (%v)", toMerge.FirestoreID, d.FirestoreID)
	}
	// fix missing data. This should not be necessary but alas I stored a bunch of bad data in the DB by not using the allDB methods
	// and this is helping me fix my error.
	if d.IndustryID == "" {
		d.IndustryID = toMerge.IndustryID
	}
	if d.DomainID == "" {
		d.DomainID = toMerge.DomainID
	}
	if d.SchemaID == "" {
		d.SchemaID = toMerge.SchemaID
	}
	if toMerge.IndustryID != d.IndustryID || toMerge.DomainID != d.DomainID {
		return fmt.Errorf("toMerge does not have the same industry or domain (different Schemas are OK). toMerge.IndustryID (%v) d.IndustryID (%v) toMerge.DomainID (%v) d.DomainID (%v)", toMerge.IndustryID, d.IndustryID, toMerge.DomainID, d.DomainID)
	}
	if toMerge.Deprecated != nil {
		d.Deprecated = toMerge.Deprecated
	}
	for k, v := range toMerge.Ref {
		d.Ref[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.F {
		d.F[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.B {
		d.B[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.S {
		d.S[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.I {
		d.I[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.ARef {
		d.ARef[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.AF {
		d.AF[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.AB {
		d.AB[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.AS {
		d.AS[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.AI {
		d.AI[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.Bytes {
		d.Bytes[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.ABytes {
		d.ABytes[k] = v
		delete(d.Nils, k)
	}
	for k, v := range toMerge.Nils {
		d.Nils[k] = v
		delete(d.S, k)
		delete(d.B, k)
		delete(d.F, k)
		delete(d.I, k)
		delete(d.AS, k)
		delete(d.AB, k)
		delete(d.AF, k)
		delete(d.AI, k)
	}
	return nil
}

// GetString returns nil if the key is nil; a valid string otherwise
func (d *Data) GetString(suffix string) *string {
	return d.S[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetRef returns nil if the key is nil; a valid string otherwise
func (d *Data) GetRef(suffix string) *string {
	return d.Ref[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetInt returns nil if the key is nil; a valid int otherwise
func (d *Data) GetInt(suffix string) *int64 {
	return d.I[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetFloat returns nil if the key is nil; a valid float otherwise
func (d *Data) GetFloat(suffix string) *float64 {
	return d.F[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetBool returns nil if the key is nil; a valid bool otherwise
func (d *Data) GetBool(suffix string) *bool {
	return d.B[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetBytes returns nil if the key is nil; a valid []byte otherwise
func (d *Data) GetBytes(suffix string) []byte {
	return d.Bytes[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetAString returns nil if the key is nil; a valid []string otherwise
func (d *Data) GetAString(suffix string) []string {
	return d.AS[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetARef returns nil if the key is nil; a valid []string otherwise
func (d *Data) GetARef(suffix string) []string {
	return d.ARef[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetAInt returns nil if the key is nil; a valid []int otherwise
func (d *Data) GetAInt(suffix string) []int64 {
	return d.AI[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetAFloat returns nil if the key is nil; a valid []float otherwise
func (d *Data) GetAFloat(suffix string) []float64 {
	return d.AF[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetABool returns nil if the key is nil; a valid []bool otherwise
func (d *Data) GetABool(suffix string) []bool {
	return d.AB[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// GetABytes returns nil if the key is nil; a valid [][]byte otherwise
func (d *Data) GetABytes(suffix string) [][]byte {
	return d.ABytes[AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, suffix)]
}

// Schema of some Data. Schema must play well with both the corresponding Data and the UX and Task.
type Schema struct {
	FirestoreID  string  `json:"polyappFirestoreID"` // For this document
	NextSchemaID *string // linked list pointing to the next schema version's ID in the database.
	Name         *string // Used when searching the collection. Typically identical to Task's Name.
	IndustryID   string  `json:"polyappIndustryID" firestore:"polyappIndustryID"` // FirestoreID
	DomainID     string  `json:"polyappDomainID" firestore:"polyappDomainID"`     // FirestoreID
	SchemaID     string  `json:"polyappSchemaID" firestore:"polyappSchemaID"`     // FirestoreID
	// Field Name -> Type. "Ref" "S" "I" "B" "F" are the non-array options. There's also "ARef", "AS", "AI", "AB", "AF". There's also "Bytes" for Bytes.
	DataTypes    map[string]string
	DataHelpText map[string]string // Field Name -> Help Text.
	DataKeys     []string          // List of Keys (Field Names) which are used in DataTypes. Used to find duplicate keys.
	// IsPublic is true if this is publicly accessible. It is false or not set otherwise.
	IsPublic   bool
	Deprecated *bool `json:"polyappDeprecated" firestore:"polyappDeprecated"`
}

// Validate return nil if the structure is valid.
func (s *Schema) Validate() error {
	if s == nil {
		return errors.New("Schema was nil")
	}
	if s.IndustryID == "" {
		return errors.New("Industry not fully populated")
	}
	if s.DomainID == "" {
		return errors.New("Domain not fully populated")
	}
	if s.SchemaID == "" {
		return errors.New("schema not fully populated")
	}
	if s.DataTypes == nil {
		return errors.New("DataTypes was not initialized")
	}
	if s.DataKeys == nil {
		return errors.New("DataKeys was not initialized")
	}
	if s.DataHelpText == nil {
		return errors.New("DataHelpText was not initialized")
	}
	if len(s.DataTypes) != len(s.DataHelpText) || len(s.DataKeys) != len(s.DataHelpText) {
		return errors.New("must have a Data Type, Help Text and Data Keys and they must all be the same length")
	}
	for k := range s.DataTypes {
		if strings.HasPrefix(RemoveFieldPrefix(k), "polyapp") {
			return errors.New("can't prefix a Schema data type with polyapp - it's a reserved prefix")
		}
		switch s.DataTypes[k] {
		case "Ref":
			continue
		case "S":
			continue
		case "I":
			continue
		case "B":
			continue
		case "F":
			continue
		case "Bytes":
			continue
		case "ARef":
			continue
		case "AS":
			continue
		case "AI":
			continue
		case "AB":
			continue
		case "AF":
			continue
		default:
			return errors.New("unrecognized value in Schema's DataTypes field: " + s.DataTypes[k] + " for key: " + k)
		}
	}
	return nil
}

// GetFirestoreID returns this firestore ID.
func (s *Schema) GetFirestoreID() string {
	return s.FirestoreID
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (s *Schema) CollectionName() string {
	return CollectionSchema
}

// Init translates a raw map into the Schema type. It does NOT set FirestoreID.
func (s *Schema) Init(simple map[string]interface{}) error {
	s.DataTypes = make(map[string]string)
	s.DataKeys = make([]string, 0)
	s.DataHelpText = make(map[string]string)
	Dereference(simple)
	sElements := reflect.ValueOf(s).Elem()
	err := populateType(sElements, simple)
	if err != nil {
		return err
	}
	return nil
}

// Merge merges a Schema 'toMerge' into the pointer receiver. It requires that they have the same FirestoreID.
func (s *Schema) Merge(toMerge *Schema) error {
	if toMerge.FirestoreID != s.FirestoreID {
		return errors.New("toMerge must have same FirestoreID as the pointer receiver on the method")
	}
	if toMerge.IndustryID != s.IndustryID || toMerge.SchemaID != s.SchemaID || toMerge.DomainID != s.DomainID {
		return errors.New("toMerge does not have the same industry, domain or schema")
	}
	if toMerge.DataTypes != nil {
		for k, v := range toMerge.DataTypes {
			s.DataTypes[k] = v
		}
		s.DataKeys = make([]string, len(s.DataTypes))
		i := 0
		for k := range s.DataTypes {
			s.DataKeys[i] = k
			i++
		}
	}
	if toMerge.NextSchemaID != nil {
		s.NextSchemaID = toMerge.NextSchemaID
	}
	if toMerge.Deprecated != nil {
		s.Deprecated = toMerge.Deprecated
	}
	if toMerge.IsPublic {
		s.IsPublic = true
	}
	if toMerge.Name != nil {
		s.Name = toMerge.Name
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (s *Schema) Simplify() (map[string]interface{}, error) {
	outMap := map[string]interface{}{
		PolyappIndustryID: s.IndustryID,
		PolyappDomainID:   s.DomainID,
		PolyappSchemaID:   s.SchemaID,
		"IsPublic":        s.IsPublic,
	}
	if s.Deprecated != nil {
		outMap[PolyappDeprecated] = *s.Deprecated
	}
	if s.NextSchemaID != nil {
		outMap["NextSchemaID"] = s.NextSchemaID
	}
	if s.DataTypes != nil {
		outMap["DataTypes"] = s.DataTypes
	}
	if s.DataKeys != nil {
		outMap["DataKeys"] = s.DataKeys
	}
	if s.DataHelpText != nil {
		outMap["DataHelpText"] = s.DataHelpText
	}
	if s.Name != nil {
		outMap["Name"] = s.Name
	}
	return outMap, nil
}

// Action is a name of a function or a chromedp.Action call. Actions are executed with RunBot(bot, data)
//
// Functions are of the format: somefuncname()
//
// chromedp.Action calls are of the format: chromedp.Action
//
// TODO implement the logic needed to get chromedp.Action calls to work.
// TODO every action a human can take should be doable by a chromedp.Action call, so why not
// have each action taken translated first into a chromedp.Action and stored in a big list of actions.
// Periodically updates are transmitted, and then that chromedp.Action list is replayed to apply the changes to the system
// before it's committed. If things were done this way it might be easier to sync the idbCRUD and firestoreCRUD databases.
// Doing things this way would also make regression testing really easy. Just replay the chromedp.Actions using a Bot &
// verify the Data at the end is the same.
type Action struct {
	FirestoreID string `json:"polyappFirestoreID"`
	// Name describes this Action
	Name string
	// HelpText explains why this action exists and what it does.
	HelpText   string
	Deprecated *bool `json:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (a *Action) Validate() error {
	if a.Name == "" || a.HelpText == "" || a.FirestoreID == "" {
		return errors.New("Name, HelpText, and FirestoreID must be set")
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (a *Action) Simplify() (map[string]interface{}, error) {
	simple := make(map[string]interface{})
	simple[PolyappFirestoreID] = a.FirestoreID
	simple["Name"] = a.Name
	simple["HelpText"] = a.HelpText
	if a.Deprecated != nil {
		simple[PolyappDeprecated] = a.Deprecated
	}
	return simple, nil
}

// GetFirestoreID returns the FirestoreID property.
func (a *Action) GetFirestoreID() string {
	return a.FirestoreID
}

// Init populates the Action data structure from the map.
func (a *Action) Init(simple map[string]interface{}) error {
	Dereference(simple)
	sElements := reflect.ValueOf(a).Elem()
	err := populateType(sElements, simple)
	if err != nil {
		return err
	}
	return nil
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (a *Action) CollectionName() string {
	return CollectionAction
}

// Bot is a software bot. It is an ordered list of Actions.
type Bot struct {
	FirestoreID string `json:"polyappFirestoreID"`
	Name        string
	HelpText    string `suffix:"Help Text"`
	// Actions are of type Action. This array contains an ordered list of IDs of Actions to execute.
	ActionIDs  []string `suffix:"Action IDs"`
	Deprecated *bool    `json:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (b *Bot) Validate() error {
	if b.Name == "" || b.HelpText == "" || b.FirestoreID == "" {
		return errors.New("Name, HelpText, and FirstoreID must be set")
	}
	if b.ActionIDs == nil {
		return errors.New("ActionIDs was not initialized")
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (b *Bot) Simplify() (map[string]interface{}, error) {
	simple := make(map[string]interface{})
	simple[PolyappFirestoreID] = b.FirestoreID
	simple["Name"] = b.Name
	simple["HelpText"] = b.HelpText
	if b.ActionIDs != nil {
		simple["ActionIDs"] = b.ActionIDs
	}
	if b.Deprecated != nil {
		simple[PolyappDeprecated] = b.Deprecated
	}
	return simple, nil
}

// GetFirestoreID returns the FirestoreID element.
func (b *Bot) GetFirestoreID() string {
	return b.FirestoreID
}

// Init the Bot with the simple map.
func (b *Bot) Init(simple map[string]interface{}) error {
	Dereference(simple)
	sElements := reflect.ValueOf(b).Elem()
	err := populateType(sElements, simple)
	if err != nil {
		return err
	}
	if b.ActionIDs == nil {
		b.ActionIDs = make([]string, 0)
	}
	return nil
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (b *Bot) CollectionName() string {
	return CollectionBot
}

// PublicMap is used as a lookup mechanism when trying to find the content of a web page. For instance, if you wanted
// to find the content of "example.com/pay" you would first query for that URL in the HostAndPath parameter. You would then
// take the first (and only) result and examine its UXID, TaskID, and other data and use that to construct a request
// in the form of private requests. You can then use that private request to access the rest of the code normally.
type PublicMap struct {
	FirestoreID string `json:"polyappFirestoreID"`
	// HostAndPath includes subdomains, like: "example.com" or "a.example.com", and probably looks like this: "example.com/pay/"
	// typical construction looks like: c.Request().Host + "/" + c.Request().URL.EscapedPath()
	//
	// The HostAndPath ALWAYS ends in a /
	HostAndPath string
	// IndustryID is the Industry associated with this HostAndPath.
	IndustryID string
	// DomainID is the domain associated with this HostAndPath.
	DomainID string
	// UXID is the UX associated with this HostAndPath, if any.
	UXID *string
	// TaskID is the Task associated with this HostAndPath, if any.
	TaskID *string
	// SchemaID is the Schema associated with this HostAndPath, if any.
	SchemaID *string
	// OwningUserID is a FirestoreID for whatever user owns this PublicMap. This is set when the PublicMap is created
	// and is used to verify that this page is being paid for (aka the subscription is still active).
	OwningUserID string
	Deprecated   *bool `json:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (p *PublicMap) Validate() error {
	if p.FirestoreID == "" {
		return errors.New("FirstoreID is required fro a PublicMap object")
	}
	if p.HostAndPath == "" {
		return errors.New("HostAndPath is required for a PublicMap object")
	}
	if p.HostAndPath[len(p.HostAndPath)-1] != '/' {
		p.HostAndPath = p.HostAndPath + "/"
	}
	if p.IndustryID == "" {
		return errors.New("IndustryID is required for a PublicMap object")
	}
	if p.DomainID == "" {
		return errors.New("DomainID is required for a PublicMap object")
	}
	if p.OwningUserID == "" {
		return errors.New("OwningUserID is required for a PublicMap object (or else it would never be possible to access this page).")
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (p *PublicMap) Simplify() (map[string]interface{}, error) {
	simple := make(map[string]interface{})
	simple[PolyappFirestoreID] = p.FirestoreID
	simple["HostAndPath"] = p.HostAndPath
	simple["IndustryID"] = p.IndustryID
	simple["DomainID"] = p.DomainID
	simple["OwningUserID"] = p.OwningUserID
	if p.SchemaID != nil {
		simple["SchemaID"] = *p.SchemaID
	}
	if p.UXID != nil {
		simple["UXID"] = *p.UXID
	}
	if p.TaskID != nil {
		simple["TaskID"] = *p.TaskID
	}
	if p.Deprecated != nil {
		simple[PolyappDeprecated] = *p.Deprecated
	}
	return simple, nil
}

// GetFirestoreID returns the FirestoreID property.
func (p *PublicMap) GetFirestoreID() string {
	return p.FirestoreID
}

// Init the PublicMap with the simple map.
func (p *PublicMap) Init(simple map[string]interface{}) error {
	Dereference(simple)
	sElements := reflect.ValueOf(p).Elem()
	err := populateType(sElements, simple)
	if err != nil {
		return err
	}
	return nil
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (p *PublicMap) CollectionName() string {
	return CollectionPublicMap
}

// CSS contains CSS which is unique to certain pages or websites. This is intended to be used to 'theme' public websites.
type CSS struct {
	FirestoreID string `json:"polyappFirestoreID"`
	// Tags are search terms used to identify CSS. It could be the name of the CSS, describe what it looks like, or categorize it somehow.
	// The first Tag is always the "Name" of the CSS.
	Tags []string
	// CompiledCSS is the output from the compilation process. It is served directly to end users.
	CompiledCSS *string
	Deprecated  *bool `json:"polyappDeprecated"`
}

// Validate returns an error for improper data.
func (c *CSS) Validate() error {
	if c.FirestoreID == "" {
		return errors.New("FirstoreID is required fro a CSS object")
	}
	return nil
}

// Simplify into a map[string]interface{}. This is the opposite of Init().
func (c *CSS) Simplify() (map[string]interface{}, error) {
	simple := make(map[string]interface{})
	simple[PolyappFirestoreID] = c.FirestoreID
	if c.Tags != nil {
		simple["Tags"] = c.Tags
	}
	if c.CompiledCSS != nil {
		simple["CompiledCSS"] = *c.CompiledCSS
	}
	if c.Deprecated != nil {
		simple[PolyappDeprecated] = *c.Deprecated
	}
	return simple, nil
}

// GetFirestoreID returns the FirestoreID property.
func (c *CSS) GetFirestoreID() string {
	return c.FirestoreID
}

// Init CSS with the simple map.
func (c *CSS) Init(simple map[string]interface{}) error {
	Dereference(simple)
	sElements := reflect.ValueOf(c).Elem()
	err := populateType(sElements, simple)
	if err != nil {
		return err
	}
	return nil
}

// CollectionName is the literal name of this collection. This is used by generic functions.
func (c *CSS) CollectionName() string {
	return CollectionCSS
}

// DataSatisfiesSchema if and only if Data's fields are a subset of or == the Schema's fields.
func DataSatisfiesSchema(d *Data, s *Schema) error {
	if d == nil || s == nil {
		return errors.New("DataTypes or Schema was nil")
	}
	err := d.Validate()
	if err != nil {
		return fmt.Errorf("validating data failed: %w", err)
	}
	err = s.Validate()
	if err != nil {
		return fmt.Errorf("validating schema failed: %w", err)
	}
	if d.SchemaID != s.SchemaID && d.SchemaID != s.FirestoreID {
		// FYI this used to also require d.IndustryID == s.IndustryID and the same for domain.
		// I removed this, which means you can now have cross-Industry & cross-Domain tasks as long as the path is the true
		// Industry/Domain and the URL contains industry and domain overrides which give context to the Task.
		// This setup was chosen to allow polyapp/Create Task to be used as a generic Task which worked outside of polyapp/Task domain.
		return fmt.Errorf("Schema (%v) and Data (%v) are not using the same SchemaID. Schema: %v ; Data: %v", s.FirestoreID, d.FirestoreID, s.SchemaID, d.SchemaID)
	}
	for k := range d.F {
		t, ok := s.DataTypes[k]
		if !ok || t != "F" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a Float field at key: " + k)
		}
	}
	for k := range d.B {
		t, ok := s.DataTypes[k]
		if !ok || t != "B" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a Bool field at key: " + k)
		}
	}
	for k := range d.I {
		t, ok := s.DataTypes[k]
		if !ok || t != "I" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked an Int field at key: " + k)
		}
	}
	for k := range d.S {
		t, ok := s.DataTypes[k]
		if !ok || t != "S" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a String field at key: " + k)
		}
	}
	for k := range d.Ref {
		t, ok := s.DataTypes["_"+k]
		if !ok || t != "Ref" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a Ref field at key: " + k)
		}
	}
	for k := range d.Bytes {
		t, ok := s.DataTypes[k]
		if !ok || t != "Bytes" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a Bytes field at key: " + k)
		}
	}
	for k := range d.AS {
		t, ok := s.DataTypes[k]
		if !ok || t != "AS" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a AS field at key: " + k)
		}
	}
	for k := range d.AI {
		t, ok := s.DataTypes[k]
		if !ok || t != "AI" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a AI field at key: " + k)
		}
	}
	for k := range d.AF {
		t, ok := s.DataTypes[k]
		if !ok || t != "AF" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a AF field at key: " + k)
		}
	}
	for k := range d.AB {
		t, ok := s.DataTypes[k]
		if !ok || t != "AB" {
			return errors.New("Schema should contain every field in Data (" + d.FirestoreID + ") but this Schema (" + s.FirestoreID + ") lacked a AB field at key: " + k)
		}
	}
	// note: d.ARef and d.Nils are both absent on purpose. d.ARef is messy right now and can include cross-industry, domain and schema keys.
	// d.Nils only sometimes includes some keys and I'm not going to put in the effort to verifying its integrity right now.
	return nil
}

// SchemaSatisfiesTask ensures all of a Task's inputs are available from a particular Schema.
func SchemaSatisfiesTask(s *Schema, t *Task) error {
	if t == nil || s == nil {
		return errors.New("DataTypes or Schema was nil")
	}
	if t.DomainID != s.DomainID || t.IndustryID != s.IndustryID {
		return errors.New("Task and Data are not using the same IndustryID, DomainID or SchemaID")
	}
	err := t.Validate()
	if err != nil {
		return fmt.Errorf("validating data failed: %w", err)
	}
	err = s.Validate()
	if err != nil {
		return fmt.Errorf("validating schema failed: %w", err)
	}
	for k := range t.TaskGoals {
		tg, err := t.taskGoalGet(k)
		if err != nil {
			return fmt.Errorf("taskGoalGet: %w", err)
		}
		if strings.HasPrefix(k, "polyappError") {
			_, ok := s.DataTypes[strings.TrimPrefix(k, "polyappError")]
			if !ok {
				return errors.New("field error key: " + k + " is associated with did not appear in schema so " +
					"if an error were thrown there would be no place in the UI to display it to users")
			}
		}

		_, ok := s.DataTypes[tg.Op1Variable]
		if !ok {
			// the goal will never be satisfied if the schema lacks an input variable
			return errors.New("goal with name: " + k + " input parameter 1 not in schema: " + tg.Op1Variable)
		}
		if tg.Op2Variable != "" {
			_, ok := s.DataTypes[tg.Op2Variable]
			if !ok {
				// the goal will never be satisfied if the schema lacks an input variable
				return errors.New("goal with name: " + k + " input parameter 2 not in schema: " + tg.Op2Variable)
			}
		}
	}
	return nil
}

// SchemaSatisfiesUX ensures a Schema and UX are compatible
// We *could* go through every HTML ID in UX.HTML and ensure it has something in Schema & pull additional Schemas recursively.
// But we do not because that would be a lot of work to verify that the "SchemaID" variables are not lying.
func SchemaSatisfiesUX(s *Schema, ux *UX) error {
	if ux == nil || s == nil {
		return errors.New("DataTypes or Schema was nil")
	}
	if ux.SchemaID != s.SchemaID || ux.DomainID != s.DomainID || ux.IndustryID != s.IndustryID {
		return errors.New("Schema and UX are not using the same IndustryID, DomainID or SchemaID")
	}
	err := ux.Validate()
	if err != nil {
		return fmt.Errorf("validating data failed: %w", err)
	}
	err = s.Validate()
	if err != nil {
		return fmt.Errorf("validating schema failed: %w", err)
	}
	return nil
}

// populateType populates the struct type at 'elements' with information from map 'simple'.
func populateType(elements reflect.Value, simple map[string]interface{}) error {
	for i := 0; i < elements.NumField(); i++ {
		field := elements.Type().Field(i)
		simpleKey := field.Name
		tag := field.Tag.Get("json")
		if tag != "" {
			simpleKey = tag
		}
		_, ok := simple[simpleKey]
		if !ok {
			continue
		}

		simpleValue := reflect.ValueOf(simple[simpleKey])
		simpleValueType := reflect.TypeOf(simple[simpleKey])
		if simpleValueType == nil {
			continue
		}
		// to prevent an unexpected error, let's do some type checking
		if field.Type.Kind() == simpleValueType.Kind() {
			// this is expected
			if field.Type.Kind() == reflect.Array || field.Type.Kind() == reflect.Slice {
				// we also need to type check values in the array
				// If the type in the array is already concrete & not interface we're doing a lot of unnecessary work here.
				switch field.Type.Elem().Kind() {
				case reflect.String:
					convertedToSlice := make([]string, simpleValue.Len())
					for i := 0; i < simpleValue.Len(); i++ {
						convertedToSlice[i] = simpleValue.Index(i).Interface().(string)
					}
					elements.Field(i).Set(reflect.ValueOf(convertedToSlice))
				case reflect.Float64:
					convertedToSlice := make([]float64, simpleValue.Len())
					for i := 0; i < simpleValue.Len(); i++ {
						convertedToSlice[i] = simpleValue.Index(i).Interface().(float64)
					}
					elements.Field(i).Set(reflect.ValueOf(convertedToSlice))
				case reflect.Int64:
					convertedToSlice := make([]int64, simpleValue.Len())
					for i := 0; i < simpleValue.Len(); i++ {
						convertedToSlice[i] = simpleValue.Index(i).Interface().(int64)
					}
					elements.Field(i).Set(reflect.ValueOf(convertedToSlice))
				case reflect.Bool:
					convertedToSlice := make([]bool, simpleValue.Len())
					for i := 0; i < simpleValue.Len(); i++ {
						convertedToSlice[i] = simpleValue.Index(i).Interface().(bool)
					}
					elements.Field(i).Set(reflect.ValueOf(convertedToSlice))
				default:
					// no idea what happened here
					return errors.New("unhandled array type")
				}
			} else if field.Type.Kind() == reflect.Map {
				// TODO this code path isn't very well tested and may be benefiting from the fact that as of this writing
				// this function will always be called with map types which are map[string]string
				switch field.Type.Elem().Kind() {
				case reflect.String:
					convertedToMap := make(map[string]string)
					simpleMapStringInt, ok := simple[simpleKey].(map[string]interface{})
					if !ok {
						convertedToMap = simple[simpleKey].(map[string]string)
					} else {
						for k, v := range simpleMapStringInt {
							convertedToMap[k] = v.(string)
						}
					}
					elements.Field(i).Set(reflect.ValueOf(convertedToMap))
				}
			} else {
				elements.Field(i).Set(simpleValue)
			}
		} else if field.Type.Kind() == reflect.PtrTo(simpleValueType).Kind() {
			switch v := simple[simpleKey].(type) {
			case string:
				elements.Field(i).Set(reflect.ValueOf(String(v)))
			case bool:
				elements.Field(i).Set(reflect.ValueOf(Bool(v)))
			case int:
				elements.Field(i).Set(reflect.ValueOf(Int(v)))
			case int64:
				elements.Field(i).Set(reflect.ValueOf(Int64(v)))
			case float64:
				elements.Field(i).Set(reflect.ValueOf(Float64(v)))
			}
		}
	}
	return nil
}

// taskGoal contains the format for a taskGoal.
type taskGoal struct {
	Name        string
	HelpText    string
	Operation   string // one of "==" "!=" "<" ">" "<=", ">="
	Op1Variable string
	Op2Variable string
	Op2Constant interface{}
	// ErrorMessage is User-readable and must be understandable.
	ErrorMessage string
}

// Validate returns an error for improper data.
func (t *taskGoal) Validate() error {
	if t.Name == "" {
		return errors.New("Name was empty")
	}
	switch t.Operation {
	case "==":
	case "!=":
	case "<":
	case ">":
	case "<=":
	case ">=":
	case "len>0":
	default:
		return errors.New("Operation invalid")
	}
	if t.Op1Variable == "" {
		return errors.New("Op1Variable is required")
	}
	if t.ErrorMessage == "" {
		return errors.New("ErrorMessage is required")
	}
	if t.Op2Constant == nil && t.Op2Variable == "" {
		return errors.New("either no Op2Constant or no Op2Variable")
	}
	if t.Op2Constant != nil && t.Op2Variable != "" {
		return errors.New("both Op2Constant and Op2Variable cannot be initialized")
	}
	if t.Op2Constant != nil {
		switch t.Op2Constant.(type) {
		case string:
			if t.Operation != "==" && t.Operation != "!=" {
				return errors.New("sting comparisons must be == or !=")
			}
		case int64:
		case float64:
		case bool:
			if t.Operation != "==" && t.Operation != "!=" {
				return errors.New("can only compare booleans with == or !=")
			}
		case []interface{}:
			if t.Operation != "len>0" {
				return errors.New("can only compare arrays with len>0")
			}
		default:
			return errors.New("unsupported type for Op2Constant")
		}
	}
	return nil
}

// taskGoalGet gets a task goal from the TaskGoals map.
func (t *Task) taskGoalGet(key string) (taskGoal, error) {
	tg, ok := t.TaskGoals[key]
	if !ok {
		return taskGoal{}, status.Error(codes.NotFound, "TaskGoal with key not found: "+key)
	}
	var taskGoalOut taskGoal
	err := json.Unmarshal([]byte(tg), &taskGoalOut)
	if err != nil {
		return taskGoal{}, fmt.Errorf("could not unmarshal task goal: %w", err)
	}
	return taskGoalOut, nil
}

// TaskGoalAdd adds a Task Goal to the TaskGoals map. Include op2Variable XOR op2Constant.
//
// Inputs:
//
// associatedField: the name of a field an error should be shown underneath. Typically associatedField == op1Variable || associatedField == op2Variable.
// If associatedField is not set, key = name. If set, key = "polyappError" + name
//
// name: the user-readable (and human-searchable) name of this validator.
//
// helpText: a user-readable description of why this validator exists, etc.
//
// op1Variable: if [op1Variable] [operation] [op2Variable] { return "" } else { return errorMessage }
//
// operation: if [op1Variable] [operation] [op2Variable] { return "" } else { return errorMessage }
//
// op2Variable: if [op1Variable] [operation] [op2Variable] { return "" } else { return errorMessage }
//
// op2Constant: if [op1Variable] [operation] [op2Constant] { return "" } else { return errorMessage }
//
// errorMessage: if [op1Variable] [operation] [op2Variable] { return "" } else { return errorMessage }
func (t *Task) TaskGoalAdd(associatedField string, name string, helpText string, op1Variable string, operation string, op2Variable string, op2Constant interface{}, errorMessage string) error {
	key := name
	if associatedField != "" {
		key = "polyappError" + associatedField
	}
	tg := taskGoal{
		Name:         name,
		HelpText:     helpText,
		Operation:    operation,
		Op1Variable:  op1Variable,
		Op2Variable:  op2Variable,
		Op2Constant:  op2Constant,
		ErrorMessage: errorMessage,
	}
	err := tg.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	encoded, err := json.Marshal(tg)
	if err != nil {
		return fmt.Errorf("json.Marshal: %w", err)
	}
	t.TaskGoals[key] = string(encoded)
	return nil
}

func evalString(op1 string, operation string, op2 string, errorMessage string) (string, error) {
	switch operation {
	case "==":
		if op1 == op2 {
			return "", nil
		}
		return errorMessage, nil
	case "!=":
		if op1 != op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">":
		if op1 > op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<":
		if op1 < op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">=":
		if op1 >= op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<=":
		if op1 <= op2 {
			return "", nil
		}
		return errorMessage, nil
	}
	return "", errors.New("unmatched operation in string - should never happen - this is a developer error")
}

func evalInt(op1 int64, operation string, op2 int64, errorMessage string) (string, error) {
	switch operation {
	case "==":
		if op1 == op2 {
			return "", nil
		}
		return errorMessage, nil
	case "!=":
		if op1 != op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">":
		if op1 > op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<":
		if op1 < op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">=":
		if op1 >= op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<=":
		if op1 <= op2 {
			return "", nil
		}
		return errorMessage, nil
	}
	return "", errors.New("unmatched operation in int - should never happen - this is a developer error")

}

func evalFloat(op1 float64, operation string, op2 float64, errorMessage string) (string, error) {
	switch operation {
	case "==":
		if op1 == op2 {
			return "", nil
		}
		return errorMessage, nil
	case "!=":
		if op1 != op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">":
		if op1 > op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<":
		if op1 < op2 {
			return "", nil
		}
		return errorMessage, nil
	case ">=":
		if op1 >= op2 {
			return "", nil
		}
		return errorMessage, nil
	case "<=":
		if op1 <= op2 {
			return "", nil
		}
		return errorMessage, nil
	}
	return "", errors.New("unmatched operation in float - should never happen - this is a developer error")
}

func evalBool(op1 bool, operation string, op2 bool, errorMessage string) (string, error) {
	switch operation {
	case "==":
		if op1 == op2 {
			return "", nil
		}
		return errorMessage, nil
	case "!=":
		if op1 != op2 {
			return "", nil
		}
		return errorMessage, nil
	}
	return "", errors.New("unmatched operation in bool - should never happen - this is a developer error")
}

func evalArray(op1 []interface{}, operation string, op2 []interface{}, errorMessage string) (string, error) {
	switch operation {
	case "len>0":
		if len(op1) > 0 {
			return "", nil
		}
		return errorMessage, nil
	}
	return "", errors.New("unmatched operation in []interface{} - should never happen - this is a developer error")
}

var (
	ErrTaskGoalEvaluateNotHardCoded = errors.New("Task Goal Evaluate Not Hard Coded")
)

func taskGoalEvaluateHardCoded(t taskGoal, key string, task *Task, data *Data) (errorMessage string, err error) {
	if data.S[t.Op1Variable] == nil || *data.S[t.Op1Variable] == "" {
		return "", nil
	}
	switch t.Name {
	case "Email":
		emailRegex := regexp.MustCompile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
		v, ok := data.S[t.Op1Variable]
		if !ok {
			// we are OK with the field being empty
			return "", nil
		}
		matchString := emailRegex.FindString(*v)
		if matchString == "" {
			return t.ErrorMessage, nil
		}
		return "", nil
	case "URL":
		urlRegex := regexp.MustCompile("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")
		v, ok := data.S[t.Op1Variable]
		if !ok {
			// we are OK with the field being empty
			return "", nil
		}
		matchString := urlRegex.FindString(*v)
		if matchString == "" {
			return t.ErrorMessage, nil
		}
		return "", nil
	case "Phone Number":
		// https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number
		phoneNumberRegex := regexp.MustCompile("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$")
		v, ok := data.S[t.Op1Variable]
		if !ok {
			// we are OK with the field being empty
			return "", nil
		}
		matchString := phoneNumberRegex.FindString(*v)
		if matchString == "" {
			return t.ErrorMessage, nil
		}
		return "", nil
	case "Alphanumeric Characters Only":
		s, ok := data.S[t.Op1Variable]
		if ok {
			if RemoveNonAlphaNumeric(*s) != *s {
				return t.ErrorMessage, nil
			}
			return "", nil
		}
		// float, bool, int all already kind of meet this requirement OR it should be ignored for them
		as, ok := data.AS[t.Op1Variable]
		if ok {
			for _, s := range as {
				if RemoveNonAlphaNumeric(s) != s {
					return t.ErrorMessage, nil
				}
			}
			return "", nil
		}
	case "Alphanumeric Unicode Characters Only":
		// TODO
	case "Alpha Characters Only":
		// TODO
	case "Alpha Unicode Characters Only":
		// TODO
	case "Numeric Only":
		// TODO
	case "Numeric Unicode Only":
		// TODO
	case "Integer Only":
		// TODO
	}
	return "", ErrTaskGoalEvaluateNotHardCoded
}

// TaskGoalEvaluate returns errorMessage "" if the TaskGoal passes validation, an errorMessage string, or an error in failures.
func TaskGoalEvaluate(key string, task *Task, data *Data) (errorMessage string, err error) {
	if key == "" || task == nil || data == nil {
		return "", errors.New("name, task or data were not provided")
	}
	taskGoalMarshalled, ok := task.TaskGoals[key]
	if !ok {
		return "", errors.New("task goal not found with key: " + key)
	}
	var t taskGoal
	err = json.Unmarshal([]byte(taskGoalMarshalled), &t)
	if err != nil {
		return "", fmt.Errorf("unmarshal error: %w", err)
	}
	errorMessage, err = taskGoalEvaluateHardCoded(t, key, task, data)
	if err != ErrTaskGoalEvaluateNotHardCoded {
		return errorMessage, err
	}

	err = t.Validate()
	if err != nil {
		return "", fmt.Errorf("validating taskGoal: %w", err)
	}
	if t.Op2Constant != nil {
		// This is great because both operands are easily typed
		switch op2 := t.Op2Constant.(type) {
		case string:
			op1, ok := data.S[t.Op1Variable]
			if !ok {
				return "", errors.New("string not found for Op1Variable: " + t.Op1Variable)
			}
			return evalString(*op1, t.Operation, op2, t.ErrorMessage)
		case int64:
			op1, ok := data.I[t.Op1Variable]
			if !ok {
				return "", errors.New("int not found for Op1Variable: " + t.Op1Variable)
			}
			return evalInt(*op1, t.Operation, op2, t.ErrorMessage)
		case float64:
			op1, ok := data.F[t.Op1Variable]
			if ok {
				return evalFloat(*op1, t.Operation, op2, t.ErrorMessage)
			}
			// An alternative to this logic is to rectify the Data into the Schema before calling this code.
			op1Int, ok := data.I[t.Op1Variable]
			if !ok {
				// this can happen for time controls. I do not want there to be a special case for time controls. TODO
				return t.ErrorMessage, nil
			}
			return evalInt(*op1Int, t.Operation, int64(op2), t.ErrorMessage)
		case bool:
			op1, ok := data.B[t.Op1Variable]
			if !ok {
				return "", errors.New("bool not found for Op1Variable: " + t.Op1Variable)
			}
			return evalBool(*op1, t.Operation, op2, t.ErrorMessage)
		case []interface{}:
			op1, ok := data.AS[t.Op1Variable]
			if ok {
				return evalArray(ASToAInterface(op1), "len>0", op2, t.ErrorMessage)
			}
			op12, ok := data.AB[t.Op1Variable]
			if ok {
				return evalArray(ABToAInterface(op12), "len>0", op2, t.ErrorMessage)
			}
			op13, ok := data.AF[t.Op1Variable]
			if ok {
				return evalArray(AFToAInterface(op13), "len>0", op2, t.ErrorMessage)
			}
			op14, ok := data.AI[t.Op1Variable]
			if ok {
				return evalArray(AIToAInterface(op14), "len>0", op2, t.ErrorMessage)
			}
			op15, ok := data.Bytes[t.Op1Variable]
			if ok {
				return evalArray(BytesToAInterface(op15), "len>0", op2, t.ErrorMessage)
			}
			op16, ok := data.ABytes[t.Op1Variable]
			if ok {
				return evalArray(ABytesToAInterface(op16), "len>0", op2, t.ErrorMessage)
			}
			return "", errors.New("Array not found for Op1Variable: " + t.Op1Variable)
		}

	} else {
		op1String, ok := data.S[t.Op1Variable]
		if ok {
			op2, ok := data.S[t.Op2Variable]
			if !ok {
				return "", errors.New("string not found for Op2Variable: " + t.Op2Variable)
			}
			return evalString(*op1String, t.Operation, *op2, t.ErrorMessage)
		}
		op1Float, ok := data.F[t.Op1Variable]
		if ok {
			op2, ok := data.F[t.Op2Variable]
			if !ok {
				return "", errors.New("float not found for Op2Variable: " + t.Op2Variable)
			}
			return evalFloat(*op1Float, t.Operation, *op2, t.ErrorMessage)
		}
		op1Int, ok := data.I[t.Op1Variable]
		if ok {
			op2, ok := data.I[t.Op2Variable]
			if !ok {
				return "", errors.New("int not found for Op2Variable: " + t.Op2Variable)
			}
			return evalInt(*op1Int, t.Operation, *op2, t.ErrorMessage)
		}
		op1Bool, ok := data.B[t.Op1Variable]
		if ok {
			op2, ok := data.B[t.Op2Variable]
			if !ok {
				return "", errors.New("bool not found for Op2Variable: " + t.Op2Variable)
			}
			return evalBool(*op1Bool, t.Operation, *op2, t.ErrorMessage)
		}
		return "", errors.New("t.Op1Variable was not found in Data's String, Int, Bool, or Float maps: " + t.Op1Variable)
	}
	return t.ErrorMessage, nil
}

func ASToAInterface(AS []string) []interface{} {
	o := make([]interface{}, len(AS))
	for i := range AS {
		o[i] = AS[i]
	}
	return o
}
func ABToAInterface(A []bool) []interface{} {
	o := make([]interface{}, len(A))
	for i := range A {
		o[i] = A[i]
	}
	return o
}
func AIToAInterface(A []int64) []interface{} {
	o := make([]interface{}, len(A))
	for i := range A {
		o[i] = A[i]
	}
	return o
}
func AFToAInterface(A []float64) []interface{} {
	o := make([]interface{}, len(A))
	for i := range A {
		o[i] = A[i]
	}
	return o
}
func ABytesToAInterface(A [][]byte) []interface{} {
	o := make([]interface{}, len(A))
	for i := range A {
		o[i] = A[i]
	}
	return o
}
func BytesToAInterface(A []byte) []interface{} {
	o := make([]interface{}, len(A))
	for i := range A {
		o[i] = A[i]
	}
	return o
}

// ValidateRef ensures something which ought to be a Ref type follows the format of a Ref type.
func ValidateRef(ref string) error {
	URL, err := url.ParseRequestURI(ref)
	if err != nil {
		return err
	}
	request, err := CreateGETRequest(*URL)
	if err != nil {
		return err
	}
	if request.IndustryID == "polyappNone" {
		return errors.New("Ref did not include IndustryID")
	}
	if request.DomainID == "polyappNone" {
		return errors.New("Ref did not include DomainID")
	}
	if request.TaskID == "polayppNone" {
		return errors.New("Ref did not include Task ID")
	}
	if request.SchemaID == "polayppNone" {
		return errors.New("Ref did not include Schema ID")
	}
	if request.DataID == "polayppNone" {
		return errors.New("Ref did not include DataID")
	}
	if request.UXID == "polyappNone" {
		return errors.New("Ref did not include UXID")
	}
	return nil
}

// ConvertDataToMatchSchema finds keys which should be Ints which are currently floats and
// converts them and finds key which should be Floats which are currently Ints and converts them.
//
// To determine the proper data type, schema is examined.
//
// This should really be called whenever you are retrieving Data from the database but under some circumstances you
// don't have access to the Schema for a Data so you won't be able to call this. When this is called, you should also
// call RemoveExtraFields.
func ConvertDataToMatchSchema(data *Data, schema *Schema) error {
	if data == nil {
		return errors.New("data was nil")
	}
	if schema == nil {
		return errors.New("schema is required; try using data.SchemaCache")
	}
	for k, v := range data.F {
		if v != nil && schema.DataTypes[k] == "I" {
			s := fmt.Sprintf("%.0f", *v)
			if i, err := strconv.Atoi(s); err == nil {
				data.I[k] = Int64(int64(i))
				delete(data.F, k)
			} else {
				// error?
			}
		}
	}
	for k, v := range data.I {
		if v != nil && schema.DataTypes[k] == "F" {
			data.F[k] = Float64(float64(*v))
			delete(data.I, k)
		}
	}
	for k, v := range data.AS {
		if v != nil && len(v) == 0 {
			// this can be a problem if the array starts out with a length w/ numbers and then you delete all of them
			// the resulting empty array is untyped in the JSON format so the json-> interface{} converter makes it an []string.
			if schema.DataTypes[k] == "AI" {
				data.AI[k] = []int64{}
				delete(data.AS, k)
			} else if schema.DataTypes[k] == "AF" {
				data.AF[k] = []float64{}
				delete(data.AS, k)
			}
		}
	}
	for k, v := range data.AF {
		if v != nil && schema.DataTypes[k] == "AI" {
			iArray := make([]int64, len(v))
			for i := range v {
				s := fmt.Sprintf("%v", v[i])
				if iVal, err := strconv.Atoi(s); err == nil {
					iArray[i] = int64(iVal)
				} else {
					// error?
				}
			}
			data.AI[k] = iArray
			delete(data.AF, k)
		}
	}
	for k, v := range data.AI {
		if v != nil && schema.DataTypes[k] == "AF" {
			fArray := make([]float64, len(v))
			for i := range v {
				fArray[i] = float64(v[i])
			}
			data.AF[k] = fArray
			delete(data.AI, k)
		}
	}
	return nil
}

// RemoveExtraFields from Data. "Extra Fields" are fields which are present in Data but not present in the
// Schema of this Data. Extra fields may be introduced when a field is removed from a Task.
//
// This function should always be called after func ConvertDataToMatchSchema or else some data which could have been
// rectified may be deleted.
func RemoveExtraFields(data *Data, schema *Schema) error {
	if data == nil {
		return errors.New("data was nil")
	}
	if schema == nil {
		return errors.New("schema is required; try using data.SchemaCache")
	}
	for k := range data.S {
		if schema.DataTypes[k] != "S" {
			delete(data.S, k)
		}
	}
	for k := range data.B {
		if schema.DataTypes[k] != "B" {
			delete(data.B, k)
		}
	}
	for k := range data.F {
		if schema.DataTypes[k] != "F" {
			delete(data.F, k)
		}
	}
	for k := range data.I {
		if schema.DataTypes[k] != "I" {
			delete(data.I, k)
		}
	}
	for k := range data.Bytes {
		if schema.DataTypes[k] != "Bytes" {
			delete(data.Bytes, k)
		}
	}
	for k := range data.Ref {
		if schema.DataTypes["_"+k] != "Ref" {
			delete(data.Ref, k)
		}
	}
	for k := range data.ARef {
		if schema.DataTypes["_"+k] != "ARef" {
			delete(data.ARef, k)
		}
	}
	for k := range data.AS {
		if schema.DataTypes[k] != "AS" {
			delete(data.AS, k)
		}
	}
	for k := range data.AB {
		if schema.DataTypes[k] != "AB" {
			delete(data.AB, k)
		}
	}
	for k := range data.AF {
		if schema.DataTypes[k] != "AF" {
			delete(data.AF, k)
		}
	}
	for k := range data.AI {
		if schema.DataTypes[k] != "AI" {
			delete(data.AI, k)
		}
	}
	for k := range data.ABytes {
		if schema.DataTypes[k] != "ABytes" {
			delete(data.ABytes, k)
		}
	}
	return nil
}
