//+build js

package common

import (
	"syscall/js"
)

// ValueToMap converts a js.Value to a map.
// This function was manually tested.
func ValueToMap(value js.Value) map[string]interface{} {
	globalObject := js.Global().Get("Object")
	outMap := make(map[string]interface{})
	keys := globalObject.Call("keys", value)
	for i := 0; i < keys.Length(); i++ {
		k := keys.Index(i).String()
		jsv := value.Get(k)
		switch jsv.Type() {
		case js.TypeObject:
			outMap[k] = objectToMapOrArray(jsv)
		case js.TypeString:
			outMap[k] = jsv.String()
		case js.TypeNumber:
			// lean towards using floating point numbers.
			// We can't make a call on if this is correct or not until we see the Schema.
			// TODO when comparing Data and Schema, rectify Data which is of type Float but should be type Int & throw error if float64(int64(floatValue)) != floatValue
			outMap[k] = jsv.Float()
		case js.TypeBoolean:
			outMap[k] = jsv.Bool()
		case js.TypeNull:
			outMap[k] = nil
		default:
			js.Global().Get("console").Call("log", "other type: "+jsv.Type().String())
		}
	}
	return outMap
}

// objectToMapOrArray handles TypeObject in ValueToMap. The main difference with ValueToMap is that this does not call
// the keys API.
// This function was manually tested.
func objectToMapOrArray(jsv js.Value) interface{} {
	if jsv.Length() == 0 {
		// a bit of testing in a JS console:
		// b = {0: "one", 1: "two"}
		// b.length        which returned: undefined
		// so if jsv.Length() == 0 then it could be a 0 length array & if undefined then it's an object.
		// However, since js.go docs for: type ref uint64
		// say that 'undefined' is encoded as a 0, we really have no way to know if this is a map or array.
		// Therefore callers of this function will have to figure out based on some other information what type this is.
		// A caller might detect this problem like this: if v, ok := outMap[k]; ok && v == nil { // the value exists and is nil }
		return nil
	} else if jsv.Length() > 0 {
		// this is an array since an Object would return 'undefined' for a jsv.Length() call.
		out := make([]interface{}, jsv.Length())

		for arrayI := 0; arrayI < jsv.Length(); arrayI++ {
			arrayVal := jsv.Index(arrayI)
			switch arrayVal.Type() {
			case js.TypeObject:
				out[arrayI] = objectToMapOrArray(arrayVal)
			case js.TypeString:
				out[arrayI] = arrayVal.String()
			case js.TypeNumber:
				// lean towards using floating point numbers.
				// We can't make a call on if this is correct or not until we see the Schema.
				out[arrayI] = arrayVal.Float()
			case js.TypeBoolean:
				out[arrayI] = arrayVal.Bool()
			case js.TypeNull:
				out[arrayI] = nil
			}
		}
		return out
	} else {
		// probably a Map
		return ValueToMap(jsv)
	}
}

// ConvertPointers converts *bool & similar reference variables to nil pointers or dereferences them.
// Use this to prevent panics in JS when trying to convert *bool and similar types to JS types.
// The map MUST be initialized before calling this function (that's how Go works).
func ConvertPointers(m map[string]interface{}) {
	for k, v := range m {
		switch typedValue := v.(type) {
		case *bool:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *uint:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *uint8:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *uint16:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *uint32:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *uint64:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *int:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *int8:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *int16:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *int32:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *int64:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *float32:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *float64:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *complex64:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *complex128:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case *string:
			if typedValue == nil {
				m[k] = nil
			} else {
				m[k] = *typedValue
			}
		case nil:
			// not necessary and for demonstrative purposes only
			m[k] = nil
		}
	}
}
