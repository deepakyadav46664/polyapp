package actions

import (
	"reflect"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestDataIntoStructures(t *testing.T) {
	type emptyStructure struct{}
	type simpleStructure struct {
		St string
		Bo bool
		In int64   `suffix:"Integer"`
		Fl float64 `suffix:"Float"`
	}
	type arraysStructure struct {
		St []string
		Bo []bool `suffix:"Bool"`
		In []int64
		Fl []float64 `suffix:"Float"`
		B  []byte    `suffix:"BytesHere"`
		AB [][]byte
	}
	type args struct {
		data           *common.Data
		ptrToStructure interface{}
	}
	tests := []struct {
		name          string
		args          args
		wantStructure interface{}
		wantErr       bool
	}{
		{
			name: "empty structure nil data",
			args: args{
				data:           nil,
				ptrToStructure: &emptyStructure{},
			},
			wantStructure: &emptyStructure{},
			wantErr:       false,
		},
		{
			name: "simple structure but no data",
			args: args{
				data:           &common.Data{},
				ptrToStructure: &simpleStructure{},
			},
			wantStructure: &simpleStructure{},
			wantErr:       false,
		},
		{
			name: "data larger than structure",
			args: args{
				data: &common.Data{
					Ref: nil,
					S: map[string]*string{
						"_Ignored": common.String("ignored"),
						"_St":      common.String("string value"),
					},
					I: map[string]*int64{
						"_In":      common.Int64(33),
						"_Integer": common.Int64(55),
					},
					B: map[string]*bool{
						"_Bo": common.Bool(true),
					},
					F: map[string]*float64{
						"_Float": common.Float64(33.3),
						"_Fl":    common.Float64(55.5),
					},
					ARef:   nil,
					AS:     nil,
					AI:     nil,
					AB:     nil,
					AF:     nil,
					Bytes:  nil,
					ABytes: nil,
				},
				ptrToStructure: &simpleStructure{},
			},
			wantStructure: &simpleStructure{
				St: "string value",
				Bo: true,
				In: 55,
				Fl: 33.3,
			},
			wantErr: false,
		},
		{
			name: "structure larger than data",
			args: args{
				data: &common.Data{
					Ref: nil,
					S: map[string]*string{
						"_St": common.String("string value"),
					},
					I: map[string]*int64{
						"_Integer": common.Int64(55),
					},
					B: map[string]*bool{
						"_Bo": common.Bool(true),
					},
					ARef:   nil,
					AS:     nil,
					AI:     nil,
					AB:     nil,
					AF:     nil,
					Bytes:  nil,
					ABytes: nil,
				},
				ptrToStructure: &simpleStructure{},
			},
			wantStructure: &simpleStructure{
				St: "string value",
				Bo: true,
				In: 55,
			},
			wantErr: false,
		},
		{
			name: "test structure arrays",
			args: args{
				data: &common.Data{
					AS: map[string][]string{
						"_St": []string{"one", "two"},
					},
					AI: map[string][]int64{
						"_In": []int64{55, 22},
					},
					AB: map[string][]bool{
						"_Bool": {true, false, true},
					},
					AF: map[string][]float64{
						"_Float": {22.3, 44.5},
					},
					Bytes: map[string][]byte{
						"_BytesHere": {1, 2},
					},
					ABytes: map[string][][]byte{
						"_AB": {
							[]byte{1, 2},
							[]byte{1, 2, 3, 4},
						},
					},
				},
				ptrToStructure: &arraysStructure{},
			},
			wantStructure: &arraysStructure{
				St: []string{"one", "two"},
				Bo: []bool{true, false, true},
				In: []int64{55, 22},
				Fl: []float64{22.3, 44.5},
				B:  []byte{1, 2},
				AB: [][]byte{
					[]byte{
						1, 2,
					},
					[]byte{
						1, 2, 3, 4,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := common.DataIntoStructure(tt.args.data, tt.args.ptrToStructure); (err != nil) != tt.wantErr {
				t.Errorf("DataIntoStructure() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(tt.args.ptrToStructure, tt.wantStructure) {
				t.Errorf("DataIntoStructure() ptrToStructure != wantStructure ptrToStructure:\n%v\n\nwantStructure:\n%v", tt.args.ptrToStructure, tt.wantStructure)
			}
		})
	}
}

func TestStructureIntoData(t *testing.T) {
	type emptyStructure struct{}
	type simpleStructure struct {
		St string
		Bo bool
		In int64   `suffix:"Integer"`
		Fl float64 `suffix:"Float"`
	}
	type arraysStructure struct {
		St []string
		Bo []bool `suffix:"Bool"`
		In []int64
		Fl []float64 `suffix:"Float"`
		B  []byte    `suffix:"BytesHere"`
		AB [][]byte
	}
	type args struct {
		industry       string
		domain         string
		schema         string
		ptrToStructure interface{}
		data           *common.Data
	}
	initData1 := common.Data{}
	initData1.Init(nil)
	initData2 := common.Data{}
	initData2.Init(nil)
	initData3 := common.Data{}
	initData3.Init(nil)
	initData4 := common.Data{}
	initData4.Init(nil)
	tests := []struct {
		name     string
		args     args
		wantData *common.Data
		wantErr  bool
	}{
		{
			name: "empty structure nil data",
			args: args{
				industry:       "ind",
				domain:         "dom",
				schema:         "sch",
				ptrToStructure: &emptyStructure{},
				data:           nil,
			},
			wantData: nil,
			wantErr:  true,
		},
		{
			name: "simple structure produces Data with IsZero values",
			args: args{
				industry:       "ind",
				domain:         "dom",
				schema:         "sch",
				data:           &initData1,
				ptrToStructure: &simpleStructure{},
			},
			wantData: &common.Data{
				FirestoreID: "",
				IndustryID:  "ind",
				DomainID:    "dom",
				SchemaID:    "sch",
				Deprecated:  nil,
				Ref:         map[string]*string{},
				S: map[string]*string{
					"ind_dom_sch_St": common.String(""),
				},
				I: map[string]*int64{
					"ind_dom_sch_Integer": common.Int64(0),
				},
				B: map[string]*bool{
					"ind_dom_sch_Bo": common.Bool(false),
				},
				F: map[string]*float64{
					"ind_dom_sch_Float": common.Float64(0),
				},
				ARef:   map[string][]string{},
				AS:     map[string][]string{},
				AI:     map[string][]int64{},
				AB:     map[string][]bool{},
				AF:     map[string][]float64{},
				Bytes:  map[string][]byte{},
				ABytes: map[string][][]byte{},
				Nils:   map[string]bool{},
			},
			wantErr: false,
		},
		{
			name: "data larger than structure",
			args: args{
				industry: "ind",
				domain:   "dom",
				schema:   "sch",
				data:     &initData2,
				ptrToStructure: &simpleStructure{
					St: "string value",
					Bo: true,
					In: 55,
					Fl: 33.3,
				},
			},
			wantData: &common.Data{
				Ref: map[string]*string{},
				S: map[string]*string{
					"ind_dom_sch_St": common.String("string value"),
				},
				I: map[string]*int64{
					"ind_dom_sch_Integer": common.Int64(55),
				},
				B: map[string]*bool{
					"ind_dom_sch_Bo": common.Bool(true),
				},
				F: map[string]*float64{
					"ind_dom_sch_Float": common.Float64(33.3),
				},
				IndustryID: "ind",
				DomainID:   "dom",
				SchemaID:   "sch",
				Deprecated: nil,
				ARef:       map[string][]string{},
				AS:         map[string][]string{},
				AI:         map[string][]int64{},
				AB:         map[string][]bool{},
				AF:         map[string][]float64{},
				Bytes:      map[string][]byte{},
				ABytes:     map[string][][]byte{},
				Nils:       map[string]bool{},
			},
			wantErr: false,
		},
		{
			name: "test structure arrays",
			args: args{
				industry: "ind",
				domain:   "dom",
				schema:   "sch",
				data:     &initData4,
				ptrToStructure: &arraysStructure{
					St: []string{"one", "two"},
					Bo: []bool{true, false, true},
					In: []int64{55, 22},
					Fl: []float64{22.3, 44.5},
					B:  []byte{1, 2},
					AB: [][]byte{
						[]byte{
							1, 2,
						},
						[]byte{
							1, 2, 3, 4,
						},
					},
				},
			},
			wantData: &common.Data{
				IndustryID: "ind",
				DomainID:   "dom",
				SchemaID:   "sch",
				Deprecated: nil,
				Ref:        map[string]*string{},
				S:          map[string]*string{},
				I:          map[string]*int64{},
				B:          map[string]*bool{},
				F:          map[string]*float64{},
				ARef:       map[string][]string{},
				Nils:       map[string]bool{},
				AS: map[string][]string{
					"ind_dom_sch_St": []string{"one", "two"},
				},
				AI: map[string][]int64{
					"ind_dom_sch_In": []int64{55, 22},
				},
				AB: map[string][]bool{
					"ind_dom_sch_Bool": {true, false, true},
				},
				AF: map[string][]float64{
					"ind_dom_sch_Float": {22.3, 44.5},
				},
				Bytes: map[string][]byte{
					"ind_dom_sch_BytesHere": {1, 2},
				},
				ABytes: map[string][][]byte{
					"ind_dom_sch_AB": {
						[]byte{1, 2},
						[]byte{1, 2, 3, 4},
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := common.StructureIntoData(tt.args.industry, tt.args.domain, tt.args.schema, tt.args.ptrToStructure, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("StructureIntoData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(tt.args.data, tt.wantData) {
				t.Errorf("DataIntoStructure() data != wantData data:\n%v\n\nwantData:\n%v", tt.args.data, tt.wantData)
			}
		})
	}
}
