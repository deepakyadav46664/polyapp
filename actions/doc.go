// package Actions was created to house all Action code. Actions are code which is run as part of a Bot. A Bot is triggered
// usually at the loading time of a Task, when a change is made to the Task's data, or when the Task is "Done".
//
// Anyone ought to be able to write code which is used as an Action. The whole point of Actions is that they are a way
// to infinitely extend the functionality of Polyapp. You can use Actions to validate data, calculate values based
// on values currently present on screen, respond to a button press, or even get information from a third party and store it in the form.
// You receive the current state of the data on a user's screen, information about the request and response,
// and this package also includes tools to translate the murky Data into regular Go structs.
//
// Actions are configurable by setting BotStaticData when designing a Task.
package actions
