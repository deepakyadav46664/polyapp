package actions

import (
	"gitlab.com/polyapp-open-source/polyapp/common"
	"reflect"
	"testing"
)

func TestFindFieldsWhichNeedConversions(t *testing.T) {
	type args struct {
		fields    []Field
		oldSchema common.Schema
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "empty",
			args: args{
				fields:    []Field{},
				oldSchema: common.Schema{},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "oldSchema but fields is empty",
			args: args{
				fields: []Field{},
				oldSchema: common.Schema{
					FirestoreID:  "TestFindFieldsWhichNeedConversions",
					NextSchemaID: nil,
					Name:         nil,
					IndustryID:   "TestFindFieldsWhichNeedConversions",
					DomainID:     "TestFindFieldsWhichNeedConversions",
					SchemaID:     "TestFindFieldsWhichNeedConversions",
					DataTypes: map[string]string{
						"i_d_s_a": "S",
						"i_d_s_b": "AS",
					},
					DataHelpText: map[string]string{
						"i_d_s_a": "a",
						"i_d_s_b": "b",
					},
					DataKeys: []string{
						"i_d_s_a",
						"i_d_s_b",
					},
					IsPublic:   false,
					Deprecated: nil,
				},
			},
			want:    []string{"i_d_s_a", "i_d_s_b"},
			wantErr: false,
		},
		{
			name: "no oldSchema fields but Fields is full",
			args: args{
				fields: []Field{
					{
						Name:            "a",
						HelpText:        "a",
						UserInterface:   "text",
						Validator:       "",
						SelectOptions:   nil,
						UseAsDoneButton: false,
						ReadOnly:        false,
						Hidden:          false,
					},
					{
						Name:            "b",
						HelpText:        "b",
						UserInterface:   "text",
						Validator:       "",
						SelectOptions:   nil,
						UseAsDoneButton: false,
						ReadOnly:        false,
						Hidden:          false,
					},
				},
				oldSchema: common.Schema{
					FirestoreID:  "TestFindFieldsWhichNeedConversions",
					NextSchemaID: nil,
					Name:         nil,
					IndustryID:   "TestFindFieldsWhichNeedConversions",
					DomainID:     "TestFindFieldsWhichNeedConversions",
					SchemaID:     "TestFindFieldsWhichNeedConversions",
					DataTypes:    map[string]string{},
					DataHelpText: map[string]string{},
					DataKeys:     []string{},
					IsPublic:     false,
					Deprecated:   nil,
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "mix",
			args: args{
				fields: []Field{
					{
						Name:            "a",
						HelpText:        "a",
						UserInterface:   "text",
						Validator:       "",
						SelectOptions:   nil,
						UseAsDoneButton: false,
						ReadOnly:        false,
						Hidden:          false,
					},
					{
						Name:            "b",
						HelpText:        "b",
						UserInterface:   "text",
						Validator:       "",
						SelectOptions:   nil,
						UseAsDoneButton: false,
						ReadOnly:        false,
						Hidden:          false,
					},
				},
				oldSchema: common.Schema{
					FirestoreID:  "TestFindFieldsWhichNeedConversions",
					NextSchemaID: nil,
					Name:         nil,
					IndustryID:   "TestFindFieldsWhichNeedConversions",
					DomainID:     "TestFindFieldsWhichNeedConversions",
					SchemaID:     "TestFindFieldsWhichNeedConversions",
					DataTypes: map[string]string{
						"i_d_s_a": "S",
						"i_d_s_c": "AS",
					},
					DataHelpText: map[string]string{
						"i_d_s_a": "a",
						"i_d_s_c": "b",
					},
					DataKeys: []string{
						"i_d_s_a",
						"i_d_s_c",
					},
					IsPublic:   false,
					Deprecated: nil,
				},
			},
			want: []string{
				"i_d_s_c",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := FindFieldsWhichNeedConversions(tt.args.fields, tt.args.oldSchema)
			if (err != nil) != tt.wantErr {
				t.Errorf("FindFieldsWhichNeedConversions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindFieldsWhichNeedConversions() got = %v, want %v", got, tt.want)
			}
		})
	}
}
