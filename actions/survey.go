package actions

import (
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/integrity"
	"strings"
)

type CreateSurvey struct {
	Title                  string  `suffix:"Title"`
	Welcome                string  `suffix:"Welcome"`
	ExternalServiceURL     string  `suffix:"External Service URL"`
	GeneratedTaskURL       string  `suffix:"Generated Task URL"`
	Fields                 []Field `suffix:"Fields"`
	TaskID                 string  `suffix:"Task ID"`
	SchemaID               string  `suffix:"Schema ID"`
	UXID                   string  `suffix:"UX ID"`
	ViewTaskHelpTextDataID string  `suffix:"View Task Help Text Data ID"`
	EditTaskDataID         string  `suffix:"Edit Task Data ID"`
	DashboardDataID        string  `suffix:"Dashboard Data ID"`
	TableReportDataID      string  `suffix:"Table Report Data ID"`
}

// ActionCreateSurvey Surveys are public or private forms whose goals are different from regular Tasks in a few key ways:
// - Accessed via a link: Respondents should be able to click a link to start the survey.
// - Users may all be new to Polyapp: Public - facing surveys shouldn't require people to sign in or be issued security credentials to access the survey unless the survey creator desires that functionality.
// - Branded: Respondents who click on a survey link expect to see something to indicate the next page is owned by the company who sent them the survey. This might include a specific theme, brand colors, and/or a logo.
// - Users are less incentivized to finish: Almost all surveys are for the benefit of the person issuing the survey, whereas Polyapp's Tasks assume you intrinsically want to finish the Task because it is part of your job. We need to provide them with incentives to start the survey, incentivize them to keep going, and give them an appropriate reward when they are done.
// - Confidential: Each survey response should be hidden from the other people taking the survey. Polyapp is designed to grant access to all results of a Task, so security around surveys needs to be tightened.
//
//This Task is an excellent way to create such surveys.
func ActionCreateSurvey(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	CreateSurvey := CreateSurvey{}
	err = allDB.DataIntoStructureComplex(data, &CreateSurvey)
	if err != nil {
		return fmt.Errorf("DataIntoStructureComplex: %w", err)
	}
	fields := make([]string, len(CreateSurvey.Fields))
	for i := range CreateSurvey.Fields {
		fields[i] = common.CreateRef("polyapp", "TaskSchemaUX", "dssHRRryWueHfJDfcCCMgCAsq", "tBrxmDfyKRTsOwlXVUgwqgfdo", "SszMuOVRaMIOUteoWiXClbpSC", CreateSurvey.Fields[i].FirestoreID)
	}
	editTask := EditTask{
		Name:                      CreateSurvey.Title,
		HelpText:                  CreateSurvey.Welcome,
		AgreeToMakePublic:         false,
		ExportAsJSONToFile:        false,
		TaskID:                    CreateSurvey.TaskID,
		SchemaID:                  CreateSurvey.SchemaID,
		UXID:                      CreateSurvey.UXID,
		Industry:                  "Education and Health Services",
		Domain:                    "Survey",
		BotsTriggeredAtLoad:       []string{"MMTwGvAzFXtekHfwSHjnVlEEc"},
		BotsTriggeredContinuously: []string{"gihZWbGOqhEwswLqJNPoIkLQH"},
		BotsTriggeredAtDone:       []string{},
		BotStaticData:             []string{},
		Fields:                    []string{},
		Subtasks:                  nil,
		ViewTaskHelpTextDataID:    CreateSurvey.ViewTaskHelpTextDataID,
	}
	if CreateSurvey.ExternalServiceURL != "" {
		// Add the HTTPS Call Bot
		editTask.BotsTriggeredContinuously = append(editTask.BotsTriggeredContinuously, "dplFLIDbLxEpMicZPIZBfRElq")
		// Configure the BotActionHTTPSCall
		editTask.BotStaticData = append(editTask.BotStaticData, "HTTPSCall POST "+CreateSurvey.ExternalServiceURL)
	}
	editTaskData := common.Data{}
	_ = editTaskData.Init(nil)
	editTaskData.SchemaID = "gjGLMlcNApJyvRjmgQbopsXPI"
	err = integrity.BackPopulate(&editTaskData)
	if err != nil {
		return fmt.Errorf("integrity.BackPopulate: %w", err)
	}
	err = common.StructureIntoData("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", &editTask, &editTaskData)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	// Note: This strategy is pretty weird. I'm not duplicating the Fields into a new Data document; I'm using the
	// Fields documents which we already have from the Create Survey task. You're definitely not supposed to do this
	// because it creates a separation of concerns issue.
	editTaskData.ARef = map[string][]string{
		"polyapp_TaskSchemaUX_SszMuOVRaMIOUteoWiXClbpSC_Fields": fields,
	}
	if CreateSurvey.EditTaskDataID != "" {
		editTaskData.FirestoreID = CreateSurvey.EditTaskDataID
		err = allDB.UpdateData(&editTaskData)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
	} else {
		editTaskData.FirestoreID = common.GetRandString(25)
		err = allDB.Create(&editTaskData)
		if err != nil {
			return fmt.Errorf("allDB.CreateData editTaskData: %w", err)
		}
	}

	editTaskRequest := &common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "TaskSchemaUX",
		UserID:     request.UserID,
		UserCache:  request.UserCache,
		RoleID:     request.RoleID,
	}
	err = ActionEditTask(&editTaskData, editTaskRequest, response)
	if err != nil {
		return fmt.Errorf("ActionEditTask: %w", err)
	}
	err = allDB.Read(&editTaskData)
	if err != nil {
		return fmt.Errorf("allDB.Read: %w", err)
	}
	err = common.DataIntoStructure(&editTaskData, &editTask)
	if err != nil {
		return fmt.Errorf("common.DataIntoSTructure: %w", err)
	}

	CreateSurvey.TaskID = editTask.TaskID
	CreateSurvey.SchemaID = editTask.SchemaID
	CreateSurvey.UXID = editTask.UXID
	CreateSurvey.ViewTaskHelpTextDataID = editTask.ViewTaskHelpTextDataID
	CreateSurvey.GeneratedTaskURL = editTaskRequest.FinishURL[0:strings.Index(editTaskRequest.FinishURL, "&data=")]
	CreateSurvey.EditTaskDataID = editTaskData.FirestoreID

	// Create a table report to show in the dashboard
	createTableData := common.Data{
		IndustryID:  "polyapp",
		DomainID:    "Reporting",
		SchemaID:    "IYrpkFurrcTnXqbLuMawFknMd",
		FirestoreID: common.GetRandString(25),
	}
	createTableData.Init(nil)
	CreateTableFromData := CreateTableFromData{
		Name:       CreateSurvey.Title,
		Schema:     editTask.SchemaID,
		TableHTML:  nil,
		TimeFilter: []chartFilter{},
		HiddenColumns: []string{
			common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, "Done User IDs"),
			common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, "Done Timestamps"),
			common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, "Done"),
			common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, "Load Timestamps"),
			common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, "Load User IDs"),
		},
		ChartFilters: []chartFilter{},
	}
	if CreateSurvey.TableReportDataID == "" {
		CreateSurvey.TableReportDataID = createTableData.FirestoreID
		err = allDB.Create(&createTableData)
		if err != nil {
			return fmt.Errorf("allDB.Create %v: %w", createTableData.FirestoreID, err)
		}
	} else {
		createTableData.FirestoreID = CreateSurvey.TableReportDataID
	}
	tableReportRequest := common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "Reporting",
		TaskID:     "zIoQjcmACDOdQsrKckUmmzLTZ",
		UXID:       "GjxnRTamLcVfyALAMjpWaywYm",
		SchemaID:   "IYrpkFurrcTnXqbLuMawFknMd",
		DataID:     CreateSurvey.TableReportDataID,
		UserID:     request.UserID,
		UserCache:  request.UserCache,
		RoleID:     request.RoleID,
	}

	err = createTableFromData(&CreateTableFromData, request)
	if err != nil {
		return fmt.Errorf("createTableFromData: %w", err)
	}

	err = common.StructureIntoData(tableReportRequest.IndustryID, tableReportRequest.DomainID, tableReportRequest.SchemaID, &CreateTableFromData, &createTableData)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	err = allDB.UpdateData(&createTableData)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// Create the dashboard
	dashboardData := common.Data{
		IndustryID:  "polyapp",
		DomainID:    "Reporting",
		SchemaID:    "ZhcLBvwSBdjcpFVApHUyAygqv",
		FirestoreID: common.GetRandString(25),
	}
	dashboardData.Init(nil)
	if CreateSurvey.DashboardDataID == "" {
		CreateSurvey.DashboardDataID = dashboardData.FirestoreID
		err = allDB.Create(&dashboardData)
		if err != nil {
			return fmt.Errorf("allDB.Create %v: %w", dashboardData.FirestoreID, err)
		}
	} else {
		dashboardData.FirestoreID = CreateSurvey.DashboardDataID
	}
	CreateDashboard := CreateDashboard{
		Header:        CreateSurvey.Title,
		ReportIDs:     []string{tableReportRequest.DataID},
		SetAsHomePage: false,
		Fields:        []Field{},
		Links: []Link{
			{Name: "Survey Responses (Change Data)", URL: CreateSurvey.GeneratedTaskURL},
			{Name: "Edit Survey", URL: common.CreateURLFromRequest(*request)},
			{Name: "Charts", URL: "/polyappChangeData?industry=polyapp&domain=Reporting&task=ltygzBCYpjlqeXRhSVPZtzJKb&ux=cYdflemxtmQCdlsgiGVcsgFpm&schema=nMyfcJQJSykcdLpvjqFRZXkeO"},
			{Name: "Table Reports", URL: "/polyappChangeData?industry=polyapp&domain=Reporting&task=zIoQjcmACDOdQsrKckUmmzLTZ&ux=GjxnRTamLcVfyALAMjpWaywYm&schema=IYrpkFurrcTnXqbLuMawFknMd"},
			{Name: "Import Data", URL: "/polyappChangeData?industry=polyapp&domain=Import&task=NmWRIUMwmZsZTPkqtBiacDVjE&ux=kgGbDPiPAmGvJawTgbfjyXzUS&schema=FLDIivYCRvcKHvSJkPQMMVwTy"},
			{Name: "Export Data", URL: "/polyappChangeData?industry=polyapp&domain=Export&task=fKgfaVERIycoCdJoaXmhDTSYr&ux=ZHrpqruzyePaJZLMXbGqiNNzg&schema=uGhKgwWImpQhdXIPssYLfuxaO"},
		},
		DashboardHTML:   nil,
		DashboardDataID: CreateSurvey.DashboardDataID,
		ExportToJSON:    false,
		industry:        dashboardData.IndustryID,
		domain:          dashboardData.DomainID,
		schema:          dashboardData.SchemaID,
	}
	CreateDashboard.industry = dashboardData.IndustryID
	CreateDashboard.domain = dashboardData.DomainID
	CreateDashboard.schema = dashboardData.SchemaID
	CreateDashboard.DashboardDataID = dashboardData.FirestoreID

	err = common.StructureIntoData(dashboardData.IndustryID, dashboardData.DomainID, dashboardData.SchemaID, &CreateDashboard, &dashboardData)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&dashboardData)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData for DashboardData %v: %w", dashboardData.FirestoreID, err)
	}

	if request.UserCache == nil {
		u, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
		request.UserCache = &u
	}

	err = createDashboard(&CreateDashboard, request.UserCache)
	if err != nil {
		return fmt.Errorf("CreateDashboard: %w", err)
	}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &CreateSurvey, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	response.NewURL = "/blob/assets/" + CreateDashboard.DashboardDataID + "/polyapp_Reporting_ZhcLBvwSBdjcpFVApHUyAygqv_Dashboard%20HTML?cacheBuster=" + common.GetRandString(15)

	return nil
}

func ActionSurveyAtLoad(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	createSurveyQueryable, err := allDB.QueryEquals(common.CollectionData, "polyapp_TaskSchemaUX_wknXhEqpHMdMaOhsqGcAUlHYk_Generated Task URL", "/t/Education and Health Services/Survey/"+request.TaskID+"?ux="+request.UXID+"&schema="+request.SchemaID)
	if err != nil {
		return fmt.Errorf("allDB.QueryEquals: %w", err)
	}
	createSurveyData := createSurveyQueryable.(*common.Data)
	createSurveyS := CreateSurvey{}
	err = common.DataIntoStructure(createSurveyData, &createSurveyS)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	// add the survey Welcome
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "h2",
		Action:         "afterend",
		HTML:           "<h2 class=\"display-3\">" + createSurveyS.Title + "</h2>",
	}, common.ModDOM{
		DeleteSelector: "h2:first-of-type",
	}, common.ModDOM{
		DeleteSelector: "h6",
		InsertSelector: "h2",
		Action:         "afterend",
		HTML:           "<p>" + strings.ReplaceAll(createSurveyS.Welcome, "\n", "<br>") + "</p>",
	})

	// move all Prediction elements into their own column
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "#polyappJSMain > div",
		Action:         "wrap",
		HTML:           `<div class="container-fluid"><div class="row"><div class="col col-12 col-md-9"></div><div class="col col-12 col-md-3 polyappJSPredictContainer"></div></div></div>`, // one-liner prevents text nodes for \n which cause .Wrap to fail.
	})
	return nil
}

// ActionSurveyContinuous is called when someone enters information into the survey.
// It is used to update predictions.
func ActionSurveyContinuous(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	createSurveyQueryable, err := allDB.QueryEquals(common.CollectionData, "polyapp_TaskSchemaUX_wknXhEqpHMdMaOhsqGcAUlHYk_Generated Task URL", "/t/Education and Health Services/Survey/"+request.TaskID+"?ux="+request.UXID+"&schema="+request.SchemaID)
	if err != nil {
		return fmt.Errorf("allDB.QueryEquals: %w", err)
	}
	createSurveyData := createSurveyQueryable.(*common.Data)
	createSurveyS := CreateSurvey{}
	err = allDB.DataIntoStructureComplex(createSurveyData, &createSurveyS)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	return nil
}
