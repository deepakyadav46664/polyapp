package elasticsearchCRUD

import "testing"

func TestRawSearchToQueryString(t *testing.T) {
	if rawSearchToQueryString("") != "" {
		t.Error("Failed with empty string")
	}
	if rawSearchToQueryString("lsdlsd") != "(lsdlsd)" {
		t.Error("Failed with single word")
	}
	if rawSearchToQueryString("lkasdlk asdlk") != "(lkasdlk OR asdlk)" {
		t.Error("Failed with two words")
	}
	if rawSearchToQueryString("hello me how are you today?") != "(hello OR me OR how OR are OR you OR today)" {
		t.Error("Failed with several words")
	}
	if rawSearchToQueryString("I am quite good. Ho23w are you? 33") != "(I OR am OR quite OR good OR Ho23w OR are OR you OR 33)" {
		t.Error("Failed with additional punctuation")
	}
}
