package postgresCRUD

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strconv"
	"strings"
	"sync"
	"text/template"
)

type field struct {
	Name string
	Type string
}
type row struct {
	Name  string
	Value interface{}
}

// should be refactored
type q struct {
	CollectionName string
	FirestoreID    string
	Fields         []field
	Rows           []row
}

func Create(queryable common.Queryable) error {
	client, err := GetClient()
	if err != nil {
		return fmt.Errorf("GetClient: %w", err)
	}
	qInput := q{
		CollectionName: tableFromCollectionName(queryable.CollectionName()),
		Rows:           make([]row, 0),
		FirestoreID:    queryable.GetFirestoreID(),
	}
	qInput.Rows, err = makeRows(queryable)
	if err != nil {
		return fmt.Errorf("makeRows: %w", err)
	}
	for i := range qInput.Fields {
		qInput.Fields[i].Name = ColumnNameEncode(qInput.Fields[i].Name)
	}

	createTemplate := `INSERT INTO {{.CollectionName}}({{range .Rows}}{{.Name}}, {{end}}created_at, updated_at)
VALUES({{range .Rows}}{{.Value}},{{end}} NOW(), NOW())`
	t, err := template.New("").Parse(createTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	var w bytes.Buffer
	err = t.Execute(&w, qInput)
	if err != nil {
		return fmt.Errorf("t.Execute: %w", err)
	}
	_, err = client.Exec(w.String())
	if err != nil && strings.Contains(err.Error(), "bad connection") {
		CloseClient()
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil && strings.Contains(err.Error(), "database is closed") {
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil {
		return fmt.Errorf("client.Exec: %w", err)
	}
	return nil
}

func makeRows(queryable common.Queryable) ([]row, error) {
	rowsOut := make([]row, 0)
	simplified, err := queryable.Simplify()
	if err != nil {
		return nil, fmt.Errorf("queryable.Simplify: %w", err)
	}
	common.Dereference(simplified)
	for k, v := range simplified {
		switch vConcrete := v.(type) {
		case []interface{}:
			if len(vConcrete) < 1 {
				continue
			}
			var toWrite bytes.Buffer
			toWrite.WriteString("ARRAY [")
			for i := range vConcrete {
				if i > 0 {
					toWrite.WriteString(",")
				}
				switch viConcrete := vConcrete[i].(type) {
				case string:
					toWrite.WriteString(`'` + viConcrete + `'`)
				case int64:
					toWrite.WriteString(strconv.FormatInt(viConcrete, 10))
				case float64:
					toWrite.WriteString(strconv.FormatFloat(viConcrete, 'f', -1, 64))
				case bool:
					// TODO. This doesn't seem to show support: https://www.postgresql.org/docs/9.1/arrays.html
					// Should I throw an error? I think not, because we aren't going to want to query on []bool anyway.
				}
			}
			toWrite.WriteString("]")
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: toWrite.String(),
			})
		case []string:
			if len(vConcrete) < 1 {
				continue
			}
			var toWrite bytes.Buffer
			toWrite.WriteString("ARRAY [")
			for i := range vConcrete {
				if i > 0 {
					toWrite.WriteString(",")
				}
				toWrite.WriteString(`'` + common.RemoveNonAlphaNumeric(vConcrete[i]) + `'`)
			}
			toWrite.WriteString("]")
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: toWrite.String(),
			})
		case []int64:
			if len(vConcrete) < 1 {
				continue
			}
			var toWrite bytes.Buffer
			toWrite.WriteString("ARRAY [")
			for i := range vConcrete {
				if i > 0 {
					toWrite.WriteString(",")
				}
				toWrite.WriteString(strconv.FormatInt(vConcrete[i], 10))
			}
			toWrite.WriteString("]")
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: toWrite.String(),
			})
		case []float64:
			if len(vConcrete) < 1 {
				continue
			}
			var toWrite bytes.Buffer
			toWrite.WriteString("ARRAY [")
			for i := range vConcrete {
				if i > 0 {
					toWrite.WriteString(",")
				}
				toWrite.WriteString(strconv.FormatFloat(vConcrete[i], 'f', -1, 64))
			}
			toWrite.WriteString("]")
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: toWrite.String(),
			})
		case []bool:
			if len(vConcrete) < 1 {
				continue
			}
			// TODO. This doesn't seem to show support: https://www.postgresql.org/docs/9.1/arrays.html
			// Should I throw an error? I think not, because we aren't going to want to query on []bool anyway.
		case string:
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: `'` + common.RemoveNonAlphaNumeric(vConcrete) + `'`,
			})
		case int64, float64, bool:
			rowsOut = append(rowsOut, row{
				Name:  k,
				Value: v,
			})
		default:
		}
	}
	rowsOut = append(rowsOut, row{
		Name:  common.PolyappFirestoreID,
		Value: `'` + queryable.GetFirestoreID() + `'`,
	})
	for i := range rowsOut {
		rowsOut[i].Name = ColumnNameEncode(rowsOut[i].Name)
	}
	return rowsOut, nil
}

func ColumnNameEncode(name string) string {
	oldnew := make([]string, 0)
	for _, c := range "ABCDEFGHIJKLMNOPQRSTUVWXYZ_" {
		oldnew = append(oldnew, string(c), "_"+strings.ToLower(string(c)))
	}
	replacer := strings.NewReplacer(oldnew...)
	return replacer.Replace(name)
}

func ColumnNameDecode(name string) string {
	var builtKey strings.Builder
	nextUpper := false
	for _, r := range name {
		if nextUpper {
			builtKey.WriteString(strings.ToUpper(string(r)))
		} else if r == '_' {
			nextUpper = true
			continue
		} else {
			builtKey.WriteRune(r)
		}
		nextUpper = false
	}
	return builtKey.String()
}

func Read(q common.Queryable) error {
	client, err := GetClient()
	if err != nil {
		return fmt.Errorf("GetClient: %w", err)
	}
	m := make(map[string]interface{}, 0)
	row := client.QueryRowx(`SELECT row_to_json(` + tableFromCollectionName(q.CollectionName()) + `) FROM ` + tableFromCollectionName(q.CollectionName()) + ` WHERE ` + ColumnNameEncode(common.PolyappFirestoreID) + `='` + q.GetFirestoreID() + `'`)
	var rowJSON string
	err = row.Scan(&rowJSON)
	if err != nil && strings.Contains(err.Error(), "sql: no rows in result set") {
		return status.Error(codes.NotFound, err.Error())
	} else if err != nil {
		return fmt.Errorf("row.Scan: %w", err)
	}
	err = json.Unmarshal([]byte(rowJSON), &m)
	if err != nil {
		return fmt.Errorf("row.MapScan: %w", err)
	}
	newMap := make(map[string]interface{})
	for k, v := range m {
		newMap[ColumnNameDecode(k)] = v
	}
	return q.Init(newMap)
}

func Update(queryable common.Queryable) error {
	client, err := GetClient()
	if err != nil {
		return fmt.Errorf("GetClient: %w", err)
	}
	qInput := q{
		CollectionName: tableFromCollectionName(queryable.CollectionName()),
		Rows:           make([]row, 0),
		FirestoreID:    queryable.GetFirestoreID(),
	}
	qInput.Rows, err = makeRows(queryable)
	if err != nil {
		return fmt.Errorf("makeRows: %w", err)
	}
	for i := range qInput.Fields {
		qInput.Fields[i].Name = ColumnNameEncode(qInput.Fields[i].Name)
	}
	updateTemplate := `UPDATE {{.CollectionName}} SET {{range .Fields}}{{.Name}} = '{{.Value}}', {{end}}updated_at = NOW() WHERE ` + ColumnNameEncode(common.PolyappFirestoreID) + ` = '{{.FirestoreID}}';`
	t, err := template.New("").Parse(updateTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	var w bytes.Buffer
	err = t.Execute(&w, qInput)
	if err != nil {
		return fmt.Errorf("t.Execute: %w", err)
	}
	_, err = client.Exec(w.String())
	if err != nil && strings.Contains(err.Error(), "bad connection") {
		CloseClient()
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil && strings.Contains(err.Error(), "database is closed") {
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil {
		return fmt.Errorf("client.Exec: %w", err)
	}
	return nil
}

func Delete(firestoreID string, collectionName string) error {
	client, err := GetClient()
	if err != nil {
		return fmt.Errorf("GetClient: %w", err)
	}
	query := `DELETE FROM ` + tableFromCollectionName(collectionName) + ` WHERE ` + ColumnNameEncode(common.PolyappFirestoreID) + `='` + firestoreID + `'`
	_, err = client.Exec(query)
	if err != nil && strings.Contains(err.Error(), "bad connection") {
		CloseClient()
		client, _ = GetClient()
		_, err = client.Exec(query)
	}
	if err != nil && strings.Contains(err.Error(), "database is closed") {
		client, _ = GetClient()
		_, err = client.Exec(query)
	}
	if err != nil && strings.Contains(err.Error(), "SQLSTATE 42P01") {
		// the table was never created so this row does not exist.
		return nil
	}
	if err != nil {
		return fmt.Errorf("client.Exec: %w", err)
	}
	return nil
}

var (
	createdTables    map[string]bool
	createdTablesMux sync.Mutex
)

func CreateTableIfNotExists(queryable common.Queryable) error {
	client, err := GetClient()
	if err != nil {
		return fmt.Errorf("GetClient: %w", err)
	}
	createdTablesMux.Lock()
	defer createdTablesMux.Unlock()
	if createdTables == nil {
		createdTables = make(map[string]bool)
	}
	if createdTables[tableFromCollectionName(queryable.CollectionName())] {
		return nil
	}
	qInput := q{
		CollectionName: tableFromCollectionName(queryable.CollectionName()),
	}
	simplified, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("queryable.Simplify: %w", err)
	}
	common.Dereference(simplified)
	qInput.Fields = make([]field, 0)
	for k, v := range simplified {
		f := field{
			Name: k,
		}
		switch v.(type) {
		case string:
			f.Type = "TEXT"
		case int64:
			f.Type = "BIGINT"
		case float64:
			f.Type = "NUMERIC(64)"
		case bool:
			f.Type = "BOOLEAN"
		case []string:
			f.Type = "TEXT []"
		case []int64:
			f.Type = "BIGINT []"
		case []float64:
			f.Type = "NUMERIC(64) []"
		case []bool:
			f.Type = "BOOLEAN []"
		default:
			continue
		}
		qInput.Fields = append(qInput.Fields, f)
	}
	if simplified[common.PolyappFirestoreID] == nil || simplified[common.PolyappFirestoreID] == "" {
		qInput.Fields = append(qInput.Fields, field{
			Name: common.PolyappFirestoreID,
			Type: "TEXT",
		})
	}

	for i := range qInput.Rows {
		qInput.Rows[i].Name = ColumnNameEncode(qInput.Rows[i].Name)
	}
	for i := range qInput.Fields {
		qInput.Fields[i].Name = ColumnNameEncode(qInput.Fields[i].Name)
	}

	createQueryTemplate := `CREATE TABLE IF NOT EXISTS {{.CollectionName}} ( id SERIAL NOT NULL, created_at timestamp NOT NULL, updated_at timestamp NOT NULL, {{range .Fields}}{{.Name}} {{.Type}}, {{end}}PRIMARY KEY (id) );`
	t, err := template.New("").Parse(createQueryTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	var w bytes.Buffer
	err = t.Execute(&w, qInput)
	if err != nil {
		return fmt.Errorf("t.Execute: %w", err)
	}
	_, err = client.Exec(`ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES TO polyappmain`)
	if err != nil {
		return fmt.Errorf("ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES TO polyappmain: %w", err)
	}
	_, err = client.Exec(`ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE, SELECT ON SEQUENCES TO polyappmain`)
	if err != nil {
		return fmt.Errorf("ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES TO polyappmain: %w", err)
	}
	_, err = client.Exec(w.String())
	if err != nil && strings.Contains(err.Error(), "bad connection") {
		CloseClient()
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil && strings.Contains(err.Error(), "database is closed") {
		client, _ = GetClient()
		_, err = client.Exec(w.String())
	}
	if err != nil && strings.Contains(err.Error(), "permission denied for table") {
		// https://stackoverflow.com/questions/52243652/gcp-sql-postgres-problem-with-privileges-cant-run-a-query-with-postgres-user
		_, err = client.Exec(`ALTER TABLE ` + ColumnNameEncode(qInput.CollectionName) + ` OWNER to polyappmain`)
		if err != nil {
			return fmt.Errorf(`ALTER TABLE `+ColumnNameEncode(qInput.CollectionName)+" OWNER to polyappmain: %w", err)
		}
		_, err = client.Exec(w.String())
	}
	if err != nil {
		return fmt.Errorf("client.Exec: %w", err)
	}
	createdTables[queryable.CollectionName()] = true
	return nil
}

func tableFromCollectionName(collectionName string) string {
	upperCaseCollectionName := strings.ToUpper(collectionName)
	if isReservedKeyword(upperCaseCollectionName) {
		return collectionName + "t"
	}
	return collectionName
}

var (
	reservedMap      map[string]bool
	reservedKeywords = []string{"ACCESS",
		"ACTION",
		"ADA",
		"ADD",
		"ADMIN",
		"AFTER",
		"AGGREGATE",
		"ALIAS",
		"ALL",
		"ALLOCATE",
		"ALSO",
		"ALTER",
		"ALWAYS",
		"ANALYSE",
		"ANALYZE",
		"AND",
		"ANY",
		"ARE",
		"ARRAY",
		"AS",
		"ASC",
		"ASENSITIVE",
		"ASSERTION",
		"ASSIGNMENT",
		"ASYMMETRIC",
		"AT",
		"ATOMIC",
		"ATTRIBUTE",
		"ATTRIBUTES",
		"AUTHORIZATION",
		"AVG",
		"BACKWARD",
		"BEFORE",
		"BEGIN",
		"BERNOULLI",
		"BETWEEN",
		"BIGINT",
		"BINARY",
		"BIT",
		"BITVAR",
		"BIT_LENGTH",
		"BLOB",
		"BOOLEAN",
		"BOTH",
		"BREADTH",
		"BY",
		"C",
		"CACHE",
		"CALL",
		"CALLED",
		"CARDINALITY",
		"CASCADE",
		"CASCADED",
		"CASE",
		"CAST",
		"CATALOG",
		"CATALOG_NAME",
		"CEIL",
		"CEILING",
		"CHAIN",
		"CHAR",
		"CHARACTER",
		"CHARACTERISTICS",
		"CHARACTERS",
		"CHARACTER_LENGTH",
		"CHARACTER_SET_CATALOG",
		"CHARACTER_SET_NAME",
		"CHARACTER_SET_SCHEMA",
		"CHAR_LENGTH",
		"CHECK",
		"CHECKED",
		"CHECKPOINT",
		"CLASS",
		"CLASS_ORIGIN",
		"CLOB",
		"CLOSE",
		"CLUSTER",
		"COALESCE",
		"COBOL",
		"COLLATE",
		"COLLATION",
		"COLLATION_CATALOG",
		"COLLATION_NAME",
		"COLLATION_SCHEMA",
		"COLLECT",
		"COLUMN",
		"COLUMN_NAME",
		"COMMAND_FUNCTION",
		"COMMAND_FUNCTION_CODE",
		"COMMENT",
		"COMMIT",
		"COMMITTED",
		"COMPLETION",
		"CONDITION",
		"CONDITION_NUMBER",
		"CONNECT",
		"CONNECTION",
		"CONNECTION_NAME",
		"CONSTRAINT",
		"CONSTRAINTS",
		"CONSTRAINT_CATALOG",
		"CONSTRAINT_NAME",
		"CONSTRAINT_SCHEMA",
		"CONSTRUCTOR",
		"CONTAINS",
		"CONTINUE",
		"CONVERSION",
		"CONVERT",
		"COPY",
		"CORR",
		"CORRESPONDING",
		"COUNT",
		"COVAR_POP",
		"COVAR_SAMP",
		"CREATE",
		"CREATEDB",
		"CREATEROLE",
		"CREATEUSER",
		"CROSS",
		"CSV",
		"CUBE",
		"CUME_DIST",
		"CURRENT",
		"CURRENT_DATE",
		"CURRENT_DEFAULT_TRANSFORM_GROUP",
		"CURRENT_PATH",
		"CURRENT_ROLE",
		"CURRENT_TIME",
		"CURRENT_TIMESTAMP",
		"CURRENT_TRANSFORM_GROUP_FOR_TYPE",
		"CURRENT_USER",
		"CURSOR",
		"CURSOR_NAME",
		"CYCLE",
		"DATA",
		"DATABASE",
		"DATE",
		"DATETIME_INTERVAL_CODE",
		"DATETIME_INTERVAL_PRECISION",
		"DAY",
		"DEALLOCATE",
		"DEC",
		"DECIMAL",
		"DECLARE",
		"DEFAULT",
		"DEFAULTS",
		"DEFERRABLE",
		"DEFERRED",
		"DEFINED",
		"DEFINER",
		"DEGREE",
		"DELETE",
		"DELIMITER",
		"DELIMITERS",
		"DENSE_RANK",
		"DEPTH",
		"DEREF",
		"DERIVED",
		"DESC",
		"DESCRIBE",
		"DESCRIPTOR",
		"DESTROY",
		"DESTRUCTOR",
		"DETERMINISTIC",
		"DIAGNOSTICS",
		"DICTIONARY",
		"DISABLE",
		"DISCONNECT",
		"DISPATCH",
		"DISTINCT",
		"DO",
		"DOMAIN",
		"DOUBLE",
		"DROP",
		"DYNAMIC",
		"DYNAMIC_FUNCTION",
		"DYNAMIC_FUNCTION_CODE",
		"EACH",
		"ELEMENT",
		"ELSE",
		"ENABLE",
		"ENCODING",
		"ENCRYPTED",
		"END",
		"END-EXEC",
		"EQUALS",
		"ESCAPE",
		"EVERY",
		"EXCEPT",
		"EXCEPTION",
		"EXCLUDE",
		"EXCLUDING",
		"EXCLUSIVE",
		"EXEC",
		"EXECUTE",
		"EXISTING",
		"EXISTS",
		"EXP",
		"EXPLAIN",
		"EXTERNAL",
		"EXTRACT",
		"FALSE",
		"FETCH",
		"FILTER",
		"FINAL",
		"FIRST",
		"FLOAT",
		"FLOOR",
		"FOLLOWING",
		"FOR",
		"FORCE",
		"FOREIGN",
		"FORTRAN",
		"FORWARD",
		"FOUND",
		"FREE",
		"FREEZE",
		"FROM",
		"FULL",
		"FUNCTION",
		"FUSION",
		"G",
		"GENERAL",
		"GENERATED",
		"GET",
		"GLOBAL",
		"GO",
		"GOTO",
		"GRANT",
		"GRANTED",
		"GREATEST",
		"GROUP",
		"GROUPING",
		"HANDLER",
		"HAVING",
		"HEADER",
		"HIERARCHY",
		"HOLD",
		"HOST",
		"HOUR",
		"IDENTITY",
		"IGNORE",
		"ILIKE",
		"IMMEDIATE",
		"IMMUTABLE",
		"IMPLEMENTATION",
		"IMPLICIT",
		"IN",
		"INCLUDING",
		"INCREMENT",
		"INDEX",
		"INDICATOR",
		"INFIX",
		"INHERIT",
		"INHERITS",
		"INITIALIZE",
		"INITIALLY",
		"INNER",
		"INOUT",
		"INPUT",
		"INSENSITIVE",
		"INSERT",
		"INSTANCE",
		"INSTANTIABLE",
		"INSTEAD",
		"INT",
		"INTEGER",
		"INTERSECT",
		"INTERSECTION",
		"INTERVAL",
		"INTO",
		"INVOKER",
		"IS",
		"ISNULL",
		"ISOLATION",
		"ITERATE",
		"JOIN",
		"K",
		"KEY",
		"KEY_MEMBER",
		"KEY_TYPE",
		"LANCOMPILER",
		"LANGUAGE",
		"LARGE",
		"LAST",
		"LATERAL",
		"LEADING",
		"LEAST",
		"LEFT",
		"LENGTH",
		"LESS",
		"LEVEL",
		"LIKE",
		"LIMIT",
		"LISTEN",
		"LN",
		"LOAD",
		"LOCAL",
		"LOCALTIME",
		"LOCALTIMESTAMP",
		"LOCATION",
		"LOCATOR",
		"LOCK",
		"LOGIN",
		"LOWER",
		"M",
		"MAP",
		"MATCH",
		"MATCHED",
		"MAX",
		"MAXVALUE",
		"MEMBER",
		"MERGE",
		"MESSAGE_LENGTH",
		"MESSAGE_OCTET_LENGTH",
		"MESSAGE_TEXT",
		"METHOD",
		"MIN",
		"MINUTE",
		"MINVALUE",
		"MOD",
		"MODE",
		"MODIFIES",
		"MODIFY",
		"MODULE",
		"MONTH",
		"MORE",
		"MOVE",
		"MULTISET",
		"MUMPS",
		"NAME",
		"NAMES",
		"NATIONAL",
		"NATURAL",
		"NCHAR",
		"NCLOB",
		"NESTING",
		"NEW",
		"NEXT",
		"NO",
		"NOCREATEDB",
		"NOCREATEROLE",
		"NOCREATEUSER",
		"NOINHERIT",
		"NOLOGIN",
		"NONE",
		"NORMALIZE",
		"NORMALIZED",
		"NOSUPERUSER",
		"NOT",
		"NOTHING",
		"NOTIFY",
		"NOTNULL",
		"NOWAIT",
		"NULL",
		"NULLABLE",
		"NULLIF",
		"NULLS",
		"NUMBER",
		"NUMERIC",
		"OBJECT",
		"OCTETS",
		"OCTET_LENGTH",
		"OF",
		"OFF",
		"OFFSET",
		"OIDS",
		"OLD",
		"ON",
		"ONLY",
		"OPEN",
		"OPERATION",
		"OPERATOR",
		"OPTION",
		"OPTIONS",
		"OR",
		"ORDER",
		"ORDERING",
		"ORDINALITY",
		"OTHERS",
		"OUT",
		"OUTER",
		"OUTPUT",
		"OVER",
		"OVERLAPS",
		"OVERLAY",
		"OVERRIDING",
		"OWNER",
		"PAD",
		"PARAMETER",
		"PARAMETERS",
		"PARAMETER_MODE",
		"PARAMETER_NAME",
		"PARAMETER_ORDINAL_POSITION",
		"PARAMETER_SPECIFIC_CATALOG",
		"PARAMETER_SPECIFIC_NAME",
		"PARAMETER_SPECIFIC_SCHEMA",
		"PARTIAL",
		"PARTITION",
		"PASCAL",
		"PASSWORD",
		"PATH",
		"PERCENTILE_CONT",
		"PERCENTILE_DISC",
		"PERCENT_RANK",
		"PLACING",
		"PLI",
		"POSITION",
		"POSTFIX",
		"POWER",
		"PRECEDING",
		"PRECISION",
		"PREFIX",
		"PREORDER",
		"PREPARE",
		"PREPARED",
		"PRESERVE",
		"PRIMARY",
		"PRIOR",
		"PRIVILEGES",
		"PROCEDURAL",
		"PROCEDURE",
		"PUBLIC",
		"QUOTE",
		"RANGE",
		"RANK",
		"READ",
		"READS",
		"REAL",
		"RECHECK",
		"RECURSIVE",
		"REF",
		"REFERENCES",
		"REFERENCING",
		"REGR_AVGX",
		"REGR_AVGY",
		"REGR_COUNT",
		"REGR_INTERCEPT",
		"REGR_R2",
		"REGR_SLOPE",
		"REGR_SXX",
		"REGR_SXY",
		"REGR_SYY",
		"REINDEX",
		"RELATIVE",
		"RELEASE",
		"RENAME",
		"REPEATABLE",
		"REPLACE",
		"RESET",
		"RESTART",
		"RESTRICT",
		"RESULT",
		"RETURN",
		"RETURNED_CARDINALITY",
		"RETURNED_LENGTH",
		"RETURNED_OCTET_LENGTH",
		"RETURNED_SQLSTATE",
		"RETURNS",
		"REVOKE",
		"RIGHT",
		"ROLE",
		"ROLLBACK",
		"ROLLUP",
		"ROUTINE",
		"ROUTINE_CATALOG",
		"ROUTINE_NAME",
		"ROUTINE_SCHEMA",
		"ROW",
		"ROWS",
		"ROW_COUNT",
		"ROW_NUMBER",
		"RULE",
		"SAVEPOINT",
		"SCALE",
		"SCHEMA",
		"SCHEMA_NAME",
		"SCOPE",
		"SCOPE_CATALOG",
		"SCOPE_NAME",
		"SCOPE_SCHEMA",
		"SCROLL",
		"SEARCH",
		"SECOND",
		"SECTION",
		"SECURITY",
		"SELECT",
		"SELF",
		"SENSITIVE",
		"SEQUENCE",
		"SERIALIZABLE",
		"SERVER_NAME",
		"SESSION",
		"SESSION_USER",
		"SET",
		"SETOF",
		"SETS",
		"SHARE",
		"SHOW",
		"SIMILAR",
		"SIMPLE",
		"SIZE",
		"SMALLINT",
		"SOME",
		"SOURCE",
		"SPACE",
		"SPECIFIC",
		"SPECIFICTYPE",
		"SPECIFIC_NAME",
		"SQL",
		"SQLCODE",
		"SQLERROR",
		"SQLEXCEPTION",
		"SQLSTATE",
		"SQLWARNING",
		"SQRT",
		"STABLE",
		"START",
		"STATE",
		"STATEMENT",
		"STATIC",
		"STATISTICS",
		"STDDEV_POP",
		"STDDEV_SAMP",
		"STDIN",
		"STDOUT",
		"STORAGE",
		"STRICT",
		"STRUCTURE",
		"STYLE",
		"SUBCLASS_ORIGIN",
		"SUBLIST",
		"SUBMULTISET",
		"SUBSTRING",
		"SUM",
		"SUPERUSER",
		"SYMMETRIC",
		"SYSID",
		"SYSTEM",
		"SYSTEM_USER",
		"TABLE",
		"TABLESAMPLE",
		"TABLESPACE",
		"TABLE_NAME",
		"TEMP",
		"TEMPLATE",
		"TEMPORARY",
		"TERMINATE",
		"THAN",
		"THEN",
		"TIES",
		"TIME",
		"TIMESTAMP",
		"TIMEZONE_HOUR",
		"TIMEZONE_MINUTE",
		"TO",
		"TOAST",
		"TOP_LEVEL_COUNT",
		"TRAILING",
		"TRANSACTION",
		"TRANSACTIONS_COMMITTED",
		"TRANSACTIONS_ROLLED_BACK",
		"TRANSACTION_ACTIVE",
		"TRANSFORM",
		"TRANSFORMS",
		"TRANSLATE",
		"TRANSLATION",
		"TREAT",
		"TRIGGER",
		"TRIGGER_CATALOG",
		"TRIGGER_NAME",
		"TRIGGER_SCHEMA",
		"TRIM",
		"TRUE",
		"TRUNCATE",
		"TRUSTED",
		"TYPE",
		"UESCAPE",
		"UNBOUNDED",
		"UNCOMMITTED",
		"UNDER",
		"UNENCRYPTED",
		"UNION",
		"UNIQUE",
		"UNKNOWN",
		"UNLISTEN",
		"UNNAMED",
		"UNNEST",
		"UNTIL",
		"UPDATE",
		"UPPER",
		"USAGE",
		"USER",
		"USER_DEFINED_TYPE_CATALOG",
		"USER_DEFINED_TYPE_CODE",
		"USER_DEFINED_TYPE_NAME",
		"USER_DEFINED_TYPE_SCHEMA",
		"USING",
		"VACUUM",
		"VALID",
		"VALIDATOR",
		"VALUE",
		"VALUES",
		"VARCHAR",
		"VARIABLE",
		"VARYING",
		"VAR_POP",
		"VAR_SAMP",
		"VERBOSE",
		"VIEW",
		"VOLATILE",
		"WHEN",
		"WHENEVER",
		"WHERE",
		"WIDTH_BUCKET",
		"WINDOW",
		"WITH",
		"WITHIN",
		"WITHOUT",
		"WORK",
		"WRITE",
		"YEAR",
		"ZONE"}
	reservedMapSync sync.Mutex
)

func isReservedKeyword(s string) bool {
	reservedMapSync.Lock()
	defer reservedMapSync.Unlock()
	if reservedMap == nil {
		reservedMap = make(map[string]bool)
		for _, v := range reservedKeywords {
			reservedMap[v] = true
		}
	}
	return reservedMap[s]
}
