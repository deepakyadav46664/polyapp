package postgresCRUD

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"os"
	"path/filepath"
	"sync"

	_ "github.com/lib/pq"
)

var (
	postgresUsername       string
	postgresPassword       string
	postgresIPAddress      string
	postgresDBName         string
	instanceConnectionName string
	dbPool                 *sqlx.DB
	dbPoolCacheMux         sync.Mutex
)

// GetClient for postgres.
func GetClient() (*sqlx.DB, error) {
	var err error
	dbPoolCacheMux.Lock()
	defer dbPoolCacheMux.Unlock()
	if dbPool != nil {
		return dbPool, nil
	}
	if postgresUsername == "" || postgresPassword == "" || postgresIPAddress == "" || postgresDBName == "" {
		postgresUsername = os.Getenv("POSTGRES_USERNAME")
		postgresPassword = os.Getenv("POSTGRES_PASSWORD")
		postgresIPAddress = os.Getenv("POSTGRES_IP_ADDRESS")
		postgresDBName = os.Getenv("POSTGRES_DB_NAME")
		instanceConnectionName = os.Getenv("INSTANCE_CONNECTION_NAME")
	}
	if postgresUsername == "" || postgresPassword == "" || postgresIPAddress == "" || postgresDBName == "" {
		// intentionally excluding postgresPassword
		return nil, fmt.Errorf("could not get all postgres connection information from environment variables - username: %v ; IP Address: %v ; DBName: %v",
			postgresUsername, postgresIPAddress, postgresDBName)
	}
	pathToCerts := filepath.Clean(filepath.ToSlash(common.GetPublicPath()) + "/../")
	pathToRootCert := filepath.FromSlash(pathToCerts + "/sslrootcert")
	pathToSSLKey := filepath.FromSlash(pathToCerts + "/sslkey")
	pathToSSLCert := filepath.FromSlash(pathToCerts + "/sslcert")
	var dbURI string
	switch common.GetCloudProvider() {
	case "GOOGLE":
		// https://github.com/GoogleCloudPlatform/golang-samples/blob/master/appengine/go11x/cloudsql/cloudsql.go
		// App Engine Standard ONLY supports connecting via the UNIX socket.
		dbURI = fmt.Sprintf("host=/cloudsql/%s/ user=%s password=%s database=%s sslmode=require sslrootcert=%v sslkey=%v sslcert=%v",
			instanceConnectionName, postgresUsername, postgresPassword, postgresDBName, pathToRootCert, pathToSSLKey, pathToSSLCert)
	default:
		dbURI = fmt.Sprintf("host=%s user=%s password=%s port=%s database=%s sslmode=require sslrootcert=%v sslkey=%v sslcert=%v",
			postgresIPAddress, postgresUsername, postgresPassword, "5432", postgresDBName, pathToRootCert, pathToSSLKey, pathToSSLCert)
	}
	dbPoolTemp, err := sqlx.Open("postgres", dbURI)
	if err != nil {
		return nil, fmt.Errorf("sqlx.Open: %v", err)
	}
	dbPoolTemp.SetMaxIdleConns(5)
	dbPoolTemp.SetMaxOpenConns(7)
	dbPoolTemp.SetConnMaxLifetime(1800)

	err = dbPoolTemp.Ping()
	if err != nil {
		return nil, fmt.Errorf("dbPoolTemp.Ping: %w", err)
	}

	// https://stackoverflow.com/questions/52243652/gcp-sql-postgres-problem-with-privileges-cant-run-a-query-with-postgres-user
	_, err = dbPoolTemp.Exec(`DO
$do$
BEGIN
	IF NOT EXISTS (
		SELECT FROM pg_catalog.pg_roles
		WHERE rolname = 'polyappmain') THEN
		CREATE ROLE polyappmain;
	END IF;
END
$do$;`)
	if err != nil {
		return nil, fmt.Errorf("client.Exec to CREATE ROLE polyappmain %v: %w", postgresUsername, err)
	}
	_, err = dbPoolTemp.Exec(`GRANT polyappmain TO ` + postgresUsername)
	if err != nil {
		return nil, fmt.Errorf("client.Exec to GRANT polyappmain TO %v: %w", postgresUsername, err)
	}
	_, err = dbPoolTemp.Exec(`SET ROLE polyappmain`)
	if err != nil {
		return nil, fmt.Errorf("client.Exec SET ROLE polyappmain: %w", err)
	}
	dbPool = dbPoolTemp

	return dbPool, nil
}

// CloseClient not only closes the client and allows you to get a new one with GetClient().
func CloseClient() {
	dbPoolCacheMux.Lock()
	defer dbPoolCacheMux.Unlock()
	dbPool.Close()
	dbPool = nil
}
