package postgresCRUD

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"strings"
	"text/template"
)

type Query struct {
	Collection string
	WHERE      string
}

func (q *Query) Init(industryID string, domainID string, schemaID string, firestoreCollection string) {
	q.Collection = firestoreCollection
}

func (q *Query) AddEquals(k string, v string) {
	if len(q.WHERE) > 0 {
		q.WHERE += "AND "
	}
	q.WHERE += ColumnNameEncode(common.RemoveNonAlphaNumeric(k)) + ` = '` + common.RemoveNonAlphaNumeric(v) + `'`
}

func (q *Query) AddArrayContains(k string, v string) error {
	panic("implement me")
}

// TODO consider adding an index on fields which we use a FullTextSearch query on.
// https://www.postgresql.org/docs/current/textsearch-tables.html#TEXTSEARCH-TABLES-INDEX
func (q *Query) AddFullTextSearch(v string, keys ...string) error {
	for i, k := range keys {
		keys[i] = ColumnNameEncode(common.RemoveNonAlphaNumeric(k))
	}
	if len(q.WHERE) > 0 {
		q.WHERE += "AND "
	}
	q.WHERE += "to_tsvector('english', "
	for i, k := range keys {
		if i > 0 {
			q.WHERE += " || ' ' ||"
		}
		q.WHERE += k
	}
	queryValue := common.RemoveNonAlphaNumeric(v)
	queryValueSplit := strings.Split(queryValue, " ")
	for i := range queryValueSplit {
		if i == 0 {
			queryValue = ""
		}
		if i > 0 {
			queryValue += " & "
		}
		queryValue += queryValueSplit[i]
	}
	q.WHERE += ") @@ to_tsquery('english', '" + queryValue + "')"
	return nil
}

func (q *Query) QueryRead() (common.Iter, error) {
	if q.WHERE == "" {
		return nil, errors.New("no WHERE was set so query will not be performed")
	}
	if q.Collection == "" {
		return nil, errors.New("must init Query before calling QueryRead")
	}
	client, err := GetClient()
	if err != nil {
		return nil, fmt.Errorf("GetClient: %w", err)
	}
	queryTemplate := `SELECT row_to_json({{.Collection}}) FROM {{.Collection}} WHERE {{.WHERE}}`
	t, err := template.New("").Parse(queryTemplate)
	if err != nil {
		return nil, fmt.Errorf("template.New.Parse: %w", err)
	}
	var w bytes.Buffer
	err = t.Execute(&w, q)
	if err != nil {
		return nil, fmt.Errorf("t.Execute: %w", err)
	}
	rows, err := client.Queryx(w.String())
	if err != nil {
		return nil, fmt.Errorf("client.Queryx: %w", err)
	}
	return RowIter{
		Collection: q.Collection,
		Rows:       rows,
	}, nil
}

type RowIter struct {
	Collection string
	Rows       *sqlx.Rows
}

func (r RowIter) Next() (common.Queryable, error) {
	var rowJSON string
	ok := r.Rows.Next()
	if !ok {
		return nil, common.IterDone
	}
	err := r.Rows.Scan(&rowJSON)
	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}
	m := make(map[string]interface{}, 0)
	err = json.Unmarshal([]byte(rowJSON), &m)
	if err != nil {
		return nil, fmt.Errorf("row.MapScan: %w", err)
	}
	newMap := make(map[string]interface{})
	for k, v := range m {
		newMap[ColumnNameDecode(k)] = v
	}
	var q common.Queryable
	switch r.Collection {
	case common.CollectionData:
		q = &common.Data{}
	case common.CollectionSchema:
		q = &common.Schema{}
	case common.CollectionUser:
		q = &common.User{}
	case common.CollectionTask:
		q = &common.Task{}
	case common.CollectionUX:
		q = &common.UX{}
	case common.CollectionRole:
		q = &common.Role{}
	case common.CollectionBot:
		q = &common.Bot{}
	case common.CollectionAction:
		q = &common.Action{}
	case common.CollectionPublicMap:
		q = &common.PublicMap{}
	case common.CollectionCSS:
		q = &common.CSS{}
	default:
		return nil, errors.New("Unhandled collection: " + r.Collection)
	}
	err = q.Init(newMap)
	if err != nil {
		return nil, fmt.Errorf("q.Init: %w", err)
	}
	return q, nil
}

func (r RowIter) Stop() {
	r.Rows.Close()
}

// Length always returns -1 because we do not know how long the query result is prior to reading all results.
func (r RowIter) Length() int {
	return -1
}
